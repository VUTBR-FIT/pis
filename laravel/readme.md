### API Route table:
```
* - external routes
! - employee routes
@ - admin routes

* < ! < @
+------+----------+--------------------------------+------------------------+---------------------------------------------------+------------+------------+----------+------------+
| Host | Method   | URI                            | Name                   | Action                                            | Protected  | Version(s) | Scope(s) | Rate Limit |
+------+----------+--------------------------------+------------------------+---------------------------------------------------+------------+------------+----------+------------+
|      | GET|HEAD | /api/rental                    | api.rental.index*       | App\Http\Controllers\RentalController@index       | Yes       | v1         |          |            |
|      | GET|HEAD | /api/rental/{rental}           | api.rental.show*        | App\Http\Controllers\RentalController@show        | Yes       | v1         |          |            |
|      | POST     | /api/rental                    | api.rental.create!      | App\Http\Controllers\RentalController@create      | Yes       | v1         |          |            |
|      | PUT      | /api/rental/{rental}           | api.rental.update!      | App\Http\Controllers\RentalController@update      | Yes       | v1         |          |            |
|      | DELETE   | /api/rental/{rental}           | api.rental.delete!      | App\Http\Controllers\RentalController@delete      | Yes       | v1         |          |            |
|      | POST     | /api/login                     | api.auth.login          | App\Http\Controllers\AuthController@authenticate  | No        | v1         |          |            |
|      | POST     | /api/register                  | api.auth.register       | App\Http\Controllers\AuthController@register      | No        | v1         |          |            |
|      | POST     | /api/refresh                   | api.auth.refresh        | App\Http\Controllers\AuthController@refresh       | No        | v1         |          |            |
|      | GET|HEAD | /api/genre                     | api.genre.index*        | App\Http\Controllers\GenreController@index        | Yes       | v1         |          |            |
|      | GET|HEAD | /api/genre/{genre}             | api.genre.show*         | App\Http\Controllers\GenreController@show         | Yes       | v1         |          |            |
|      | POST     | /api/genre                     | api.genre.create!       | App\Http\Controllers\GenreController@create       | Yes       | v1         |          |            |
|      | PUT      | /api/genre/{genre}             | api.genre.update!       | App\Http\Controllers\GenreController@update       | Yes       | v1         |          |            |
|      | DELETE   | /api/genre/{genre}             | api.genre.delete!       | App\Http\Controllers\GenreController@delete       | Yes       | v1         |          |            |
|      | GET|HEAD | /api/user                      | api.user.index*         | App\Http\Controllers\UserController@index         | Yes       | v1         |          |            |
|      | GET|HEAD | /api/user/{user}               | api.user.show*          | App\Http\Controllers\UserController@show          | Yes       | v1         |          |            |
|      | POST     | /api/user                      | api.user.create@        | App\Http\Controllers\UserController@create        | Yes       | v1         |          |            |
|      | PUT      | /api/user/{user}               | api.user.update@        | App\Http\Controllers\UserController@update        | Yes       | v1         |          |            |
|      | DELETE   | /api/user/{user}               | api.user.delete@        | App\Http\Controllers\UserController@delete        | Yes       | v1         |          |            |
|      | GET|HEAD | /api/author                    | api.author.index*       | App\Http\Controllers\AuthorController@index       | Yes       | v1         |          |            |
|      | GET|HEAD | /api/author/{author}           | api.author.show*        | App\Http\Controllers\AuthorController@show        | Yes       | v1         |          |            |
|      | POST     | /api/author                    | api.author.create!      | App\Http\Controllers\AuthorController@create      | Yes       | v1         |          |            |
|      | PUT      | /api/author/{author}           | api.author.update!      | App\Http\Controllers\AuthorController@update      | Yes       | v1         |          |            |
|      | DELETE   | /api/author/{author}           | api.author.delete!      | App\Http\Controllers\AuthorController@delete      | Yes       | v1         |          |            |
|      | GET|HEAD | /api/copy                      | api.copy.index*         | App\Http\Controllers\CopyController@index         | Yes       | v1         |          |            |
|      | GET|HEAD | /api/copy/{copy}               | api.copy.show*          | App\Http\Controllers\CopyController@show          | Yes       | v1         |          |            |
|      | POST     | /api/copy                      | api.copy.create!        | App\Http\Controllers\CopyController@create        | Yes       | v1         |          |            |
|      | PUT      | /api/copy/{copy}               | api.copy.update!        | App\Http\Controllers\CopyController@update        | Yes       | v1         |          |            |
|      | DELETE   | /api/copy/{copy}               | api.copy.delete!        | App\Http\Controllers\CopyController@delete        | Yes       | v1         |          |            |
|      | GET|HEAD | /api/title                     | api.title.index*        | App\Http\Controllers\TitleController@index        | Yes       | v1         |          |            |
|      | GET|HEAD | /api/title/{title}             | api.title.show*         | App\Http\Controllers\TitleController@show         | Yes       | v1         |          |            |
|      | GET|HEAD | /api/title/rentals/{title}     | api.title.rentals!      | App\Http\Controllers\TitleController@rentals      | No        | v1         |          |            |
|      | GET|HEAD | /api/title/reservations/{title}| api.title.reservations! | App\Http\Controllers\TitleController@reservations | No        | v1         |          |            |
|      | POST     | /api/title                     | api.title.create!       | App\Http\Controllers\TitleController@create       | Yes       | v1         |          |            |
|      | PUT      | /api/title/{title}             | api.title.update!       | App\Http\Controllers\TitleController@update       | Yes       | v1         |          |            |
|      | DELETE   | /api/title/{title}             | api.title.delete!       | App\Http\Controllers\TitleController@delete       | Yes       | v1         |          |            |
|      | GET|HEAD | /api/reservation               | api.reservation.index*  | App\Http\Controllers\ReservationController@index  | Yes       | v1         |          |            |
|      | GET|HEAD | /api/reservation/{reservation} | api.reservation.show*   | App\Http\Controllers\ReservationController@show   | Yes       | v1         |          |            |
|      | POST     | /api/reservation               | api.reservation.create! | App\Http\Controllers\ReservationController@create | Yes       | v1         |          |            |
|      | PUT      | /api/reservation/{reservation} | api.reservation.update! | App\Http\Controllers\ReservationController@update | Yes       | v1         |          |            |
|      | DELETE   | /api/reservation/{reservation} | api.reservation.delete! | App\Http\Controllers\ReservationController@delete | Yes       | v1         |          |            |
+------+----------+--------------------------------+-------------------------+---------------------------------------------------+-----------+------------+----------+------------+
```
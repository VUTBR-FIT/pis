<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $fillable = [
        'genre_name', 'age_restriction', 'description'
    ];

    // Eloquent relations definition
    public function titles()
    {
        return $this->belongsToMany(Title::class);
    }
}

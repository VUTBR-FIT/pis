<?php

namespace App\Console\Commands;

use App\Reservation;
use App\Rental;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:expiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update expiration date on rentals and reservation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reservations = Reservation::all();
        $reservations->each(function($reservation) {
            $expiry_date = date('Y-m-d', strtotime($reservation->date_expiry));
            $date = new Carbon;
            if ($date->format('Y-m-d') == $expiry_date) {
                $reservation->update(['status' => 2]);
            }
        });

        $rentals = Rental::all();
        $rentals->each(function($rental) {
            $expiry_date = date('Y-m-d', strtotime($rental->date_expiry));
            $date = new Carbon;
            if ($date->format('Y-m-d') == $expiry_date) {
                $rental->update(['status' => 2]);
            }
        });
    }
}

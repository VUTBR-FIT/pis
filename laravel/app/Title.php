<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    const MEDIUM = [
        'Book', 'Magazine', 'Audiobook', 'CD', 'Other'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_name', 'isbn', 'ean', 'published', 'description', 'medium', 'publisher',
    ];

    // Eloquent relations definition
    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    public function reservation()
    {
        return $this->hasMany(Reservation::class);
    }

    public function copies() {
        return $this->hasMany(Copy::class);
    }

    // Functions
    public function addCopy($health = 1.00) {
        $copy = new Copy();
        $copy->title = $this;
        $copy->health_percentage = $health;

        return $copy->save();
    }

    public function checkAvailability()
    {
        foreach ($this->copies as $copy) {
            if($copy->availabilityStatus() == Copy::STATUS_AVAILABLE)
                return true;
        }

        return false;
    }
}

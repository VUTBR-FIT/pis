<?php

namespace App;

class Token
{
    public $token;
    public $token_created;

    public function __construct($token)
    {
        $this->token_created = time();
        $this->token = $token;
    }
}

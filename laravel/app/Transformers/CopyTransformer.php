<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 20:03
 */

namespace App\Transformers;


use App\Copy;
use App\Transformers\ShortTransformers\RentalShortTransformer;
use League\Fractal\TransformerAbstract;

class CopyTransformer extends TransformerAbstract
{
    public $defaultIncludes = [
        'rentals'
    ];

    public function transform(Copy $copy)
    {
        return [
            'id' => $copy->id,
            'health_percentage' => $copy->health_percentage * 100 . ' %',
            'title' => $copy->title->title_name,
            'status' => [
                Copy::STATUS_AVAILABLE => 'available',
                Copy::STATUS_RENTED => 'rented',
                Copy::STATUS_RENTAL_EXPIRED => 'rental expired',
                Copy::STATUS_LOST => 'lost'
            ][$copy->availabilityStatus()],
        ];
    }

    public function includeRentals(Copy $copy)
    {
        return $this->collection($copy->rentals, new RentalShortTransformer);
    }
}
<?php
/**
 * Created by PhpStorm.
 * genre: vojtech
 * Date: 2019-03-13
 * Time: 18:45
 */

namespace App\Transformers;


use App\genre;
use League\Fractal\TransformerAbstract;

class GenreTransformer extends TransformerAbstract
{
    public function transform(Genre $genre) {
        return [
            'id' => $genre->id,
            'genre_name' => $genre->genre_name,
            'age_restriction' => $genre->age_restriction,
            'description' => $genre->description
        ];
    }
}
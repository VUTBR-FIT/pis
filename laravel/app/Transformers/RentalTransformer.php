<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Rental;
use App\Transformers\ShortTransformers\UserShortTransformer;
use App\Transformers\ShortTransformers\CopyShortTransformer;

class RentalTransformer extends TransformerAbstract
{
    public $defaultIncludes = [
        'user', 'copy'
    ];

    public function transform(Rental $rental) {
        return [
            'id' => $rental->id,
            'date_rental' => date('d. m. Y', strtotime($rental->date_rental)),
            'date_expiry' => date('d. m. Y', strtotime($rental->date_expiry)),
            'status' => [
                Rental::STATUS_ACTIVE => 'active',
                Rental::STATUS_EXPIRED => 'expired',
                Rental::STATUS_RETURNED => 'returned',
                Rental::STATUS_LOST => 'lost'
            ][$rental->status],
        ];
    }

    public function includeUser(Rental $rental) {
        return $this->item($rental->user, new UserShortTransformer);
    }

    public function includeCopy(Rental $rental) {
        return $this->item($rental->copy, new CopyShortTransformer);
    }
}
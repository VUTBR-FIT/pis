<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 19:59
 */

namespace App\Transformers\ShortTransformers;


use App\User;
use League\Fractal\TransformerAbstract;

class UserShortTransformer extends TransformerAbstract
{
    public function transform(User $user) {
        return [
            'id' => $user->id,
            'name' => $user->last_name . ', ' . $user->first_name . (!is_null($user->middle_name) ? ' ' . $user->middle_name : '')
        ];
    }
}
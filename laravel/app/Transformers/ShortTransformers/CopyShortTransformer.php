<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 20:01
 */

namespace App\Transformers\ShortTransformers;


use App\Copy;
use League\Fractal\TransformerAbstract;

class CopyShortTransformer extends TransformerAbstract
{
    public function transform(Copy $copy) {
        return [
            'id' => $copy->id,
            'title' => $copy->title->title_name,
            'titleId' => $copy->title->id,
        ];
    }
}
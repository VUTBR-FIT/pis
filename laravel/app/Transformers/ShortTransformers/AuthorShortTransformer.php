<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 19:25
 */

namespace App\Transformers\ShortTransformers;


use App\Author;
use League\Fractal\TransformerAbstract;

class AuthorShortTransformer extends TransformerAbstract
{
    public function transform(Author $author) {
        return [
            'id' => $author->id,
            'name' => $author->last_name . ', ' . $author->first_name . (!is_null($author->middle_name) ? ' ' . $author->middle_name : '')
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 20:04
 */

namespace App\Transformers\ShortTransformers;


use App\Title;
use League\Fractal\TransformerAbstract;

class TitleShortTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'authors'
    ];

    public function transform(Title $title) {
        return [
            'id' => $title->id,
            'name' => $title->title_name,
            'available' => $title->checkAvailability() ? 1 : 0,
        ];
    }

    public function includeAuthors(Title $title)
    {
        return $this->collection($title->authors, new AuthorShortTransformer);
    }
}
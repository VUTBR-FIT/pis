<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 20:16
 */

namespace App\Transformers\ShortTransformers;


use App\Copy;
use App\Rental;
use App\Reservation;
use App\Title;
use League\Fractal\TransformerAbstract;

class RentalShortTransformer extends TransformerAbstract
{
    public $defaultIncludes = [
        'user'
    ];

    public function transform(Rental $rental) {

        return [
            'id' => $rental->id,
            'title' => $rental->copy->title->title_name,
            'date_rental' => $rental->date_rental,
            'date_expiry' => $rental->date_expiry,
            'status' => [
                Rental::STATUS_ACTIVE => 'active',
                Rental::STATUS_EXPIRED => 'expired',
                Rental::STATUS_RETURNED => 'returned',
                Rental::STATUS_LOST => 'lost'
            ][$rental->status],
        ];
    }

    public function includeUser(Rental $rental)
    {
        return $this->item($rental->user, new UserShortTransformer);
    }
}
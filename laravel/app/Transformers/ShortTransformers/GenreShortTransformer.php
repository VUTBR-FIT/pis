<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 19:25
 */

namespace App\Transformers\ShortTransformers;


use App\Genre;
use League\Fractal\TransformerAbstract;

class GenreShortTransformer extends TransformerAbstract
{
    public function transform(Genre $genre) {
        return [
            'id' => $genre->id,
            'name' => $genre->genre_name
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 20:16
 */

namespace App\Transformers\ShortTransformers;


use App\Reservation;
use League\Fractal\TransformerAbstract;

class ReservationShortTransformer extends TransformerAbstract
{
    public function transform(Reservation $reservation) {
        return [
            'id' => $reservation->id,
            'title' => $reservation->title->title_name,
            'status' => [
                Reservation::STATUS_ACTIVE => 'active',
                Reservation::STATUS_EXPIRED => 'expired',
                Reservation::STATUS_CANCELED => 'cancelled',
            ][$reservation->status],
        ];
    }
}
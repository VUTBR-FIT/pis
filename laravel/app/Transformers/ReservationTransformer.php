<?php
namespace App\Transformers;

use App\Reservation;
use App\User;
use League\Fractal\TransformerAbstract;
use App\Rental;
use App\Transformers\ShortTransformers\UserShortTransformer;
use App\Transformers\ShortTransformers\TitleShortTransformer;

class ReservationTransformer extends TransformerAbstract
{
    public $defaultIncludes = [
        'user', 'title'
    ];

    public function transform(Reservation $reservation) {
        return [
            'id' => $reservation->id,
            'date_reservation' => date('d. m. Y', strtotime($reservation->date_reservation)),
            'date_expiry' => date('d. m. Y', strtotime($reservation->date_expiry)),
            'status' => [
                Reservation::STATUS_ACTIVE => 'active',
                Reservation::STATUS_EXPIRED => 'expired',
                Reservation::STATUS_CANCELED => 'cancelled',
            ][$reservation->status],
        ];
    }

    public function includeUser(Reservation $reservation) {
        return $this->item($reservation->user, new UserShortTransformer);
    }

    public function includeTitle(Reservation $reservation) {
        return $this->item($reservation->title, new TitleShortTransformer);
    }
}
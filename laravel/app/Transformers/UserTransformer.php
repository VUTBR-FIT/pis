<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 18:39
 */

namespace App\Transformers;


use App\Transformers\ShortTransformers\RentalShortTransformer;
use App\Transformers\ShortTransformers\ReservationShortTransformer;
use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public $defaultIncludes = [
        'rentals', 'reservations'
    ];

    protected $token = null;

    public function transform(User $user)
    {
        $attributes = [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'middle_name' => $user->middle_name,
            'email' => $user->email,
            'role' =>
                [
                    User::ROLE_ADMIN => 'admin',
                    User::ROLE_EMPLOYEE => 'employee',
                    User::ROLE_EXTERNAL => 'external'
                ][$user->role],
            'date_birth' => !is_null($user->date_birth)
                ? date('d. m. Y', strtotime($user->date_birth))
                : null,
            'rental_capacity' => $user->role == User::ROLE_EXTERNAL
                ? $user->rental_capacity
                : null,
            'reservation_capacity' => $user->role == User::ROLE_EXTERNAL
                ? $user->reservation_capacity
                : null,
        ];
        
        return $attributes;
    }

    public function includeRentals(User $user)
    {
        return
            $user->role == User::ROLE_EXTERNAL
                ? $this->collection($user->rentals, new RentalShortTransformer)
                : null;
    }

    public function includeReservations(User $user)
    {
        return
            $user->role == User::ROLE_EXTERNAL
                ? $this->collection($user->reservations, new ReservationShortTransformer)
                : null;
    }
}
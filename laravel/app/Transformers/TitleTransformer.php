<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 17:22
 */

namespace App\Transformers;


use App\Title;
use App\Transformers\ShortTransformers\AuthorShortTransformer;
use App\Transformers\ShortTransformers\CopyShortTransformer;
use App\Transformers\ShortTransformers\GenreShortTransformer;
use League\Fractal\TransformerAbstract;

class TitleTransformer extends TransformerAbstract
{

    public $defaultIncludes = [
        'authors', 'genres'
    ];

    public function __construct($withCopies = false)
    {
        if($withCopies) {
            $this->defaultIncludes[] = 'copies';
        }
    }

    public function transform(Title $title)
    {
        return [
            'id' => $title->id,
            'title_name' => $title->title_name,
            'isbn' => $title->isbn,
            'ean' => $title->ean,
            'published_at' => date('d. m. Y', strtotime($title->published_at)),
            'description' => $title->description,
            'publisher' => $title->publisher,
            'medium' => $title->medium,
            'copies_count' => $title->copies()->count(),
            'available' => $title->checkAvailability() ? 1 : 0,
        ];
    }

    public function includeAuthors(Title $title) {
        return $this->collection($title->authors, new AuthorShortTransformer);
    }

    public function includeGenres(Title $title) {
        return $this->collection($title->genres, new GenreShortTransformer);
    }

    public function includeCopies(Title $title) {
        return $this->collection($title->copies, new CopyTransformer);
    }
}
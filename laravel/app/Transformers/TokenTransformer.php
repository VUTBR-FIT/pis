<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 2019-03-13
 * Time: 18:39
 */

namespace App\Transformers;


use App\Token;
use App\Transformers\ShortTransformers\RentalShortTransformer;
use App\Transformers\ShortTransformers\ReservationShortTransformer;
use App\User;
use League\Fractal\TransformerAbstract;

class TokenTransformer extends TransformerAbstract
{

    protected $user;
    protected $defaultIncludes = [];

    public function __construct(User $user = null)
    {
        $this->user = $user;
        if (!is_null($user)) {
            $this->defaultIncludes[] = 'user';
        }
    }

    public function transform(Token $token)
    {
        $attributes = [
            'token' => $token->token,
            'token_created' => $token->token_created
        ];
        

        return $attributes;
    }

    public function includeUser()
    {
        return $this->item($this->user, new UserTransformer);
    }
}
<?php
/**
 * Created by PhpStorm.
 * author: vojtech
 * Date: 2019-03-13
 * Time: 18:45
 */

namespace App\Transformers;


use App\Author;
use App\Transformers\ShortTransformers\TitleShortTransformer;
use League\Fractal\TransformerAbstract;

class AuthorTransformer extends TransformerAbstract
{
    public $defaultIncludes = [
        'titles'
    ];

    public function transform(Author $author) {
        return [
            'id' => $author->id,
            'first_name' => $author->first_name,
            'last_name' => $author->last_name,
            'middle_name' => $author->middle_name,
            'date_birth' => date('d. m. Y', strtotime($author->date_birth)),
            'date_death' => !is_null($author->date_death) ? date('d. m. Y', strtotime($author->date_death)) : null,
            'description' => $author->description
        ];
    }

    public function includeTitles(Author $author) {
        return $this->collection($author->titles, new TitleShortTransformer);
    }
}
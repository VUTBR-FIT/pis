<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * User role constants
     */
    const ROLE_ADMIN = 1;
    const ROLE_EMPLOYEE = 2;
    const ROLE_EXTERNAL = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'role', 'first_name', 'last_name', 'middle_name',
        'date_birth', 'rental_capacity', 'reservation_capacity',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Eloquent relations definiton
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function rentals()
    {
        return $this->hasMany(Rental::class);
    }

    // JWT functions
    public function getJWTIdentifier()
    {
        return $this->id;
    }

    public function getJWTCustomClaims()
    {
        return [
            'role' => $this->role,
        ];
    }

    /**
     * Check if user has free slots in his rental capacity
     * @param User $user
     * @return bool
     */
    public function checkRentalCapacity()
    {
        $rentals = $this->rentals
            ->where('status', '!=', Rental::STATUS_RETURNED)
            ->count();

        return $this->rental_capacity - $rentals > 0;
    }

    /**
     * Check if user has free slots in his reservation capacity
     * @param User $user
     * @return bool
     */
    public function checkReservationCapacity()
    {
        $reservations = $this->reservations
            ->where('status', Reservation::STATUS_ACTIVE)
            ->count();

        return $this->reservation_capacity - $reservations > 0;
    }

    /**
     * Check if user has unreturned expired rentals
     * @return bool
     */
    public function checkExpiredRentals()
    {
        $expired = $this->rentals
            ->where('status', Rental::STATUS_EXPIRED)
            ->count();

        return $expired > 0;
    }
}

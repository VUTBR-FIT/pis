<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Copy extends Model
{
    const STATUS_AVAILABLE = 1;
    const STATUS_RENTED = 2;
    const STATUS_RENTAL_EXPIRED = 3;
    const STATUS_LOST = 4;

    protected $table = 'copies';

    protected $fillable = [
        'health_percentage'
    ];

    // Eloquent relations definition
    public function rentals()
    {
        return $this->hasMany(Rental::class);
    }

    public function title()
    {
        return $this->belongsTo(Title::class);
    }

    public function availabilityStatus()
    {
        $rentals = $this->rentals->sortBy('status', SORT_DESC);
        if(is_null($rentals)) {
            return static::STATUS_AVAILABLE;
        }
        if($rentals->where('status', Rental::STATUS_EXPIRED)->first()) {
            return static::STATUS_RENTAL_EXPIRED;
        }
        if($rentals->where('status', Rental::STATUS_LOST)->first()) {
            return static::STATUS_LOST;
        }
        if($rentals->where('status', Rental::STATUS_ACTIVE)->first()) {
            return static::STATUS_RENTED;
        }
        return static::STATUS_AVAILABLE;
    }
}

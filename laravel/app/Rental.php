<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_EXPIRED = 2;
    const STATUS_RETURNED = 3;
    const STATUS_LOST = 4;


    protected $fillable = [
        'date_rental', 'date_expiry', 'status'
    ];

    // Eloquent relations definition
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function copy()
    {
        return $this->belongsTo(Copy::class, 'copy_id');
    }
}

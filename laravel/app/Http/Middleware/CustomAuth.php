<?php
/**
 * Created by PhpStorm.
 * User: bezoadam
 * Date: 2019-03-17
 * Time: 12:01
 */

namespace App\Http\Middleware;

use App\User;
use Closure;
use Dingo\Api\Http\Middleware\Auth;
use JWTAuth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;

define('EXTERNAL_ROUTES', [
    'api.reservation.index', 'api.reservation.show',
    'api.rental.index', 'api.rental.show',
    'api.author.index', 'api.author.show',
    'api.genre.show', 'api.genre.index',
    'api.title.index', 'api.title.show',
    'api.user.index', 'api.user.show',
    'api.copy.index', 'api.copy.show',
    'api.reservation.create', 'api.reservation.update',
    'api.auth.logout',
    'api.user.update', 'api.user.delete'
]);
define('EMPLOYEE_ROUTES', [
    'api.author.create', 'api.author.update', 'api.author.delete',
    'api.genre.create', 'api.genre.update', 'api.genre.delete',
    'api.title.create', 'api.title.update', 'api.title.delete', 'api.title.reservations', 'api.title.rentals',
    'api.rental.create', 'api.rental.update', 'api.rental.delete',
    'api.reservation.create', 'api.reservation.update', 'api.reservation.delete',
    'api.copy.create', 'api.copy.update', 'api.copy.delete',
    'api.user.create'
]);
define('ADMIN_ROUTES', []);


class CustomAuth extends Auth
{

    public function handle($request, Closure $next)
    {
        try {
            $route = $this->router->getCurrentRoute();
            $token = JWTAuth::getToken();
            if (is_null($token)) {
                $this->throwException();
            }
            $payloadRole = JWTAuth::decode($token)->get('role');
            $routeName = $route->getName();

            switch ($payloadRole) {
                case User::ROLE_ADMIN:
                    if (in_array($routeName, array_merge(
                        array_merge(ADMIN_ROUTES, EMPLOYEE_ROUTES), EXTERNAL_ROUTES
                    ))) {
                        $this->handleAuth();
                    } else {
                        $this->throwException();
                    }
                    break;

                case User::ROLE_EMPLOYEE:
                    if (in_array($routeName, array_merge(EMPLOYEE_ROUTES, EXTERNAL_ROUTES))) {
                        $this->handleAuth();
                    } else {
                        $this->throwException();
                    }
                    break;

                case User::ROLE_EXTERNAL:
                    if (in_array($routeName, EXTERNAL_ROUTES)) {
                        $this->handleAuth();
                    } else {
                        $this->throwException();
                    }
                    break;

                default:
                    $this->throwException();
            }
        } catch (TokenBlacklistedException $e) {
            $this->throwException();
        }

        $allow_origin = [
            'http://localhost',
            'http://localhost:3000',
            'http://localhost:8000',
            'http://localhost:8080',
        ];

        if (isset($_SERVER['HTTP_ORIGIN']) &&
            in_array($_SERVER['HTTP_ORIGIN'], $allow_origin)) {

            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Secret');
            header('Access-Control-Max-Age: 120');
        }

        return $next($request);
    }

    private function throwException()
    {
        throw new UnauthorizedHttpException("Basic",'Unauthorized');
    }

    private function handleAuth()
    {
        $route = $this->router->getCurrentRoute();
        if (!$this->auth->check(false)) {
            $this->auth->authenticate($route->getAuthenticationProviders());
        }
    }
}
<?php

namespace App\Http\Controllers;

use App\Rental;
use App\Title;
use App\Transformers\RentalTransformer;
use App\Transformers\ReservationTransformer;
use App\Transformers\TitleTransformer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * @group Titles
 *
 * API for titles
 */
class TitleController extends Controller
{
    /**
     * Index titles
     *
     * [Returns all titles]
     *
     */
    public function index($paginate = true)
    {
        return $this->response
            ->collection(Title::with('authors', 'genres')->get(), new TitleTransformer);

    }

    /**
     * Show title
     *
     * [Show title by given id]
     * @queryParam id int required Id of the title.
     *
     */
    public function show(Title $title)
    {
        return $this->response
            ->item($title->load('authors', 'genres', 'copies'), new TitleTransformer(true));
    }

    /**
     * Create title
     *
     * [Create new title]
     * @bodyParam title_name String required Title name.
     * @bodyParam isbn Isbn Isbn parameter.
     * @bodyParam ean Int Ean parameter.
     * @bodyParam published_at Date required Published date.
     * @bodyParam description String required Description.
     * @bodyParam publisher String required Publisher name
     * @bodyParam medium String required One medium from {Book, Magazine, Audiobook, CD, Other}
     * @bodyParam authors String required Authors
     * @bodyParam genres String required Genres
     *
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'title_name' => 'required | string | min:1 | max:191',
            'isbn' => 'required | isbn | unique:titles,isbn',
            'ean' => 'required | integer | unique:titles,ean',
            'published_at' => 'required | date | date_format:"Y-m-d"| before_or_equal:' . date('Y-m-d'),
            'description' => 'required | string | min:1 | max:191',
            'publisher' => 'required | string | min:1 | max:191',
            'medium' => 'required | string | in:Book,Magazine,Audiobook,CD,Other',
            'authors' => 'required | array',
            'genres' => 'required | array'
        ]);

        if ($validatedData) {
            $title = new Title();

            $title->fill($request->all());
            $title->published_at = $request->published_at;

            if ($title->save()) {
                $title->authors()->sync($request->authors);

                $title->genres()->sync($request->genres);

                return $this->response
                    ->item($title, new TitleTransformer);
            } else {
                return $this->response->errorInternal();
            }
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Update title
     *
     * [Update new title]
     * @bodyParam title_name String Title name.
     * @bodyParam isbn Isbn Isbn parameter.
     * @bodyParam ean Int Ean parameter.
     * @bodyParam published_at Date Published date.
     * @bodyParam description String Description.
     * @bodyParam publisher String Publisher name
     * @bodyParam medium String One medium from {Book, Magazine, Audiobook, CD, Other}
     * @bodyParam authors String Authors
     * @bodyParam genres String Genres
     *
     */
    public function update(Title $title, Request $request)
    {
        if (empty($request->all())) {
            return $this->response
                ->item($title, new TitleTransformer);
        }
        $validatedData = $request->validate([
            'title_name' => 'string | min:1 | max:191',
            'isbn' => ['isbn', Rule::unique('titles', 'isbn')->ignore($title->id)],
            'ean' => ['integer', Rule::unique('titles', 'ean')->ignore($title->id)],
            'published_at' => 'date | date_format:Y-m-d| before_or_equal:' . date('Y-m-d'),
            'description' => 'string | min:1 | max:191',
            'publisher' => 'string | min:1 | max:191',
            'medium' => 'string | in:Book,Magazine,Audiobook,CD,Other',
            'authors' => 'array',
            'genres' => 'array'
        ]);

        if ($validatedData) {
            $title->fill($request->all());

            if (isset($request->authors)) {
                $title->authors()->sync($request->authors);
            }
            if (isset($request->genres)) {
                $title->genres()->sync($request->genres);
            }

            if ($title->save()) {
                return $this->response
                    ->item($title, new TitleTransformer);
            } else {
                return $this->response->errorInternal();
            }


        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Delete reservation
     *
     * [Delete reservation by given id]
     * @queryParam id int required Id of the reservation.
     *
     */
    public
    function delete(Title $title)
    {
        if ($title->delete()) {
            return $this->response->noContent();
        } else {
            return $this->response->errorInternal();
        }
    }

    /**
     * Index title rentals
     *
     * [Index rentals by given title id]
     * @queryParam id int required Id of the title.
     *
     */
    public
    function rentals(Title $title)
    {
        $copies = $title->copies;

        $copyIds = [];
        foreach ($copies as $copy) {
            $copyIds[] = $copy->id;
        }

        return $this->response
            ->collection(Rental::whereIn('copy_id', $copyIds)
                ->with('user', 'copy')
                ->orderBy('date_rental', 'desc')->get(), new RentalTransformer);
    }

    /**
     * Index title reservations
     *
     * [Index reservations by given title id]
     * @queryParam id int required Id of the title.
     *
     */
    public
    function reservations(Title $title)
    {
        return $this->response
            ->collection($title->reservation
                ->sortByDesc('date_reservation'), new ReservationTransformer);
    }
}

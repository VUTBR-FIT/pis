<?php

namespace App\Http\Controllers;

use App\Genre;
use App\Transformers\GenreTransformer;
use Illuminate\Http\Request;
use Dingo\Api\Exception\UpdateResourceFailedException;

/**
 * @group Genre
 *
 * API for genres
 */
class GenreController extends Controller
{
    /**
     * Index genres
     *
     * [Returns all genres]
     *
     */
    public function index()
    {
        return $this->response
            ->collection(Genre::all(), new GenreTransformer);
    }

    /**
     * Show genre
     *
     * [Show genre by given id]
     * @queryParam id int required Id of the genre.
     *
     */
    public function show(Genre $genre)
    {
        return $this->response
            ->item($genre, new GenreTransformer);
    }

    /**
     * Create genre
     *
     * [Create new genre]
     * @bodyParam genre_name String required Genre name.
     * @bodyParam age_restriction Int required Genre age restriction.
     * @bodyParam description String required Genre description.
     *
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'genre_name' => 'required | string | min:1 | max:191',
            'age_restriction' => 'required | integer | max:18',
            'description' => 'required | string | min:1 | max:191',
        ]);

        if ($validatedData) {
            $genre = new Genre($request->all());

            if($genre->save()) {
                return $this->response
                    ->item($genre, new GenreTransformer);
            } else {
                return $this->response->errorInternal();
            }
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Update genre
     *
     * [Update new genre]
     * @bodyParam genre_name String Genre name.
     * @bodyParam age_restriction Int Genre age restriction.
     * @bodyParam description String Genre description.
     *
     */
    public function update(Genre $genre, Request $request)
    {
        if (empty($request->all())) {
            return $this->response
                ->item($genre, new GenreTransformer);
        }
        $validatedData = $request->validate([
            'genre_name' => 'string | min:1 | max:191',
            'age_restriction' => 'integer | max:18',
            'description' => 'string | min:1 | max:191',
        ]);

        if ($validatedData) {
            $genre->update($request->all());

            return $this->response
                ->item($genre, new GenreTransformer);
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Delete genre
     *
     * [Delete genre by given id]
     * @queryParam id int required Id of the genre.
     *
     */
    public function delete(Genre $genre)
    {
        if ($genre->delete()) {
            return $this->response->noContent();
        } else {
            return $this->response->errorInternal();
        }
    }
}

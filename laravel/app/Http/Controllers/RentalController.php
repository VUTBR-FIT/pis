<?php

namespace App\Http\Controllers;

use App\Copy;
use App\Rental;
use App\Transformers\RentalTransformer;
use App\User;
use Illuminate\Http\Request;
use Dingo\Api\Exception\UpdateResourceFailedException;

/**
 * @group Rental
 *
 * API for rentals
 */
class RentalController extends Controller
{
    /**
     * Index rentals
     *
     * [Returns all rentals]
     *
     */
    public function index()
    {
        if ($this->auth->user()->role == User::ROLE_EXTERNAL) {
            $response = $this->response
                ->collection(Rental::where('user_id', $this->auth->user()->id)
                    ->with('user', 'copy')->get(), new RentalTransformer);
        } else {
            $response = $this->response
                ->collection(Rental::with('user', 'copy')
                    ->get(), new RentalTransformer);
        }

        return $response;
    }

    /**
     * Show rental
     *
     * [Show rental by given id]
     * @queryParam id int required Id of the rental.
     *
     */
    public function show(Rental $rental)
    {
        if ($this->auth->user()->role == User::ROLE_EXTERNAL && $this->auth->user()->id != $rental->user_id) {
            $response = $this->response->errorUnauthorized();
        } else {
            $response = $this->response
                ->item($rental->load('user', 'copy'), new RentalTransformer);
        }

        return $response;
    }

    /**
     * Create rental
     *
     * [Create new rental]
     * @bodyParam date_rental date required Rental date.
     * @bodyParam date_expiry date required Expiration date.
     * @bodyParam user_id Int required User id.
     * @bodyParam copy_id Int required Copy id.
     * @bodyParam status Int [1 => active, 2 => expired, 3 => returned, 4 => lost].
     *
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'date_rental' => 'required | date | date_format:"Y-m-d"',
            'date_expiry' => 'required | date | date_format:"Y-m-d"| after_or_equal:' . date('Y-m-d', strtotime($request->date_rental)),
            'user_id' => 'required | integer',
            'copy_id' => 'required | integer',
            'status' => 'integer'
        ]);
        if ($validatedData) {
            $user = User::find($request->user_id);

            if ($user->checkExpiredRentals()) {
                return $this->response
                    ->error('User has unreturned expired rentals.', 405);
            }

            if ($user->checkRentalCapacity()) {
                $rental = new Rental($request->all());

                $rental->status = $request->status ?? Rental::STATUS_ACTIVE;

                $rental->user()->associate($user);
                $rental->copy()->associate(Copy::find($request->copy_id));

                if ($rental->save()) {
                    return $this->response
                        ->item($rental, new RentalTransformer);
                } else {
                    return $this->response->errorInternal();
                }
            } else {
                return $this->response
                    ->error('User has exceeded his rental capacity.', 405);
            }
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Update rental
     *
     * [Update new rental]
     * @bodyParam date_rental date Rental date.
     * @bodyParam date_expiry date Expiration date.
     * @bodyParam user_id Int User id.
     * @bodyParam copy_id Int Copy id.
     * @bodyParam status Int [1 => active, 2 => expired, 3 => returned, 4 => lost].
     *
     */
    public function update(Rental $rental, Request $request)
    {
        if (empty($request->all())) {
            return $this->response
                ->item($rental, new RentalTransformer);
        }

        if (isset($request->user_id) && $request->user_id != $rental->user->id) {
            $user = User::find($request->user_id);

            if (!$user->checkRentalCapacity()) {
                return $this->response
                    ->error('User has exceeded his rental capacity.', 405);
            } else {
                $user = $rental->user;
            }

            if ($user->checkExpiredRentals()) {
                return $this->response
                    ->error('User has unreturned expired rentals.', 405);
            }
        }

        $validatedData = $request->validate([
            'date_rental' => 'date | date_format:"Y-m-d"',
            'date_expiry' => 'date | date_format:"Y-m-d"| after_or_equal:' . date('Y-m-d', strtotime($request->date_rental)),
            'user_id' => 'integer',
            'copy_id' => 'integer',
            'status' => 'integer',
        ]);
        if ($validatedData) {
            if (isset($user)) {
                $rental->user()->associate($user);
            }

            if (isset($request->copy_id)) {
                $rental->copy()->associate(Copy::find($request->copy_id));
            }

            $rental->update($request->all());

            return $this->response
                ->item($rental, new RentalTransformer);
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Delete rental
     *
     * [Delete rental by given id]
     * @queryParam id int required Id of the rental.
     *
     */
    public function delete(Rental $rental)
    {
        if ($rental->delete()) {
            return $this->response->noContent();
        } else {
            return $this->response->errorInternal();
        }
    }
}

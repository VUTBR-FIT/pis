<?php

namespace App\Http\Controllers;

use App\Token;
use App\Transformers\TokenTransformer;
use App\Transformers\UserTransformer;
use App\User;
use Dingo\Api\Auth\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group Authorization
 *
 * API for authorization
 */
class AuthController extends Controller
{

    /**
     * Authenticate user
     *
     * [Authetinticate user]
     * @bodyParam email email required User email.
     * @bodyParam password String required User password.
     *
     */
    public function authenticate(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required | email',
            'password' => 'required | string | min:6',
        ]);

        if ($validatedData) {
            $credentials = $request->only('email', 'password');

            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            if($token) {
                $user = User::where('email', $request->email)->first();
            }

            return $this->response->item(new Token($token), new TokenTransformer($user));
        } else {
                return $this->response
                    ->collection($validatedData->errors()->all());
            }
    }

    /**
     * Create user
     *
     * [Create new user]
     * @bodyParam first_name String required User first name.
     * @bodyParam middle_name String User middle name.
     * @bodyParam last_name String required User last name.
     * @bodyParam email email required User email.
     * @bodyParam date_birth date required User birth date in Y-m-d format.
     * @bodyParam password string required User password > 6.
     *
     */
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required | string | min:1 | max:191',
            'middle_name' => 'string | min:1 | max:191',
            'last_name' => 'required | string | min:1 | max:191',
            'email' => 'required | email | unique:users,email',
            'date_birth' => 'required | date | date_format:"Y-m-d"| before_or_equal:' . date('Y-m-d'),
            'password' => 'required | string | min:6',
            'rental_capacity' => 'integer',
            'reservation_capacity' => 'integer',
        ]);

        if ($validatedData) {
            if (!User::where('email', $request->email)->first()) {
                $user = new User([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'middle_name' => $request->middle_name,
                    'date_birth' => $request->date_birth,
                    'password' => bcrypt($request->password),
                    'email' => $request->email,
                    'rental_capacity' => $request->rental_capacity,
                    'reservation_capacity' => $request->reservation_capacity,
                ]);
            } else {
                return $this->response->error('Email is already used.', 409);
            }

            $user->role = User::ROLE_EXTERNAL;


            if ($user->save()) {
                $credentials = [
                    'email' => $request->email,
                    'password' => $request->password
                ];

                try {
                    if (!$token = JWTAuth::attempt($credentials)) {
                        return response()->json(['error' => 'invalid_credentials'], 401);
                    }
                } catch (JWTException $e) {
                    return response()->json(['error' => 'could_not_create_token'], 500);
                }

                return $this->response->item(new Token($token), new TokenTransformer($user));
            } else {
                return $this->response->errorInternal();
            }
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Refresh
     *
     * [Refresh user token]
     *
     */
    public function refresh() {
        try {
            $token = JWTAuth::getToken();
            if (!$token) {
                return response()->json(['error' => "Token not provided"], 403);
            }
            $newToken = JWTAuth::refresh($token);
        } catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['error' => $e->getMessage()], 401);
        }
        return $this->response->item(new Token($newToken), new TokenTransformer());
    }

    /**
     * Logout
     *
     * [Logs user out and deletes his access token]
     * @return \Dingo\Api\Http\Response
     */
    public function logout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());
        return $this->response->noContent();
    }
}
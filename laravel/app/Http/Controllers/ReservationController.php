<?php

namespace App\Http\Controllers;

use App\Rental;
use App\Reservation;
use App\Title;
use App\Transformers\ReservationTransformer;
use App\User;
use Illuminate\Http\Request;
use NunoMaduro\Collision\Provider;
use Dingo\Api\Exception\UpdateResourceFailedException;

/**
 * @group Reservations
 *
 * API for reservations
 */
class ReservationController extends Controller
{
    /**
     * Index reservations
     *
     * [Returns all reservations]
     *
     */
    public function index()
    {
        if ($this->auth->user()->role == User::ROLE_EXTERNAL) {
            $response = $this->response
                ->collection(Reservation::where('user_id', $this->auth->user()->id)
                    ->with('user', 'title')->get(), new ReservationTransformer);
        } else {
            $response = $this->response
                ->collection(Reservation::with('user', 'title')
                    ->get(), new ReservationTransformer);
        }

        return $response;
    }

    /**
     * Show reservation
     *
     * [Show reservation by given id]
     * @queryParam id int required Id of the reservation.
     *
     */
    public function show(Reservation $reservation)
    {
        if ($this->auth->user()->role == User::ROLE_EXTERNAL && $this->auth->user()->id != $reservation->user_id) {
            $response = $this->response->errorUnauthorized();
        } else {
            $response = $this->response
                ->item($reservation->load('user', 'title'), new ReservationTransformer);
        }

        return $response;
    }

    /**
     * Create reservation
     *
     * [Create new reservation]
     * @bodyParam date_reservation Date required Reservation date.
     * @bodyParam date_expiry Date required Reservation expiration date.
     * @bodyParam user_id Int required User id.
     * @bodyParam title_id Int required Title id.
     * @bodyParam status Int [1 => active, 2 => expired, 3 => cancelled].
     *
     */
    public function create(Request $request)
    {

        $validatedData = $request->validate([
            'date_reservation' => 'required | date | date_format:"Y-m-d"',
            'date_expiry' => 'required | date | date_format:"Y-m-d"| after_or_equal:' . date('Y-m-d', strtotime($request->date_reservation)),
            'user_id' => 'required | integer',
            'title_id' => 'required | integer',
            'status' => 'integer',
        ]);

        if ($validatedData) {
            $user = User::find($request->user_id);

            if ($user->checkExpiredRentals()) {
                return $this->response
                    ->error('User has unreturned expired rentals.', 405);
            }

            if ($user->checkReservationCapacity()) {
                $reservation = new Reservation($request->all());

                $reservation->status = $request->status ?? Reservation::STATUS_ACTIVE;

                $reservation->user()->associate($user);
                $reservation->title()->associate(Title::find($request->title_id));

                if ($reservation->save()) {
                    return $this->response
                        ->item($reservation, new ReservationTransformer);
                } else {
                    return $this->response->errorInternal();
                }
            } else {
                return $this->response->error('User has exceeded his reservation capacity.', 405);
            }
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Update reservation
     *
     * [Update current reservation]
     * @bodyParam date_reservation Date Reservation date.
     * @bodyParam date_expiry Date Reservation expiration date.
     * @bodyParam user_id Int User id.
     * @bodyParam title_id Int Title id.
     * @bodyParam status Int [1 => active, 2 => expired, 3 => cancelled].
     *
     */
    public function update(Reservation $reservation, Request $request)
    {
        if (empty($request->all())) {
            return $this->response
                ->item($reservation, new ReservationTransformer);
        }

        if (isset($request->user_id) && $request->user_id != $reservation->user->id) {
            $user = User::find($request->user_id);

            if (!$user->checkReservationCapacity()) {
                return $this->response
                    ->error('User has exceeded his reservation capacity.', 405);
            }
        } else {
            $user = $reservation->user;
        }

        if ($user->checkExpiredRentals()) {
            return $this->response
                ->error('User has unreturned expired rentals.', 405);
        }

        $validatedData = $request->validate([
            'date_reservation' => ' date | date_format:"Y-m-d"',
            'date_expiry' => 'date | date_format:"Y-m-d"| after_or_equal:' . date('Y-m-d', strtotime($request->date_reservation)),
            'user_id' => 'integer',
            'title_id' => 'integer',
            'status' => 'integer',
        ]);

        if ($validatedData) {
            $reservation->user()->associate($user);

            if (isset($request->title_id)) {
                $reservation->title()->associate(Title::find($request->title_id));
            }

            $reservation->update($request->all());

            return $this->response
                ->item($reservation, new ReservationTransformer);
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Delete reservation
     *
     * [Delete reservation by given id]
     * @queryParam id int required Id of the reservation.
     *
     */
    public function delete(Reservation $reservation)
    {
        if ($reservation->delete()) {
            return $this->response->noContent();
        } else {
            return $this->response->errorInternal();
        }
    }
}

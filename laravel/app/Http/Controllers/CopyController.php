<?php

namespace App\Http\Controllers;

use App\Copy;
use App\Title;
use App\Transformers\CopyTransformer;
use Illuminate\Http\Request;
use NunoMaduro\Collision\Provider;
use Dingo\Api\Exception\UpdateResourceFailedException;

/**
 * @group Copy
 *
 * API for copies
 */
class CopyController extends Controller
{
    /**
     * Index copy
     *
     * [Returns all copies]
     *
     */
    public function index()
    {
        return $this->response
            ->collection(Copy::with('title', 'rentals')
                ->get(), new CopyTransformer);
    }

    /**
     * Show copy
     *
     * [Show copy by given id]
     * @queryParam id int required Id of the copy.
     *
     */
    public function show(Copy $copy)
    {
        return $this->response
            ->item($copy->load('title'), new CopyTransformer);
    }

    /**
     * Create copy
     *
     * [Create new copy]
     * @bodyParam health_percentage numeric required Copy health.
     * @bodyParam title_id integer Copy title id.
     *
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'health_percentage' => 'required | numeric | max:1.0',
            'title_id' => 'required | integer',
        ]);

        if ($validatedData) {
            $copy = new Copy($request->all());
            $copy->title()->associate(Title::find($request->title_id));

            if($copy->health_percentage > 1) {
                $copy->health_percentage = 1;
            }

            if($copy->save()) {
                return $this->response
                    ->item($copy, new CopyTransformer);
            } else {
                return $this->response->errorInternal();
            }
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Update copy
     *
     * [Update copy]
     * @bodyParam health_percentage numeric Copy health.
     * @bodyParam title_id integer Copy title id.
     *
     */
    public function update(Copy $copy, Request $request)
    {
        if (empty($request->all())) {
            return $this->response
                ->item($copy, new CopyTransformer);
        }
        $validatedData = $request->validate([
            'health_percentage' => 'numeric | max:1.0',
            'title_id' => 'integer',
        ]);

        if ($validatedData) {

            if(isset($request->title_id)) {
                $copy->title()->associate(Title::find($request->title_id));
            }
            $copy->fill($request->all());

            if($copy->health_percentage > 1) {
                $copy->health_percentage = 1;
            }

            $copy->update();

            return $this->response
                ->item($copy, new CopyTransformer);
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Delete copy
     *
     * [Delete copy by given id]
     * @queryParam id int required Id of the copy.
     *
     */
    public function delete(Copy $copy)
    {
        if ($copy->delete()) {
            return $this->response->noContent();
        } else {
            return $this->response->errorInternal();
        }
    }
}

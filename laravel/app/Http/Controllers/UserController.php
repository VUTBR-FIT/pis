<?php

namespace App\Http\Controllers;

use App\Transformers\UserTransformer;
use App\User;
use Illuminate\Http\Request;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Validation\Rule;

/**
 * @group Users
 *
 * API for users
 */
class UserController extends Controller
{
    /**
     * Index users
     *
     * [Returns all users]
     *
     */
    public function index()
    {

        if ($this->auth->user()->role == User::ROLE_EXTERNAL) {
            $response = $this->response
                ->item($this->auth->user()
                    ->load('reservations', 'rentals'), new UserTransformer);
        } else {
            $response = $this->response
                ->collection(User::with('reservations', 'rentals')
                    ->get(), new UserTransformer);
        }

        return $response;
    }

    /**
     * Show user
     *
     * [Show user by given id]
     * @queryParam id int required Id of the user.
     *
     */
    public function show(User $user)
    {
        if ($this->auth->user()->role == User::ROLE_EXTERNAL
            && $user->id != $this->auth->user()->id) {

            $response = $this->response->errorUnauthorized();
        } else {
            $response = $this->response
                ->item($user->load('reservations', 'rentals'), new UserTransformer);
        }

        return $response;
    }

    /**
     * Create user
     *
     * [Create new user]
     * @bodyParam first_name String required Firstname.
     * @bodyParam middle_name String User middle name.
     * @bodyParam last_name String required User last name.
     * @bodyParam email email required User email.
     * @bodyParam date_birth Date required User birth date in Y-m-D format.
     * @bodyParam reservation_capacity Int required User reservation capacity.
     * @bodyParam rental_capacity Int required User rental capacity.
     * @bodyParam password String required User password
     * @bodyParam role Int required User role
     *
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required | string | min:1 | max:191',
            'middle_name' => 'string | min:1 | max:191',
            'last_name' => 'required | string | min:1 | max:191',
            'email' => 'required | email | unique:users,email',
            'date_birth' => 'required | date | date_format:"Y-m-d"| before_or_equal:' . date('Y-m-d'),
            'reservation_capacity' => 'integer | min:0',
            'rental_capacity' => 'integer | min:0',
            'password' => 'required | string | min:6',
            'role' => 'required | integer | min:' . User::ROLE_ADMIN . ' | max:' . User::ROLE_EXTERNAL,
        ]);

        if ($validatedData) {
            if (
                $this->auth->user()->role == User::ROLE_ADMIN
                || $this->auth->user()->role == User::ROLE_EMPLOYEE && $request->role == User::ROLE_EXTERNAL
            ) {
                $user = new User($request->all());
                $user->password = bcrypt($request->password);
                $user->date_birth = $request->date_birth;

                $user->rental_capacity = $request->rental_capacity ?? 0;
                $user->reservation_capacity = $request->reservation_capacity ?? 0;

                $user->save();

                return $this->response
                    ->item($user, new UserTransformer);
            } else {
                return $this->response->errorUnauthorized();
            }
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Update user
     *
     * [Update new user]
     * @bodyParam first_name String Firstname.
     * @bodyParam middle_name String User middle name.
     * @bodyParam last_name String User last name.
     * @bodyParam email email User email.
     * @bodyParam date_birth Date User birth date in Y-m-D format.
     * @bodyParam reservation_capacity Int User reservation capacity.
     * @bodyParam rental_capacity Int User rental capacity.
     * @bodyParam password String User password
     * @bodyParam role Int User role
     *
     */
    public function update(User $user, Request $request)
    {
        if (
            ($this->auth->user()->role == User::ROLE_EXTERNAL && $user->id == $this->auth->user()->id)
            || ($this->auth->user()->role == User::ROLE_EMPLOYEE
                && ($user->role == User::ROLE_EXTERNAL || $this->auth->user()->id == $user->id))
            || $this->auth->user()->role == User::ROLE_ADMIN
        ) {
            if (empty($request->all())) {
                return $this->response
                    ->item($user, new UserTransformer);
            }
            $validatedData = $request->validate([
                'first_name' => 'string | min:1 | max:191',
                'middle_name' => 'string | min:1 | max:191',
                'last_name' => 'string | min:1 | max:191',
                'email' => ['email', Rule::unique('users', 'email')->ignore($user->id)],
                'date_birth' => 'date | date_format:"Y-m-d"| before_or_equal:' . date('Y-m-d'),
                'reservation_capacity' => 'integer | min:0',
                'rental_capacity' => 'integer | min:0',
                'password' => 'string | min:6',
                'role' => 'integer | min:' . User::ROLE_ADMIN . ' | max:' . User::ROLE_EXTERNAL
            ]);

            if ($validatedData) {
                $user->fill($request->all());
                if (isset($request->password)) {
                    $user->password = bcrypt($request->password);
                }
                if (isset($request->date_birth)) {
                    $user->date_birth = $request->date_birth;
                }

                $user->update();

                return $this->response
                    ->item($user, new UserTransformer);
            } else {
                return $this->response
                    ->collection($validatedData->errors()->all());
            }
        } else {
            return $this->response->errorUnauthorized();
        }
    }

    /**
     * Delete user
     *
     * [Delete user by given id]
     * @queryParam id int required Id of the user.
     *
     */
    public function delete(User $user)
    {
        if (
            ($this->auth->user()->role == User::ROLE_EXTERNAL && $user->id == $this->auth->user()->id)
            || ($this->auth->user()->role == User::ROLE_EMPLOYEE
                && ($user->role == User::ROLE_EXTERNAL || $this->auth->user()->id == $user->id))
            || $this->auth->user()->role == User::ROLE_ADMIN
        ) {
            if ($user->delete()) {
                return $this->response->noContent();
            } else {
                return $this->response->errorInternal();
            }
        } else {
            return $this->response
                ->errorUnauthorized();
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Author;
use App\Transformers\AuthorTransformer;
use Illuminate\Http\Request;
use Dingo\Api\Exception\UpdateResourceFailedException;

/**
 * @group Authors
 *
 * API for authors
 */
class AuthorController extends Controller
{
    /**
     * Index authors
     *
     * [Returns all authors]
     *
     */
    public function index()
    {
        return $this->response
            ->collection(Author::with('titles')
                ->get(), new AuthorTransformer);
    }

    /**
     * Show author
     *
     * [Show author by given id]
     * @queryParam id int required Id of the author.
     *
     */
    public function show(Author $author)
    {
        return $this->response
            ->item($author->load('titles'), new AuthorTransformer);
    }

    /**
     * Create author
     *
     * [Create new author]
     * @bodyParam first_name String required firstname.
     * @bodyParam middle_name String Author middle name.
     * @bodyParam last_name String required Author last name.
     * @bodyParam date_birth date required Birth date.
     * @bodyParam date_death date Death date.
     * @bodyParam description string required Description.
     * @bodyParam titles string Titles.
     *
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required | string | min:1 | max:191',
            'last_name' => 'required | string | min:1 | max:191',
            'middle_name' => 'string | min:1 | max:191',
            'date_birth' => 'required | date | date_format:"Y-m-d"| before_or_equal:' . date('Y-m-d'),
            'date_death' => 'date | date_format:"Y-m-d"| after_or_equal:' . date('Y-m-d', strtotime($request->date_birth)) . '| before:' . date('Y-m-d'),
            'description' => 'required | string | min:1 | max:191',
            'titles' => 'array'
        ]);

        if ($validatedData) {
            $author = new Author();

            $author->fill($request->all());
            $author->date_birth = $request->date_birth;
            $author->date_death = $request->date_death;


            if ($author->save()) {

                $author->titles()->sync($request->titles);

                return $this->response
                    ->item($author, new AuthorTransformer);
            } else {
                return $this->response->errorInternal();
            }
        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Create author
     *
     * [Create new author]
     * @bodyParam first_name String Author Firstname.
     * @bodyParam middle_name String Author middle name.
     * @bodyParam last_name String Author last name.
     * @bodyParam date_birth date Birth date.
     * @bodyParam date_death date Death date.
     * @bodyParam description string Description.
     * @bodyParam titles string Titles.
     *
     */
    public function update(Author $author, Request $request)
    {
        if (empty($request->all())) {
            return $this->response
                ->item($author, new AuthorTransformer);
        }
        $validatedData = $request->validate([
            'first_name' => 'string | min:1 | max:191',
            'last_name' => 'string | min:1 | max:191',
            'middle_name' => 'string | min:1 | max:191',
            'date_birth' => 'date | date_format:"Y-m-d"| before_or_equal:' . date('Y-m-d'),
            'date_death' => 'nullable | date | date_format:"Y-m-d"| after_or_equal:' . date('Y-m-d', strtotime($request->date_birth)) . '| before:' . date('Y-m-d'),
            'description' => 'string | min:1 | max:191',
            'titles' => 'array'
        ]);

        if ($validatedData) {
            $author->fill($request->all());
            if (isset($request->date_birth)) {
                $author->date_birth = $request->date_birth;
            }

            if (isset($request->date_death)) {
                $author->date_death = $request->date_death;
            }

            if (isset($request->titles)) {
                if (isset($request->titles)) {
                    $author->titles()->sync($request->titles);
                }
            }

            if ($author->update()) {
                return $this->response
                    ->item($author, new AuthorTransformer);
            } else {
                return $this->response->errorInternal();
            }


        } else {
            return $this->response
                ->collection($validatedData->errors()->all());
        }
    }

    /**
     * Delete author
     *
     * [Delete author by given id]
     * @queryParam id int required Id of the author.
     *
     */
    public function delete(Author $author)
    {
        try {
            if ($author->delete()) {
                return $this->response->noContent();
            } else {
                return $this->response->errorInternal();
            }
        } catch (\Exception $e) {
            return $this->response->errorInternal($e->getMessage());
        }
    }
}

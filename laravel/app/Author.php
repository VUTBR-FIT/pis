<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'middle_name', 'date_birth', 'date_death', 'description'
    ];

    // Eloquent relations definition
    public function titles()
    {
        return $this->belongsToMany(Title::class);
    }
}

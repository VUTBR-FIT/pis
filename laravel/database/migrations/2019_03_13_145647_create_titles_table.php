<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title_name');
            $table->string('isbn');
            $table->bigInteger('ean');
            $table->date('published_at');
            $table->string('description');
            $table->string('publisher');
            $table->enum('medium', [
                1 => 'Book',
                2 => 'Magazine',
                3 => 'Audiobook',
                4 => 'CD',
                5 => 'Other'
            ])->default('Book');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('titles');
    }
}

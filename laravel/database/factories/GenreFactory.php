<?php

use Faker\Generator as Faker;

$factory->define(\App\Genre::class, function (Faker $faker) {
    return [
        'genre_name' => $faker->text(10),
        'age_restriction' => $faker->numberBetween(12, 18),
        'description' => $faker->text(191)
    ];
});

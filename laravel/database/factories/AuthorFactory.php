<?php

use Faker\Generator as Faker;

$factory->define(\App\Author::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'middle_name' => $faker->firstName,
        'date_birth' => $faker->dateTimeBetween('-100 years', '-50 years'),
        'date_death' => $faker->dateTimeBetween('-50 years', 'now'),
        'description' => $faker->text(191)
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(\App\Reservation::class, function (Faker $faker) {
    return [
        'date_reservation' => $faker->dateTimeBetween('-1 year', 'now'),
        'date_expiry' => $faker->dateTimeBetween('-11 months', '+1 month'),
        'status' => random_int(\App\Reservation::STATUS_ACTIVE, \App\Reservation::STATUS_CANCELED),
        'title_id' => factory(\App\Title::class)->lazy(),
        'user_id' => factory(\App\User::class)->lazy()
    ];
});

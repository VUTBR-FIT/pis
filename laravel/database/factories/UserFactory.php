<?php

use Faker\Generator as Faker;

$factory->define(\App\User::class, function (Faker $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'middle_name' => $faker->firstName,
        'email' => $faker->email,
        'password' => $password ?: $password = bcrypt('111111'),
        'date_birth' => $faker->dateTimeBetween('-50 years', 'now'),
        'rental_capacity' => $faker->randomNumber(2),
        'reservation_capacity' => $faker->randomNumber(2),
        'role' => \App\User::ROLE_EXTERNAL
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(\App\Rental::class, function (Faker $faker) {
    return [
        'date_rental' => $faker->dateTimeBetween('-1 year', 'now'),
        'date_expiry' => $faker->dateTimeBetween('-11 months', '+1 month'),
        'status' => random_int(\App\Rental::STATUS_ACTIVE, \App\Rental::STATUS_LOST),
        'copy_id' => factory(\App\Copy::class)->lazy(),
        'user_id' => factory(\App\User::class)->lazy(),
    ];
});

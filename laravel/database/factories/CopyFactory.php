<?php

use Faker\Generator as Faker;

$factory->define(\App\Copy::class, function (Faker $faker) {
    return [
        'health_percentage' => $faker->randomFloat(2, 0, 1),
        'title_id' => factory(\App\Title::class)->lazy()
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(\App\Title::class, function (Faker $faker) {
    return [
        'title_name' => $faker->text(15),
        'isbn' => $faker->randomNumber(),
        'ean' => $faker->randomNumber(),
        'published_at' => $faker->date(),
        'description' => $faker->text(191),
        'publisher' => $faker->name,
        'medium' => 'Book',
    ];
});

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(CopyTableSeeder::class);
        $this->call(GenreTableSeeder::class);
        $this->call(AuthorTableSeeder::class);
        $this->call(ReservationTableSeeder::class);
        $this->call(RentalTableSeeder::class);
        $this->call(TitleTableSeeder::class);
    }
}

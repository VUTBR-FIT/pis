<?php

use Illuminate\Database\Seeder;

class TitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Title::class, 10)->create();

        // filling join tables
        foreach (\App\Title::all() as $title) {
            $title->genres()->sync(\App\Genre::where('id', rand(1, 7))->get());
            $title->authors()->sync(\App\Author::where('id', rand(1, 20))->get());
            $title->save();
        }
    }
}

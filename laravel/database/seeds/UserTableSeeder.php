<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Admin',
            'last_name' => 'Pis',
            'email' => 'admin@pis.cz',
            'password' => bcrypt('111111'),
            'role' => \App\User::ROLE_ADMIN,
            'rental_capacity' => 5,
            'reservation_capacity' => 5,
        ]);

        DB::table('users')->insert([
            'first_name' => 'Employee',
            'last_name' => 'Pis',
            'email' => 'employee@pis.cz',
            'password' => bcrypt('222222'),
            'role' => \App\User::ROLE_EMPLOYEE,
            'rental_capacity' => 5,
            'reservation_capacity' => 5,
        ]);

        DB::table('users')->insert([
            'first_name' => 'External',
            'last_name' => 'Pis',
            'email' => 'external@pis.cz',
            'password' => bcrypt('333333'),
            'role' => \App\User::ROLE_EXTERNAL,
            'rental_capacity' => 5,
            'reservation_capacity' => 5,
        ]);

        factory(\App\User::class, 5)->create();

    }
}

<?php

use Illuminate\Database\Seeder;

class CopyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Copy::class, 40)->create();
    }
}

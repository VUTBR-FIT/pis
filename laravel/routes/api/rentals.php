<?php

$api->group(['middleware' => ['cors', 'custom.auth']], function () use ($api) {
    $api->get('/rental', [
        'uses' => 'RentalController@index',
        'as' => 'api.rental.index'
    ]);

    $api->get('/rental/{rental}', [
        'uses' => 'RentalController@show',
        'as' => 'api.rental.show'
    ]);

    $api->post('/rental', [
        'uses' => 'RentalController@create',
        'as' => 'api.rental.create'
    ]);

    $api->put('/rental/{rental}', [
        'uses' => 'RentalController@update',
        'as' => 'api.rental.update'
    ]);

    $api->delete('/rental/{rental}', [
        'uses' => 'RentalController@delete',
        'as' => 'api.rental.delete'
    ]);
});
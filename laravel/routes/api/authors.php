<?php

$api->group(['middleware' => ['cors', 'custom.auth']], function () use ($api) {
    $api->get('/author', [
        'uses' => 'AuthorController@index',
        'as' => 'api.author.index'
    ]);

    $api->get('/author/{author}', [
        'uses' => 'AuthorController@show',
        'as' => 'api.author.show'
    ]);

    $api->post('/author', [
        'uses' => 'AuthorController@create',
        'as' => 'api.author.create'
    ]);

    $api->put('/author/{author}', [
        'uses' => 'AuthorController@update',
        'as' => 'api.author.update'
    ]);

    $api->delete('/author/{author}', [
        'uses' => 'AuthorController@delete',
        'as' => 'api.author.delete'
    ]);
});

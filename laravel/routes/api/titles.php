<?php

$api->group(['middleware' => ['cors', 'custom.auth']], function () use ($api) {
    $api->get('/title', [
        'uses' => 'TitleController@index',
        'as' => 'api.title.index'
    ]);

    $api->get('/title/{title}', [
        'uses' => 'TitleController@show',
        'as' => 'api.title.show'
    ]);

    $api->get('/title/rentals/{title}', [
        'uses' => 'TitleController@rentals',
        'as' => 'api.title.rentals'
    ]);

    $api->get('/title/reservations/{title}', [
        'uses' => 'TitleController@reservations',
        'as' => 'api.title.reservations'
    ]);

    $api->post('/title', [
        'uses' => 'TitleController@create',
        'as' => 'api.title.create'
    ]);

    $api->put('/title/{title}', [
        'uses' => 'TitleController@update',
        'as' => 'api.title.update'
    ]);

    $api->delete('/title/{title}', [
        'uses' => 'TitleController@delete',
        'as' => 'api.title.delete'
    ]);
});
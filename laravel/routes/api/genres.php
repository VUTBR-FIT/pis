<?php

$api->group(['middleware' => ['cors', 'custom.auth']], function () use ($api) {
    $api->get('/genre', [
        'uses' => 'GenreController@index',
        'as' => 'api.genre.index'
    ]);

    $api->get('/genre/{genre}', [
        'uses' => 'GenreController@show',
        'as' => 'api.genre.show'
    ]);

    $api->post('/genre', [
        'uses' => 'GenreController@create',
        'as' => 'api.genre.create'
    ]);

    $api->put('/genre/{genre}', [
        'uses' => 'GenreController@update',
        'as' => 'api.genre.update'
    ]);

    $api->delete('/genre/{genre}', [
        'uses' => 'GenreController@delete',
        'as' => 'api.genre.delete'
    ]);
});

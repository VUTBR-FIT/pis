<?php

$api->group(['middleware' => ['cors', 'custom.auth']], function () use ($api) {
    $api->get('/reservation', [
        'uses' => 'ReservationController@index',
        'as' => 'api.reservation.index'
    ]);

    $api->get('/reservation/{reservation}', [
        'uses' => 'ReservationController@show',
        'as' => 'api.reservation.show'
    ]);

    $api->post('/reservation', [
        'uses' => 'ReservationController@create',
        'as' => 'api.reservation.create'
    ]);

    $api->put('/reservation/{reservation}', [
        'uses' => 'ReservationController@update',
        'as' => 'api.reservation.update'
    ]);

    $api->delete('/reservation/{reservation}', [
        'uses' => 'ReservationController@delete',
        'as' => 'api.reservation.delete'
    ]);
});
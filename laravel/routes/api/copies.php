<?php

$api->group(['middleware' => ['cors', 'custom.auth']], function () use ($api) {
    $api->get('/copy', [
        'uses' => 'CopyController@index',
        'as' => 'api.copy.index'
    ]);

    $api->get('/copy/{copy}', [
        'uses' => 'CopyController@show',
        'as' => 'api.copy.show'
    ]);

    $api->post('/copy', [
        'uses' => 'CopyController@create',
        'as' => 'api.copy.create'
    ]);

    $api->put('/copy/{copy}', [
        'uses' => 'CopyController@update',
        'as' => 'api.copy.update'
    ]);

    $api->delete('/copy/{copy}', [
        'uses' => 'CopyController@delete',
        'as' => 'api.copy.delete'
    ]);
});

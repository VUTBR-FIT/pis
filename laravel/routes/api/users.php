<?php

$api->group(['middleware' => ['cors', 'custom.auth']], function () use ($api) {
    $api->get('/user', [
        'uses' => 'UserController@index',
        'as' => 'api.user.index'
    ]);

    $api->get('/user/{user}', [
        'uses' => 'UserController@show',
        'as' => 'api.user.show'
    ]);

    $api->post('/user', [
        'uses' => 'UserController@create',
        'as' => 'api.user.create'
    ]);

    $api->put('/user/{user}', [
        'uses' => 'UserController@update',
        'as' => 'api.user.update'
    ]);

    $api->delete('/user/{user}', [
        'uses' => 'UserController@delete',
        'as' => 'api.user.delete'
    ]);
});

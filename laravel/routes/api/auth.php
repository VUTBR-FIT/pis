<?php

$api->group(['middleware' => 'cors'], function () use ($api) {
    $api->post('login', [
        'as' => 'api.auth.login',
        'uses' => 'AuthController@authenticate'
    ]);

    $api->post('register', [
        'as' => 'api.auth.register',
        'uses' => 'AuthController@register'
    ]);

    $api->post('/refresh', [
        'uses' => 'AuthController@refresh',
        'as' => 'api.auth.refresh'
    ]);
});

$api->group(['middleware' => ['cors', 'custom.auth']], function () use ($api) {
    $api->post('logout', [
        'uses' => 'AuthController@logout',
        'as' => 'api.auth.logout'
    ]);
});

# UI

## Install
```bash
npm install
```
## Run - debug mode
```bash
npm run start
http://localhost:3000
```
## Build package for production
```bash
npm run build
```
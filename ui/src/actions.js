import Dispatcher from "./Dispatcher";

class Actions {
  clearStore() {
    Dispatcher.dispatch({ type: "CLEAR" });
  }
}

export default new Actions();

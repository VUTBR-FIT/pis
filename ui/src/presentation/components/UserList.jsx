import React from "react";
import PropTypes from "prop-types";
import Plus from "@material-ui/icons/Add";

import AbstractComponent from "../SmartComponents/AbstractComponent";
import Card from "./Card/Card";
import Filter from "../SmartComponents/Filter";
import CardHeader from "./Card/CardHeader";
import CardBody from "./Card/CardBody";
import CardFooter from "./Card/CardFooter";
import { SystemContext } from "../../variables/SystemContext";
import DialogForm from "../Forms/User/Default";
import Button from "../SmartComponents/Button";
import UserStore from "../../stores/UserStore";
import Datatable from "../SmartComponents/Datatable";

export default class UserList extends AbstractComponent {
  static propTypes = {
    classes: PropTypes.object,
    allowCreate: PropTypes.bool,
    onSelect: PropTypes.func,
    filterRole: PropTypes.func,
    enableRoleFilter: PropTypes.bool,
    filter: PropTypes.object
  };
  static defaultProps = {
    filter: {},
    enableRoleFilter: true,
    allowCreate: true,
    classes: {},
    filterRole: () => true
  };

  constructor(props) {
    super(props);
    this.state.source = [];
    this.state.data = [];
    this.state.value = Object.assign({
      fulltext: () => true,
      role: props.filterRole
    }, props.filter);
    this.state.enableRole = props.enableRoleFilter;
  }

  componentDidMount() {
    UserStore.addChangeListener(this.updateData);
    this.updateData();
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this.updateData);
  }

  updateData = () => {
    this.setState({ source: UserStore.getAll() });
  };

  handleFilter = data => {
    let value = Object.assign({}, this.state.value);
    if (data.role !== undefined) {
      if (data.role === -1) {
        value.role = () => true;
      } else {
        value.role = row => data.role.localeCompare(row.role) === 0;
      }
    }
    if (data.fulltext && data.fulltext.length !== 0) {
      let filters = this.header({ user: {} });
      value.fulltext = row => {
        for (let key in filters) {
          let value = filters[key].access(row);

          if (value !== null && value !== undefined) {
            if (
              (filters[key].type === undefined ||
                filters[key].type === "string") &&
              value.toLowerCase().indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
            if (
              filters[key].type === "number" &&
              value
                .toString()
                .toLowerCase()
                .indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
          }
        }
        return false;
      };
    } else value.fulltext = () => true;
    this.setState({ value });
  };

  header = (language, forFilter) => {
    let result = [
      {
        id: "firstName",
        name: language.user.firstName,
        access: row => row.firstName
      },
      {
        id: "lastName",
        name: language.user.surname,
        access: row => row.lastName
      },
      {
        id: "role",
        name: language.user.role,
        access: row => UserStore.getRoleName(row.role),
        options: {
          filter: "select",
          options: UserStore.getRoleMapping()
        }
      }
    ];
    if (forFilter !== true)
      result.push({
        id: "email",
        name: language.user.email,
        access: row => row.email
      });
    if (this.state.enableRole === false)
      result = result.filter(item => item.id !== "role");
    if (forFilter)
      result.push({
        id: "fulltext",
        name: language.dataTable.toolbar.search,
        access: () => {},
        options: {
          filter: "search"
        }
      });
    while (result.length > 4 && forFilter) result.shift();
    return result;
  };

  render() {
    const { onSelect, allowCreate } = this.props;
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <Card>
              <CardHeader color="primary">
                <Filter
                  filters={this.header(language, true)}
                  onChange={this.handleFilter}
                />
              </CardHeader>
              <CardBody>
                <Datatable
                  data={Filter.applyFilter(this.state.source, this.state.value)}
                  keyRow={row => row.id}
                  header={this.header(language)}
                  optLanguage={language.dataTable}
                  optSortable={true}
                  optSortBy={"firstName"}
                  optSortDir={"asc"}
                  onSelectRow={onSelect}
                />
              </CardBody>
              {allowCreate ? (
                <CardFooter>
                  <DialogForm
                    buttonRender={() => props => (
                      <Button {...props} color={"primary"}>
                        <Plus /> {language.user.create}
                      </Button>
                    )}
                  />
                </CardFooter>
              ) : null}
            </Card>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

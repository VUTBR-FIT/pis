import React from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core";

import style from "../../../assets/jss/material-dashboard-react/components/footerStyle";

function Footer({ ...props }) {
  const { classes } = props;
  return (
    <footer className={classes.footer}>
      <div className={classes.container}>
        <span>&copy; {1900 + new Date().getYear()} </span>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(style)(Footer);

import React from "react";
import classNames from "classnames";
import {
  ClickAwayListener,
  Grow,
  Hidden,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  withStyles
} from "@material-ui/core";
// @material-ui/icons
import Person from "@material-ui/icons/Person";
import Button from "../SmartComponents/Button";

import headerLinksStyle from "../../assets/jss/material-dashboard-react/components/headerLinksStyle";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import UserStore from "../../stores/UserStore";
import { SystemContext } from "../../variables/SystemContext";
import Configuration from "../../variables/Configuration";
import UrlStore from "../../stores/UrlStore";

class HeaderLinks extends AbstractComponent {
  constructor(props) {
    super(props);
    this.state.open = false;
    this.state.user = {};
  }

  componentDidMount() {
    UserStore.addTypeListener("ACTION_USER", this.updateUser);
    this.updateUser();
  }
  componentWillUnmount() {
    UserStore.removeTypeListener("ACTION_USER", this.updateUser);
  }

  updateUser = () => {
    this.setState({ user: UserStore.getUser() });
  };

  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };
  handleSignOut = () => {
    UserStore.signOut();
    UrlStore.logout();
  };
  handleProfile = () => {
    Configuration.redirect(
      UrlStore.generateUrl("Admin", "UserDetail", { id: this.state.user.id })
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <>
              <div className={classes.manager}>
                <Popper
                  open={this.state.open}
                  anchorEl={this.anchorEl}
                  transition
                  disablePortal
                  className={
                    classNames({ [classes.popperClose]: !this.state.open }) +
                    " " +
                    classes.pooperNav
                  }
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom"
                      }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handleClose}>
                          <MenuList role="menu">
                            {window.innerWidth < 959 ? (
                              <MenuItem className={classes.dropdownItem}>
                                <Person className={classes.icons} />
                                {this.state.user.firstName +
                                  " " +
                                  this.state.user.lastName}
                              </MenuItem>
                            ) : null}
                            <MenuItem
                              onClick={this.handleProfile}
                              className={classes.dropdownItem}
                            >
                              {language.user.profileButton}
                            </MenuItem>
                            <MenuItem
                              onClick={this.handleSignOut}
                              className={classes.dropdownItem}
                            >
                              {language.action.signOut}
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </div>
              {window.innerWidth >= 959 ? (
                <Button
                  buttonRef={node => {
                    this.anchorEl = node;
                  }}
                  onClick={this.handleToggle}
                  color={window.innerWidth > 959 ? "transparent" : "white"}
                  justIcon={!(window.innerWidth > 959)}
                  simple={!(window.innerWidth > 959)}
                  aria-label="Person"
                  className={classes.buttonLink}
                >
                  <Person className={classes.icons} />
                  {this.state.user.firstName + " " + this.state.user.lastName}
                </Button>
              ) : (
                <Button
                  buttonRef={node => {
                    this.anchorEl = node;
                  }}
                  onClick={this.handleToggle}
                  color={window.innerWidth > 959 ? "transparent" : "white"}
                  justIcon={!(window.innerWidth > 959)}
                  simple={!(window.innerWidth > 959)}
                  aria-label="Person"
                  className={classes.buttonLink}
                >
                  <Person className={classes.icons} />
                  <Hidden mdUp implementation="css">
                    <p className={classes.linkText}>Profile</p>
                  </Hidden>
                </Button>
              )}
            </>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(headerLinksStyle)(HeaderLinks);

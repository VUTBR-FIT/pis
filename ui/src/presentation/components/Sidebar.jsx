import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import HeaderLinks from "./HeaderLinks";

import sidebarStyle from "../../assets/jss/material-dashboard-react/components/sidebarStyle";
import {
  withStyles,
  Drawer,
  Hidden,
  Icon,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import UserStore from "../../stores/UserStore";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import UrlStore from "../../stores/UrlStore";
function activeRoute(props, routeName) {
  return props.location.pathname.indexOf(routeName) > -1;
}
class Sidebar extends AbstractComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    layoutName: PropTypes.string.isRequired
  };
  constructor(props) {
    super(props);
    this.state.user = UserStore.getUser();
  }
  componentDidMount() {
    UserStore.addListener(UserStore.actionUser, this.updateUser);
  }
  componentWillUnmount() {
    UserStore.removeListener(UserStore.actionUser, this.updateUser);
  }

  updateUser = () => {
    this.setState({ user: UserStore.getUser() });
  };

  render() {
    const {
      classes,
      color,
      logo,
      open,
      image,
      logoText,
      routes,
      handleDrawerToggle,
      layoutName
    } = this.props;

    let links = (
      <List className={classes.list}>
        {routes
          .filter(route => {
            if ("permission" in route)
              return route.permission(this.state.user.role);
            return true;
          })
          .map((prop, key) => {
            if (prop.hidden === true) return null;
            var listItemClasses;
            listItemClasses = classNames({
              [" " + classes[color]]: activeRoute(this.props, prop.path)
            });
            const whiteFontClasses = classNames({
              [" " + classes.whiteFont]: activeRoute(this.props, prop.path)
            });
            return (
              <NavLink
                to={prop.path}
                className={classes.item}
                activeClassName="active"
                key={key}
              >
                <ListItem button className={classes.itemLink + listItemClasses}>
                  {typeof prop.icon === "string" ? (
                    <Icon
                      className={classNames(classes.itemIcon, whiteFontClasses)}
                    >
                      {prop.icon}
                    </Icon>
                  ) : (
                    <prop.icon
                      className={classNames(classes.itemIcon, whiteFontClasses)}
                    />
                  )}
                  <ListItemText
                    primary={prop.sidebarName}
                    className={classNames(classes.itemText, whiteFontClasses)}
                    disableTypography={true}
                  />
                </ListItem>
              </NavLink>
            );
          })}
      </List>
    );
    let brand = (
      <div className={classes.logo}>
        <a href={UrlStore.getPath(layoutName, "Dashboard")} className={classNames(classes.logoLink)}>
          <div className={classes.logoImage}>
            <img src={logo} alt="logo" className={classes.img} />
          </div>
          {logoText}
        </a>
      </div>
    );
    return (
      <div>
        <Hidden mdUp implementation="css">
          <Drawer
            variant="temporary"
            anchor={"right"}
            open={open}
            classes={{
              paper: classNames(classes.drawerPaper)
            }}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            {brand}
            <div className={classes.sidebarWrapper}>
              <HeaderLinks />
              {links}
            </div>
            {image !== undefined ? (
              <div
                className={classes.background}
                style={{ backgroundImage: "url(" + image + ")" }}
              />
            ) : null}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            anchor={"left"}
            variant="permanent"
            open
            classes={{
              paper: classNames(classes.drawerPaper)
            }}
          >
            {brand}
            <div className={classes.sidebarWrapper}>{links}</div>
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          </Drawer>
        </Hidden>
      </div>
    );
  }
}

export default withStyles(sidebarStyle)(Sidebar);

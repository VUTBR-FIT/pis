import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import {withStyles, AppBar, Toolbar, Hidden, IconButton} from "@material-ui/core";
import Menu from "@material-ui/icons/Menu";
import AdminNavbarLinks from "./HeaderLinks";
import Button from "../SmartComponents/Button";

import headerStyle from "../../assets/jss/material-dashboard-react/components/headerStyle";
function compareUrl(location, sourceRoute) {
  if (sourceRoute.exact === true) return location === sourceRoute.path;
  let reg = new RegExp(sourceRoute.path.split(/:[a-zA-Z0-9]+/g).join("[a-zA-Z0-9]+"));
  return location.match(reg) !== null;
}

function Header({ ...props }) {
  const { classes, color, admin } = props;
  function makeBrand() {
    let names = props.routes
      .map(prop => {
        if (compareUrl(props.location.pathname, prop)) {
          return prop.title;
        }
        return null;
      })
      .filter(name => name !== null);
    if (names.length) {
      return (
        <Button color="transparent" href="#" className={classes.title}>
          {names[0]}
        </Button>
      );
    }
    return null;
  }
  const appBarClasses = classNames({
    [" " + classes[color]]: color
  });
  return (
    <AppBar className={classes.appBar + appBarClasses}>
      <Toolbar className={classes.container}>
        <div className={classes.flex}>{makeBrand()}</div>
        <Hidden smDown implementation="css">
          { admin ? <AdminNavbarLinks /> : null }
        </Hidden>
        <Hidden mdUp implementation="css">
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={props.handleDrawerToggle}
          >
            <Menu />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  admin: PropTypes.bool,
  color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"])
};
Header.defaultProps = {
  admin: false
};

export default withStyles(headerStyle)(Header);

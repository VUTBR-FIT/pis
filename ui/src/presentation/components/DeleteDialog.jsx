import React from "react";
import PropTypes from "prop-types";

import Dialog from "../SmartComponents/Dialog";
import DialogContentText from "@material-ui/core/DialogContentText";
import Button from "../SmartComponents/Button";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import UserStore from "../../stores/UserStore";

export default class CustomDialog extends AbstractComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    onDelete: PropTypes.func.isRequired,
    deleteValue: PropTypes.string.isRequired,
    buttonValue: PropTypes.string.isRequired,
    buttonRender: PropTypes.func,
    userRole: PropTypes.string
  };
  static defaultProps = {
    userRole: undefined,
    buttonValue: undefined,
    buttonColor: "danger",
    buttonRender: (value, color) => props => (
        <Button {...props} color={color} >
          {value}
        </Button>
    )
  };
  constructor(props) {
    super(props);
    this.refDialog = React.createRef();
    this.state.user = {};
  }
  componentDidMount() {
    UserStore.addChangeListener(this.updateUser);
    this.updateUser();
  }
  componentWillUnmount() {
    UserStore.removeChangeListener(this.updateUser);
  }

  updateUser = () => {
    this.setState({user: UserStore.getUser()});
  };

  handleClose = () => {
    this.refDialog.handleClose();
  };

  render() {
    const { title, content, onDelete, deleteValue, buttonValue, userRole, buttonRender, buttonColor } = this.props;

    if (this.state.user.role !== "admin") {
      if (userRole !== undefined && (userRole === "employee" || userRole === "admin"))
        return null;
      if (this.state.user.role !== "employee") return null;
    }
    return (
      <Dialog
        innerRef={node => (this.refDialog = node)}
        title={title}
        fullScreen={false}
        simpleTitle
        actions={[
          <Button key="cancel" color="primary" onClick={this.handleClose} autoFocus>Cancel</Button>,
          <Button key="delete" color="primary" onClick={onDelete}>{deleteValue}</Button>
        ]}
        // button={props => <Button {...props} color={"danger"}>{buttonValue}</Button>}
        button={buttonRender(buttonValue, buttonColor)}
      >
        <DialogContentText>{content}</DialogContentText>
      </Dialog>
    );
  }
}

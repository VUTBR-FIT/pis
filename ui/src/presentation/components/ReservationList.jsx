import React from "react";
import PropTypes from "prop-types";

import { Grid, Typography } from "@material-ui/core";

import ReservationStore from "../../stores/ReservationStore";
import { SystemContext } from "../../variables/SystemContext";

import Card from "./Card/Card";
import Filter from "../SmartComponents/Filter";
import CardBody from "./Card/CardBody";
import Datatable from "../SmartComponents/Datatable";
import TitleStore from "../../stores/TitleStore";
import Button from "../SmartComponents/Button";
import CardHeader from "./Card/CardHeader";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Configuration from "../../variables/Configuration";
import UserStore from "../../stores/UserStore";
import DateUtil from "../../utils/Date";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import UrlStore from "../../stores/UrlStore";

export default class List extends AbstractComponent {
  static propTypes = {
    filter: PropTypes.object,
    displayStatus: PropTypes.bool,
    displayUser: PropTypes.bool,
    title: PropTypes.bool
  };
  static defaultProps = {
    filter: {},
    displayStatus: true,
    displayUser: true,
    title: false
  };
  constructor(props) {
    super(props);
    this.state.source = ReservationStore.getAll();
    this.state.data = [];
    this.state.titleList = TitleStore.getAll();
    this.state.value = {
      fulltext: () => true
    };
    this.state.displayStatus = props.displayStatus;
    this.state.displayUser = props.displayUser;
    this.state.isDialogOpen = false;
    this.state.row = undefined;
    this.state.user = UserStore.getUser();
  }

  componentDidMount() {
    TitleStore.addChangeListener(this.updateTitle);
    ReservationStore.addChangeListener(this.updateData);
    UserStore.addChangeListener(this.updateUser);
    this.updateData();
    this.updateUser();
  }

  componentWillUnmount() {
    TitleStore.removeChangeListener(this.updateTitle);
    ReservationStore.removeChangeListener(this.updateData);
    UserStore.removeChangeListener(this.updateUser);
  }

  updateData = () => {
    this.setState({ source: ReservationStore.getAll() });
  };

  updateUser = () => {
    this.setState({ user: UserStore.getUser() });
  };

  updateTitle = () => {
    if (this.state.row !== undefined && this.state.row.id !== undefined) {
      let row = this.state.row;
      let title = TitleStore.getById(row.title.id);
      if (title.id !== undefined) {
        row.title = title;
        this.setState({ row });
      }
    }
  };

  handleFilter = data => {
    let newState = Object.assign({}, this.state);
    Object.assign(newState.value, data);
    if (data.fulltext && data.fulltext !== "") {
      let filters = this.header({ reservation: {} });
      newState.value.fulltext = row => {
        for (let key in filters) {
          let value = filters[key].access(row);

          if (value !== null && value !== undefined) {
            if (
              (filters[key].type === undefined ||
                filters[key].type === "string") &&
              value
                .toString()
                .toLowerCase()
                .indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
            if (
              filters[key].type === "number" &&
              value
                .toString()
                .toLowerCase()
                .indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
          }
        }
        return false;
      };
    } else newState.value.fulltext = () => true;
    this.setState(newState);
  };

  extend = data => () => {
    ReservationStore.add(data.title.id, data.user.id, () => {
      let language = Configuration.langMapper.getContent(Configuration.lang);
      Configuration.addMessage(
        "primary",
        language.message["reservation_create"]
      );
    });
  };

  cancel = data => () => {
    let title = TitleStore.getById(data.title.id);
    if (title.id !== undefined) data.title = title;
    this.setState({
      isDialogOpen: true,
      row: data
    });
  };

  onDialogClose = () => {
    this.setState({
      isDialogOpen: false,
      row: undefined
    });
  };

  printCancelMessage = () => {
    let language = Configuration.langMapper.getContent(Configuration.lang);
    let row = this.state.row;
    return (
      <>
        {language.message["reservation_cancel"]}
        <br />
        <p style={{ paddingLeft: "10px", marginBottom: "7px", marginTop: "0" }}>
          {language.title.titleName}: {row.title.name || row.title.titleName}{" "}
          <br />
          {language.title.isbn}: {row.title.isbn} <br />
          {language.title.ean}: {row.title.ean} <br />
        </p>
        {language.message["reservation_info"]} <br />
        <p style={{ paddingLeft: "10px", marginTop: "0" }}>
          {language.reservation.user}: {row.user.name} <br />
          {language.reservation.dateReservation}: {row.dateReservation} <br />
          {language.reservation.dateExpiry}: {row.dateExpiry}
        </p>
      </>
    );
  };

  onReservationCancel = () => {
    if (this.state.row === undefined) return;
    let _this = this;
    ReservationStore.update(
      this.state.row.id,
      { status: "cancelled" },
      () => {
        _this.onDialogClose();
        let language = Configuration.langMapper.getContent(Configuration.lang);
        Configuration.addMessage(
          "primary",
          language.message["reservation_cancel_success"]
        );
      },
      response => {
        let language = Configuration.langMapper.getContent(Configuration.lang);
        for (let type in response.errors) {
          if (language.errorMessage[type] !== undefined)
            Configuration.addMessage("danger", language.errorMessage[type]);
          else Configuration.addMessage("danger", response.errors[type]);
        }
      }
    );
  };

  header = (language, forFilter) => {
    let results = [
      {
        id: "userName",
        name: language.reservation.user,
        access: row => row.user.name
      },
      {
        id: "titleId",
        name: language.reservation.titleId,
        access: row => row.title.name
      },
      {
        id: "dateReservation",
        type: "date",
        name: language.reservation.dateReservation,
        access: row => row.dateReservation,
        accessDate: row => DateUtil.transformDate(row.dateReservation)
      },
      {
        id: "dateExpiry",
        type: "date",
        name: language.reservation.dateExpiry,
        access: row => row.dateExpiry,
        accessDate: row => DateUtil.transformDate(row.dateExpiry)
      }
    ];
    if (this.state.displayStatus)
      results.push({
        id: "status",
        type: "string",
        name: language.reservation.status,
        access: row =>
          row.status === "cancelled"
            ? "Closed"
            : row.status.charAt(0).toUpperCase() + row.status.slice(1)
      });
    if (!this.state.displayUser)
      results = results.filter(item => item.id !== "userName");
    if (forFilter)
      results.push({
        id: "fulltext",
        padding: "0px",
        name: language.dataTable.toolbar.search,
        access: () => {},
        options: {
          filter: "search"
        }
      });
    while (results.length > 4 && forFilter) results.shift();
    return results;
  };

  render() {
    const { filter, title, ...rest } = this.props;
    let _filterValue = Object.assign(this.state.value, filter);
    let _filter = language => (
      <Filter
        filters={this.header(language, true)}
        onChange={this.handleFilter}
      />
    );
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <Card>
              <CardHeader color="primary">
                {title ? (
                  <Grid container>
                    <Grid item style={{ marginTop: "10px" }}>
                      <Typography variant={"h5"} style={{ color: "white" }}>
                        {language.reservation.dashboard}
                      </Typography>
                    </Grid>
                    <Grid item style={{ flex: 4 }}>
                      {_filter(language)}
                    </Grid>
                  </Grid>
                ) : (
                  _filter(language)
                )}
              </CardHeader>
              <CardBody>
                <Datatable
                  {...rest}
                  data={Filter.applyFilter(this.state.source, _filterValue)}
                  keyRow={row => row.id}
                  header={this.header(language)}
                  optLanguage={language.dataTable}
                  optSortable={true}
                  optSortBy={this.state.displayUser ? "userName" : "titleId"}
                  optSortDir={"asc"}
                  onSelectRow={id => Configuration.redirect(
                      UrlStore.generateUrl("Admin", "ReservationDetail", { id: id })
                  )}
                />
                <Dialog
                  open={this.state.isDialogOpen}
                  onClose={this.onDialogClose}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="alert-dialog-title">
                    {"Cancel reservation?"}
                  </DialogTitle>
                  <DialogContent>
                    {this.state.row !== undefined
                      ? this.printCancelMessage()
                      : undefined}
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={this.onDialogClose}
                      color="primary"
                      autoFocus
                    >
                      Back
                    </Button>
                    <Button
                      color="primary"
                      onClick={this.onReservationCancel}
                      autoFocus
                    >
                      Cancel Reservation
                    </Button>
                  </DialogActions>
                </Dialog>
              </CardBody>
            </Card>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

import React from "react";
import PropTypes from "prop-types";
import { Grid, Typography } from "@material-ui/core";

import Datatable from "../SmartComponents/Datatable";
import Card from "./Card/Card";
import CardHeader from "./Card/CardHeader";
import Filter from "../SmartComponents/Filter";
import CardBody from "./Card/CardBody";
import { SystemContext } from "../../variables/SystemContext";
import RentalStore from "../../stores/RentalStore";
import UserStore from "../../stores/UserStore";
import Configurator from "../../variables/Configuration";
import UrlStore from "../../stores/UrlStore";
import DateUtil from "../../utils/Date";
import AbstractComponent from "../SmartComponents/AbstractComponent";

export default class List extends AbstractComponent {
  static propTypes = {
    filter: PropTypes.object,
    displayStatus: PropTypes.bool
  };
  static defaultProps = {
    filter: {},
    displayStatus: true
  };
  constructor(props) {
    super(props);
    this.state.source = RentalStore.getAll();
    this.state.displayStatus = props.displayStatus;
    this.state.value = {
      fulltext: () => true
    };
    this.state.user = UserStore.getUser();
  }

  componentDidMount() {
    RentalStore.addChangeListener(this.updateData);
    UserStore.addChangeListener(this.updateUser);
    this.updateUser();
  }

  componentWillUnmount() {
    RentalStore.removeChangeListener(this.updateData);
    UserStore.removeChangeListener(this.updateUser);
  }

  updateData = () => {
    this.setState({ source: RentalStore.getAll() });
  };

  updateUser = () => {
    this.setState({ user: UserStore.getUser() });
  };

  handleFilter = data => {
    let newState = Object.assign({}, this.state);
    Object.assign(newState.value, data);
    if (data.fulltext) {
      let filters = this.header({ rental: {} });
      newState.value.fulltext = row => {
        for (let key in filters) {
          let value = filters[key].access(row);

          if (value !== null && value !== undefined) {
            if (
              (filters[key].type === undefined ||
                filters[key].type === "string") &&
              value
                .toString()
                .toLowerCase()
                .indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
            if (
              filters[key].type === "number" &&
              value
                .toString()
                .toLowerCase()
                .indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
          }
        }
        return false;
      };
    } else newState.value.fulltext = () => {};
    this.setState(newState);
  };

  header = (language, forFilter) => {
    let results = [
      {
        id: "copy",
        name: language.rental.titleName,
        access: row => (row.copy || {}).title
      },
      {
        id: "dateStart",
        type: "date",
        name: language.rental.dateRental,
        access: row => row.dateRental,
        accessDate: row => DateUtil.transformDate(row.dateRental)
      },
      {
        id: "dateExpiry",
        type: "date",
        name: language.rental.dateExpiry,
        access: row => row.dateExpiry,
        accessDate: row => DateUtil.transformDate(row.dateExpiry)
      }
    ];
    if (this.state.displayStatus)
      results.push({
        id: "status",
        name: language.rental.status,
        access: row => row.status.charAt(0).toUpperCase() + row.status.slice(1)
      });
    if (
      this.state.user.role !== undefined &&
      this.state.user.role !== "external"
    ) {
      results.unshift({
        id: "name",
        type: "string",
        name: language.rental.name,
        access: row => row.user.name
      });
    }
    if (forFilter)
      results.push({
        id: "fulltext",
        padding: "0px",
        name: language.dataTable.toolbar.search,
        access: () => {},
        options: {
          filter: "search"
        }
      });
    while (results.length > 4 && forFilter) results.shift();
    return results;
  };

  render() {
    const { filter, title, ...rest } = this.props;
    let _filterValue = Object.assign(this.state.value, filter);
    let _filter = language => (
      <Filter
        filters={this.header(language, true)}
        onChange={this.handleFilter}
      />
    );
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <Card>
              <CardHeader color="primary">
                {title ? (
                  <Grid container>
                    <Grid item style={{ marginTop: "10px" }}>
                      <Typography variant={"h5"} style={{ color: "white" }}>
                        {language.rental.dashboard}
                      </Typography>
                    </Grid>
                    <Grid item style={{ flex: 4 }}>
                      {_filter(language)}
                    </Grid>
                  </Grid>
                ) : (
                  _filter(language)
                )}
              </CardHeader>
              <CardBody>
                <Datatable
                  {...rest}
                  data={Filter.applyFilter(this.state.source, _filterValue)}
                  keyRow={row => row.id}
                  header={this.header(language)}
                  optLanguage={language.dataTable}
                  optSortable={true}
                  optSortBy={"copy"}
                  optSortDir={"asc"}
                  onSelectRow={id =>
                    Configurator.redirect(
                      UrlStore.generateUrl("Admin", "RentalDetail", { id })
                    )
                  }
                />
              </CardBody>
            </Card>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

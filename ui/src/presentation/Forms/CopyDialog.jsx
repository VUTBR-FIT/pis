import React from "react";
import PropTypes from "prop-types";

import { SystemContext } from "../../variables/SystemContext";
import AbstractForm from "./AbstractDialogForm";
import FormGenerator from "../SmartComponents/FormGenerator/Dialog";
import Button from "../SmartComponents/Button";
import TitleStore from "../../stores/TitleStore";
import Configuration from "../../variables/Configuration";

function validateForm(id, value) {
  switch (id) {
    case "healthPercentage": return value >= 0 && value <= 100;
    default:
      return true;
  }
}
export default class Form extends AbstractForm {
  static propTypes = {
    titleId: PropTypes.number.isRequired,
    onSubmit: PropTypes.func,
    buttonValue: PropTypes.string,
    buttonColor: PropTypes.string,
    buttonRender: PropTypes.func,
    entity: PropTypes.object,
    shortIcon: PropTypes.bool
  };

  static defaultProps = {
    entity: undefined,
    buttonValue: undefined,
    buttonColor: "transparent",
    buttonRender: (value, color) => props => (
      <Button {...props} color={color}>
        {value}
      </Button>
    )
  };

  constructor(props) {
    super(props);
    this.state.titleId = props.titleId;
  }

  onSuccess = (init) => {
    let language = Configuration.langMapper.getContent(Configuration.lang);
    this.refInnerForm.handleClose();
    Configuration.addMessage("primary", init? language.message["copy_create"] : language.message["copy_edit"]);
  };

  handleSubmit(data) {
    if (this.state.entity !== undefined) {
      TitleStore.updateCopy(
        this.state.entity.id,
        data,
        () => this.onSuccess(false),
        this.onError
      );
    } else {
      TitleStore.addCopy(
        this.state.titleId,
        data.healthPercentage,
          () => this.onSuccess(true),
        this.onError
      );
    }
  }

  controls = (language, entity) => {
    let result = [
      {
        id: "healthPercentage",
        type: "number",
        label: language.copy.state,
        required: true,
        endAdornment: "%",
        value: "100"
      }
    ];
    if (entity !== undefined) {
      result[0].value = entity.healthPercentage.split(" ")[0];
    }
    return result;
  };

  render() {
    const { entity, buttonValue, buttonColor, buttonRender } = this.props;

    if (!this.checkPermission()) return null;

    let _buttonRender = buttonRender;

    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              innerRef={node => (this.refInnerForm = node)}
              title={language.copy.formTitle}
              controls={this.controls(language, entity)}
              buttons={this.buttons(language, entity)}
              openButton={_buttonRender(
                buttonValue === undefined
                  ? entity === undefined
                    ? language.copy.create
                    : language.copy.update
                  : buttonValue,
                buttonColor
              )}
              onSubmit={this.handleSubmit.bind(this)}
              validate={validateForm}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

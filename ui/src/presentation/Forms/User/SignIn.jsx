import React from "react";
import PropTypes from "prop-types";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { Avatar, Paper, Typography, withStyles } from "@material-ui/core";

import FormGenerator from "../../SmartComponents/FormGenerator/Default";

import { SystemContext } from "../../../variables/SystemContext";

import StringUtil from "../../../utils/String";
import UrlStore from "../../../stores/UrlStore";
import UserStore from "../../../stores/UserStore";
import AbstractForm from "../AbstractForm";
import Configuration from "../../../variables/Configuration";

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

function validateForm(data) {
  return (
    data.email !== undefined &&
    data.password !== undefined &&
    data.email.length > 0 &&
    data.password.length > 0 &&
    StringUtil.isEmail(data.email)
  );
}

class SignIn extends AbstractForm {
  static propTypes = {
    closeButton: PropTypes.func,
    backLink: PropTypes.string
  };
  static defaultProps = {
    backLink: UrlStore.getPath("Admin", "Dashboard")
  };

  constructor(props) {
    super(props);
    this.state.backLink = props.backLink;
  }

  handleSubmit(data) {
    if (validateForm(data))
      UserStore.signIn(data.email, data.password, this.onSuccess, this.onError);
    else this.setState({ displayError: true });
  }

  onError = () => {
    let language = Configuration.getLanguage();
    Configuration.addMessage("danger", language.errorMessage.signIn, "tc");
  };
  onSuccess = () => {};

  render() {
    const { classes } = this.props;
    let controls = language => [
      {
        id: "email",
        autoComplete: "email",
        label: language.user.email,
        required: true
      },
      {
        id: "password",
        autoComplete: "password",
        type: "password",
        label: language.user.password,
        required: true
      }
    ];
    let buttons = language => ({
      submit: language.button.signIn
    });
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <main className={classes.main}>
              <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                  <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="display1">
                  {language.pageTitle.signIn}
                </Typography>
                <FormGenerator
                  controls={controls(language)}
                  buttons={buttons(language)}
                  onSubmit={this.handleSubmit.bind(this)}
                />
              </Paper>
            </main>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SignIn);

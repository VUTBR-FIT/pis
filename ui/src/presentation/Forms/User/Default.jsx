import React from "react";
import PropTypes from "prop-types";

import UserForm from "./Abstract";
import { SystemContext } from "../../../variables/SystemContext";
import FormGenerator from "../../SmartComponents/FormGenerator/Dialog";
import Button from "../../SmartComponents/Button";
import UserStore from "../../../stores/UserStore";

export default class Form extends UserForm {
  static propTypes = {
    onSubmit: PropTypes.func,
    buttonValue: PropTypes.string,
    buttonColor: PropTypes.string,
    buttonRender: PropTypes.func,
    entity: PropTypes.object,
    userRole: PropTypes.string
  };
  static defaultProps = {
    buttonRender: (value, color) => props => (
      <Button {...props} color={color}>
        {value}
      </Button>
    )
  };
  constructor(props) {
    super(props);
    this.refInnerForm = React.createRef();
  }

  componentDidMount() {
    super.componentDidMount();
    UserStore.addChangeListener(this.update_user);
    this.update_user();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    UserStore.removeChangeListener(this.update_user);
  }

  update_user = () => {
    this.setState({
      _user: UserStore.getUser()
    })
  };

  handleSubmit(data) {
    if (data.role !== "external") {
      delete data.rentalCapacity;
      delete data.reservationCapacity;
    }
    UserForm.prototype.handleSubmit.call(this, data);
  }

  onChange = data => {
    if (data.role === "external") {
      this.enableCapacity();
    } else this.disableCapacity();
  };
  handleClose = () => {
    this.refInnerForm.handleClose();
  };

  userControls = (translate, entity) => {
    let results = this.controls(translate, entity);
    if (this.state._user && entity) {
      if (this.state._user.id === entity.id) {
        if (this.state._user.role !== "admin")
          results = results.filter(item => item.id !== "role");
        if (this.state._user.role === "external")
          results = results.filter(item => item.id !== "rentalCapacity" && item.id !== "reservationCapacity");
      } else results = results.filter(item => item.id !== "password" && item.id !== "passwordAgain")
    }
    return results;
  };

  render() {
    const { entity, buttonValue, buttonColor, buttonRender, userRole } = this.props;

    if (!this.checkPermission() && this.state.entity && this.state._user.id !== this.state.entity.id) return null;

    if (this.state._user.role !== "admin") {
      if (userRole !== undefined && ((userRole === "employee" || userRole === "admin") && entity.id !== this.state._user.id))
        return null;
    }

    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              innerRef={node => (this.refInnerForm = node)}
              title={language.user.formTitle}
              controls={this.userControls(language, entity)}
              buttons={this.buttons(language, entity)}
              openButton={buttonRender(
                buttonValue === undefined
                  ? entity === undefined
                    ? language.button.create
                    : language.button.update
                  : buttonValue,
                buttonColor
              )}
              onSubmit={this.handleSubmit.bind(this)}
              validate={this.validateForm}
              onChange={this.onChange}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

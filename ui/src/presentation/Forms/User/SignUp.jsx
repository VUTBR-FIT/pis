import React from "react";
import PropTypes from "prop-types";
import Icon from "@material-ui/icons/VerifiedUser";

import { Avatar, Paper, Typography, withStyles } from "@material-ui/core";
import UserForm from "./Abstract";
import FormGenerator from "../../SmartComponents/FormGenerator/Default";
import { SystemContext } from "../../../variables/SystemContext";
import UrlStore from "../../../stores/UrlStore";

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});
class SignUp extends UserForm {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    backLink: PropTypes.string
  };
  static defaultProps = {
    backLink: UrlStore.getPath("Admin", "Dashboard")
  };

  constructor(props) {
    super(props);
    this.isSignUp = true;
  }

  render() {
    const { classes } = this.props;
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <main className={classes.main}>
              <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                  <Icon />
                </Avatar>
                <Typography component="h1" variant="display1">
                  {language.pageTitle.signUp}
                </Typography>
                <FormGenerator
                  controls={this.controls(language)}
                  buttons={{ submit: language.button.signUp }}
                  onSubmit={this.handleSubmit.bind(this)}
                  validate={this.validateForm}
                />
              </Paper>
            </main>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(styles)(SignUp);

import PropTypes from "prop-types";

import StringUtil from "../../../utils/String";
import UserStore from "../../../stores/UserStore";
import UrlStore from "../../../stores/UrlStore";
import AbstractDialog from "../AbstractDialogForm";

export default class Form extends AbstractDialog {
  static propTypes = {
    backLink: PropTypes.string,
    entity: PropTypes.object
  };
  static defaultProps = {
    entity: undefined,
    backLink: UrlStore.getPath("Admin", "Dashboard")
  };

  constructor(props) {
    super(props);
    this.state.backLink = props.backLink;
    this.state.enableCapacity = true;
    this.isSignUp = false;
  }

  componentWillReceiveProps(nextProps, nextContext) {
    super.componentWillReceiveProps(nextProps, nextContext);
    if (nextProps.entity !== undefined && nextProps.entity.role !== undefined) {
      if (nextProps.entity.role === "external") this.enableCapacity();
      else this.disableCapacity();
    }
  }

  handleClose = () => {};
  onSuccess = () => {
    if (!this.isSignUp) this.handleClose();
  };

  disableCapacity() {
    this.setState({ enableCapacity: false });
  }
  enableCapacity() {
    this.setState({ enableCapacity: true });
  }
  validateForm(id, value, values) {
    switch (id) {
      case "email":
        return StringUtil.isEmail(value);
      case "passwordAgain":
        return values.password === value;
      case "reservationCapacity":
      case "rentalCapacity":
        return values.role !== "external" || value >= 0;
      default:
        return true;
    }
  }

  onChange = data => {};

  handleSubmit(data) {
    if (this.state.entity !== undefined) {
      data.role = UserStore.getRole(data.role).numId;
      UserStore.update(
        this.state.entity.id,
        data,
        this.onSuccess,
        this.onError
      );
    } else {
      if (this.isSignUp) {
        UserStore.signUp(data, this.onSuccess, this.onError);
      } else {
        data.role = UserStore.getRole(data.role).numId;
        UserStore.add(data, this.onSuccess, this.onError);
      }
    }
  }

  controls = (translate, entity) => {
    let result = [
      {
        id: "firstName",
        label: translate.user.name,
        required: true
      },
      {
        id: "middleName",
        label: translate.user.middleName
      },
      {
        id: "lastName",
        label: translate.user.surname,
        required: true,
        autoComplete: "family-name"
      },
      {
        id: "email",
        label: translate.user.email,
        required: true
      },
      {
        id: "password",
        type: "password",
        label: translate.user.password,
        required: entity === undefined
      },
      {
        id: "passwordAgain",
        type: "password",
        label: translate.user.passwordAgain,
        required: entity === undefined
      },
      {
        id: "dateBirth",
        type: "date",
        label: translate.user.dateOfBirth,
        required: entity === undefined,
        autoComplete: "bday"
      }
    ];
    if (this.isSignUp === false) {
      result.push({
        id: "role",
        type: "select",
        label: translate.user.role,
        emptyValue: false,
        required: true,
        options: UserStore.getRoleMapping(),
        value: "external"
      });
      result.push({
        disable: !this.state.enableCapacity,
        id: "reservationCapacity",
        type: "number",
        label: translate.user.reservationCapacity,
        required: this.state.enableCapacity,
        value: 10
      });
      result.push({
        disable: !this.state.enableCapacity,
        id: "rentalCapacity",
        type: "number",
        label: translate.user.rentalCapacity,
        required: this.state.enableCapacity,
        value: 10
      });
    }
    if (entity !== undefined) {
      result[0].value = entity.firstName;
      result[1].value = entity.middleName;
      result[2].value = entity.lastName;
      result[3].value = entity.email;
      result[4].autoComplete = "";
      result[6].autoComplete = "";
      if (entity.dateBirth) {
        let temp = entity.dateBirth.split(/\.[\s]+/);
        result[6].value = temp[2] + "-" + temp[1] + "-" + temp[0];
      }
      result[7].value = entity.role;
      result[8].value = entity.reservationCapacity;
      result[9].value = entity.rentalCapacity;
    }
    return result;
  };
}

import React from "react";
import PropTypes from "prop-types";

import Button from "../SmartComponents/Button";
import AbstractDialogForm from "./AbstractDialogForm";
import UrlStore from "../../stores/UrlStore";
import { SystemContext } from "../../variables/SystemContext";
import FormGenerator from "../SmartComponents/FormGenerator/Dialog";
import AuthorStore from "../../stores/AuthorStore";
import DateUtil from "../../utils/Date";
import Configuration from "../../variables/Configuration";

function validateForm(id, value, values) {
  switch (id) {
    case "dateBirth":
    case "dateDeath":
      if (values.dateDeath !== undefined && values.dateDeath.length > 0) {
        return (new Date(values.dateBirth)) <= (new Date(values.dateDeath));
      }
      return true;
    default:
      return true;
  }
}
export default class Form extends AbstractDialogForm {
  static propTypes = {
    onSubmit: PropTypes.func,
    buttonValue: PropTypes.string,
    buttonColor: PropTypes.string,
    buttonRender: PropTypes.func,
    entity: PropTypes.object
  };

  static defaultProps = {
    entity: undefined,
    buttonValue: undefined,
    buttonColor: "transparent",
    buttonRender: (value, color) => props => (
      <Button {...props} color={color}>
        {value}
      </Button>
    ),
    backLink: UrlStore.getPath("Admin", "Dashboard")
  };

  onSuccess = (response, init) => {
    this.refInnerForm.handleClose();
    Configuration.redirect(UrlStore.generateUrl("Admin", "AuthorDetail", {id: response.id}));
    let language = Configuration.langMapper.getContent(Configuration.lang);
    Configuration.addMessage("primary", init? language.message["author_create"]: language.message["author_edit"]);
  };

  handleSubmit(data) {
    if (this.state.entity !== undefined) {
      AuthorStore.update(
        this.state.entity.id,
        data,
          response => this.onSuccess(response, false),
        this.onError
      );
    } else {
      AuthorStore.add(data, response => this.onSuccess(response, true), this.onError);
    }
  }

  controls = (translate, entity) => {
    let result = [
      {
        id: "firstName",
        label: translate.author.firstName,
        required: true
      },
      {
        id: "middleName",
        label: translate.author.middleName
      },
      {
        id: "lastName",
        label: translate.author.lastName,
        required: true
      },
      {
        id: "dateBirth",
        type: "date",
        label: translate.author.dateBirth,
        required: true
      },
      {
        id: "dateDeath",
        type: "date",
        label: translate.author.dateDeath
      },
      {
        id: "description",
        label: translate.author.description,
        multiline: true,
        required: true
      }
    ];
    if (entity !== undefined) {
      result[0].value = entity.firstName;
      result[1].value = entity.middleName;
      result[2].value = entity.lastName;
      if (entity.dateBirth) {
        result[3].value = DateUtil.transformDate(entity.dateBirth);
      }
      if (entity.dateDeath) {
        result[4].value = DateUtil.transformDate(entity.dateDeath);
      }
      result[5].value = entity.description;
    }
    return result;
  };

  render() {
    const { entity, buttonValue, buttonColor, buttonRender } = this.props;

    if (!this.checkPermission()) return null;

    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              innerRef={node => (this.refInnerForm = node)}
              title={language.author.formTitle}
              controls={this.controls(language, entity)}
              buttons={this.buttons(language, entity)}
              openButton={buttonRender(
                buttonValue === undefined
                  ? entity === undefined
                    ? language.button.create
                    : language.button.update
                  : buttonValue,
                buttonColor
              )}
              onSubmit={this.handleSubmit.bind(this)}
              validate={validateForm}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

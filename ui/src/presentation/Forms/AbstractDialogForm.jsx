import React from "react";
import PropTypes from "prop-types";

import EditIcon from "@material-ui/icons/Edit";
import { IconButton } from "@material-ui/core";

import Button from "../SmartComponents/Button";
import UrlStore from "../../stores/UrlStore";
import AbstractForm from "./AbstractForm";
import Configuration from "../../variables/Configuration";
import UserStore from "../../stores/UserStore";

export default class Form extends AbstractForm {
  static propTypes = {
    onSubmit: PropTypes.func,
    buttonValue: PropTypes.string,
    buttonColor: PropTypes.string,
    buttonRender: PropTypes.func,
    entity: PropTypes.object,
    backLink: PropTypes.string,
    shortIcon: PropTypes.bool
  };
  static defaultProps = {
    entity: undefined,
    buttonValue: undefined,
    shortIcon: false,
    buttonColor: "transparent",
    buttonRender: (value, color) => props => (
      <Button {...props} color={color}>
        {value}
      </Button>
    ),
    backLink: UrlStore.getPath("Admin", "Dashboard")
  };

  constructor(props) {
    super(props);
    this.state.entity = props.entity;
    this.state._user = {};
    this.onSubmitCallback = props.onSubmit;
    this.refInnerForm = React.createRef();
  }
  componentWillReceiveProps(nextProps, nextContext) {
    if (
      this.state.entity &&
      !this.state.entity.id &&
      nextProps.entity.id !== undefined
    ) {
      this.setState({ entity: nextProps.entity });
    }
  }

  componentDidMount() {
    UserStore.addChangeListener(this._updateUser);
    this._updateUser();
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._updateUser);
  }

  _updateUser = () => {
    this.setState({ _user: UserStore.getUser() });
  };

  handleSubmit() {}

  onSuccess = () => {
    this.refInnerForm.handleClose();
  };

  onError = response => {
    let language = Configuration.langMapper.getContent(Configuration.lang);
    for (let type in response.errors) {
      if (language.errorMessage[type] !== undefined)
        Configuration.addMessage("danger", language.errorMessage[type]);
      else Configuration.addMessage("danger", response.errors[type]);
    }
    if (language.errorMessage[response.statusCode] !== undefined)
      Configuration.addMessage(
        "danger",
        language.errorMessage[response.statusCode]
      );
  };
  generateShortIcon = (_, color) => {
    if (this.props.entity !== undefined) {
      return props => (
        <IconButton color={color} {...props}>
          <EditIcon />
        </IconButton>
      );
    }
    return null;
  };
  checkPermission() {
    return this.state._user.role === "admin" || this.state._user.role === "employee";
  }
  controls = (translate, entity) => [];
  buttons = (language, entity) => ({
    submit: entity === undefined ? language.button.create : language.button.save
  });
}

import React from "react";
import PropTypes from "prop-types";
import { DialogContent } from "@material-ui/core";

import { SystemContext } from "../../variables/SystemContext";
import AbstractForm from "./AbstractDialogForm";
import RentalStore from "../../stores/RentalStore";
import UserStore from "../../stores/UserStore";
import UrlStore from "../../stores/UrlStore";
import Button from "../SmartComponents/Button";
import UserList from "../components/UserList";
import FormGenerator from "../SmartComponents/FormGenerator/Dialog";
import Dialog from "../SmartComponents/Dialog";
import Configuration from "../../variables/Configuration";
import ReservationStore from "../../stores/ReservationStore";

export default class Form extends AbstractForm {
  static propTypes = {
    copy: PropTypes.object.isRequired,
    onSubmit: PropTypes.func,
    buttonValue: PropTypes.string,
    buttonColor: PropTypes.string,
    buttonRender: PropTypes.func,
    entity: PropTypes.object,
    reservations: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  static defaultProps = {
    entity: undefined,
    buttonValue: undefined,
    buttonColor: "primary",
    buttonRender: (value, color) => props => (
      <Button {...props} color={color} >
        {value}
      </Button>
    ),
    backLink: UrlStore.getPath("Admin", "Dashboard")
  };

  constructor(props) {
    super(props);
    this.state.userId = -1;
    this.state.user = {};
    this.state.copy = props.copy;
    this.state.reservations = props.reservations;
    this.state.entity = props.entity;
    this.state.open = false;
  }

  componentDidMount() {
    super.componentDidMount();
    UserStore.addChangeListener(this.updateUser);
    this.updateUser();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    UserStore.removeChangeListener(this.updateUser);
  }

  updateReservations = () => {
    let reservations = ReservationStore.getAll();
    reservations = reservations.filter(item => item.title.id === this.state.entity.id);
    this.setState({
      reservations: reservations
    });
  };

  validateForm = (id, value, values) => {
    if (id === "user") {
      let available_copies = (this.state.entity.copies.filter(item => item.status === "available") || []);
      let active_reservations = this.state.reservations;
      active_reservations = active_reservations.filter(item => item.status === "active");
      if (available_copies.length > active_reservations.length)
        return true;
      let user_reservations = active_reservations.filter(item => item.user.id === this.state.userId) || [];
      if (!user_reservations.length) {
        let language = Configuration.langMapper.getContent(Configuration.lang);
        Configuration.addMessage("danger", language.message["rental_fail_reservation"]);
        return false;
      }
      let chronological_reservations = active_reservations;
      chronological_reservations = chronological_reservations.sort((lhs, rhs) => {
          let lhs_date = lhs.dateReservation.split(". ");
          let rhs_date = rhs.dateReservation.split(". ");
          return new Date(lhs_date[2] + "-" + lhs_date[1] + "-" + lhs_date[0]) -
            new Date(rhs_date[2] + "-" + rhs_date[1] + "-" + rhs_date[0]);
      });
      if (!chronological_reservations.length || user_reservations[0] !== chronological_reservations[0]) {
        let language = Configuration.langMapper.getContent(Configuration.lang);
        Configuration.addMessage("danger", language.message["rental_fail_waitlist"]);
        return false;
      }
    }
    return true;
  };

  updateUser = () => {
    if (this.state.userId !== -1) {
      this.setState({
        user: UserStore.getById(this.state.userId)
      });
      this.updateReservations();
    }
  };

  onError = response => {
    if (response.message !== undefined)
      Configuration.addMessage("danger", response.message);
  };

  controls = (translate, entity) => [
    {
      id: "title",
      label: translate.title.titleName,
      value: this.state.entity.titleName,
      readOnly: true
    },
    {
      id: "user",
      label: translate.user.name,
      type: "string-search",
      required: true,
      value: this.state.user.lastName !== undefined ? this.state.user.lastName + ", " + this.state.user.firstName + " " + (this.state.user.middleName || "") : "",
      searchDialog: (props, onSelect) => (
        <Dialog {...props} simpleTitle>
          <DialogContent>
            <UserList allowCreate={false} onSelect={onSelect} filter={{defaultRole: row => row.role === "external"}} enableRoleFilter={false}/>
          </DialogContent>
        </Dialog>
      ),
      readOnly: true
    }
  ];

  cancelReservation = (user) => {
    let reservations = this.state.reservations;
    reservations = reservations.filter(item => item.user.id === user && item.status === "active");
    if (reservations.length)
      ReservationStore.update(reservations[0].id, {status: "cancelled"})
  };

  handleChange = data => {
    if (data.user !== undefined) {
      this.setState({
        userId: data.user,
        user: UserStore.getById(parseInt(data.user))
      });
    }
  };

  handleSubmit(data) {
      let _this = this;
      RentalStore.add({
          userId: this.state.userId,
          copyId: this.state.copy.id
        },
        response => {
          _this.refInnerForm.handleClose();
          this.cancelReservation(this.state.userId);
          let language = Configuration.langMapper.getContent(Configuration.lang);
          Configuration.redirect(UrlStore.generateUrl("Admin", "RentalDetail", {id: response.id}));
          Configuration.addMessage("primary", language.message["rental_create"]);
        },
        response => {
          if (response.message !== undefined) Configuration.addMessage("danger", response.message);
        }
      );
  }

  render() {
    const { entity, buttonColor, buttonRender } = this.props;

    if (!this.checkPermission()) return null;
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              innerRef={node => (this.refInnerForm = node)}
              title={language.rental.rentOut}
              controls={this.controls(language, entity)}
              buttons={this.buttons(language, entity)}
              openButton={buttonRender(language.rental.rentOut, buttonColor)}
              onSubmit={this.handleSubmit.bind(this)}
              onChange={this.handleChange}
              validate={this.validateForm}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

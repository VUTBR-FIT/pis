import React from "react";
import PropTypes from "prop-types";

import { SystemContext } from "../../variables/SystemContext";
import AbstractForm from "./AbstractDialogForm";
import GenreStore from "../../stores/GenreStore";
import FormGenerator from "../SmartComponents/FormGenerator/Dialog";
import Button from "../SmartComponents/Button";
import Configuration from "../../variables/Configuration";
import UrlStore from "../../stores/UrlStore";
function validateForm(id, value) {
  switch (id) {
    case "ageRestriction":
      return value >= 0;
    default:
      return true;
  }
}
export default class Form extends AbstractForm {
  static propTypes = {
    onSubmit: PropTypes.func,
    buttonValue: PropTypes.string,
    buttonColor: PropTypes.string,
    buttonRender: PropTypes.func,
    entity: PropTypes.object
  };
  static defaultProps = {
    entity: undefined,
    buttonValue: undefined,
    buttonColor: "transparent",
    buttonRender: (value, color) => props => (
      <Button {...props} color={color}>
        {value}
      </Button>
    )
  };

  onSuccess = (response, init) => {
    this.refInnerForm.handleClose();
    Configuration.redirect(UrlStore.generateUrl("Admin", "GenreDetail", {id: response.id}));
    let language = Configuration.langMapper.getContent(Configuration.lang);
    Configuration.addMessage("primary", init? language.message["genre_create"] : language.message["genre_edit"]);
  };


  handleSubmit(data) {
    if (this.state.entity !== undefined) {
      GenreStore.update(
        this.state.entity.id,
        data,
        response => this.onSuccess(response, false),
        this.onError
      );
    } else {
      GenreStore.add(data, response => this.onSuccess(response, true), this.onError);
    }
  }

  controls = (language, entity) => {
    let result = [
      {
        id: "genreName",
        label: language.genre.name,
        required: true
      },
      {
        id: "ageRestriction",
        type: "number",
        label: language.genre.ageRestriction,
        required: true
      },
      {
        id: "description",
        label: language.genre.description,
        required: true,
        multiline: true
      }
    ];
    if (entity !== undefined) {
      result[0].value = entity.genreName;
      result[1].value = entity.ageRestriction;
      result[2].value = entity.description;
    }
    return result;
  };

  render() {
    const { entity, buttonValue, buttonColor, buttonRender } = this.props;

    if (!this.checkPermission()) return null;

    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              innerRef={node => (this.refInnerForm = node)}
              title={language.genre.formTitle}
              controls={this.controls(language, entity)}
              buttons={this.buttons(language, entity)}
              openButton={buttonRender(
                buttonValue === undefined
                  ? entity === undefined
                    ? language.button.create
                    : language.button.update
                  : buttonValue,
                buttonColor
              )}
              onSubmit={this.handleSubmit.bind(this)}
              validate={validateForm}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

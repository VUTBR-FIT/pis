import AbstractForm from "./AbstractDialogForm";
import PropTypes from "prop-types";
import UrlStore from "../../stores/UrlStore";
import TitleStore from "../../stores/TitleStore";
import React from "react";
import Button from "../SmartComponents/Button";
import UserStore from "../../stores/UserStore";
import UserList from "../components/UserList";
import {SystemContext} from "../../variables/SystemContext";
import FormGenerator from "../SmartComponents/FormGenerator/Dialog";
import ReservationStore from "../../stores/ReservationStore";
import Configuration from "../../variables/Configuration";
import Dialog from "../SmartComponents/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

export default class ReservationDialog extends AbstractForm {
    static propTypes = {
        onSubmit: PropTypes.func,
        buttonValue: PropTypes.string,
        buttonColor: PropTypes.string,
        buttonRender: PropTypes.func,
        entity: PropTypes.object
    };

    static defaultProps = {
        entity: undefined,
        buttonValue: undefined,
        buttonColor: "primary",
        buttonRender: (value, color) => props => (
            <Button {...props} color={color}>
                {value}
            </Button>
        ),
        backLink: UrlStore.getPath("Admin", "Dashboard")
    };

    constructor(props) {
        super(props);
        this.state.entity = props.entity;
        this.state.titleId = parseInt(this.state.entity.id);
        this.state.user = {};
        this.state.userId = -1;
        this.state.open = false;
        this.refNode = React.createRef();
    }

    componentDidMount() {
        super.componentDidMount();
        UserStore.addChangeListener(this.updateUser);
        TitleStore.addChangeListener(this.updateTitle);
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        UserStore.removeChangeListener(this.updateUser);
        TitleStore.removeChangeListener(this.updateTitle);
    }

    validateForm = (id, value, values) => {
        let language = Configuration.getLanguage();
        if (id !== "user")
            return true;
        let user = this.state.user || {};
        let reservations = user.reservations || [];
        reservations = reservations.filter(item => item.status === "active");
        let reservation_capacity = user.reservationCapacity || -1;
        if (reservations.length >= reservation_capacity) {
            Configuration.addMessage("danger", language.message["reservation_capacity"]);
            return false;
        }
        let overdue_rentals = user.rentals || [];
        overdue_rentals = overdue_rentals.filter(item => item.status === "expired");
        if (overdue_rentals.length) {
            Configuration.addMessage("danger", language.message["reservation_overdue_rental"]);
            return false;
        }
        return true;
    };

    updateUser = () => {
        if (this.state.userId === -1)
            return;
        this.setState({
            user: UserStore.getById(this.state.userId)
        })
    };

    updateTitle = () => {
        // NaN check
        if (this.state.titleId === this.state.titleId) {
            this.setState({
                title: TitleStore.getById(this.state.titleId)
            });
        }
    };

    controls = (translate) => [
        {
            id: "title",
            label: translate.title.titleName,
            type: "select-list",
            value: this.state.entity.titleName,
            required: true,
            readOnly: true
        },
        {
            id: "user",
            label: translate.reservation.user,
            type: "string-search",
            required: true,
            value: this.state.user.lastName !== undefined ? this.state.user.lastName + ", " + this.state.user.firstName + " " + (this.state.user.middleName || "") : "",
            searchDialog: (props, onSelect) => (
                <Dialog {...props} simpleTitle>
                    <DialogContent>
                        <UserList allowCreate={false} onSelect={onSelect} filter={{defaultRole: row => row.role === "external"}} enableRoleFilter={false}/>
                    </DialogContent>
                </Dialog>
            ),
            readOnly: true
        }
    ];

    handleChange = data => {
        if (data.user !== undefined) {
            this.setState({
                userId: data.user,
                user: UserStore.getById(parseInt(data.user))
            });
        }
    };

    handleSubmit = (language, data) => {
        let _this = this;
        let reservation = this.state.user.reservations;
        reservation = reservation.filter(item => item.status === "active" && item.title === this.state.entity.titleName);
        if (reservation.length) {
            let date = new Date();
            date.setDate(date.getDate() + 30);
            ReservationStore.update(reservation[0].id, {
                dateExpiry: String(date.getFullYear()) + "-" +
                    String(date.getMonth() + 1).padStart(2, "0") + "-" +
                    String(date.getDate()).padStart(2, "0"),
                },
                response => {
                    let language = Configuration.langMapper.getContent(Configuration.lang);
                    _this.refNode.handleClose();
                    Configuration.redirect(UrlStore.generateUrl("Admin", "ReservationDetail", {id: response.id}));
                    Configuration.addMessage("primary", language.message["reservation_extend"])
                }
            );
        } else
        ReservationStore.add(this.state.entity.id, data.user,
            response => {
                _this.refNode.handleClose();
                Configuration.addMessage("primary", language.message["reservation_create"]);
                Configuration.redirect(UrlStore.generateUrl("Admin", "ReservationDetail", {id: response.id}));
            },
            response => {
                if (response.message !== undefined)
                    Configuration.addMessage("danger", response.message);

            })
    };

    render() {
        const { entity, buttonColor, buttonRender } = this.props;

        if (!this.checkPermission()) return null;

        return (
            <SystemContext.Consumer>
                {({ lang, langMapper }) => {
                    let language = langMapper.getContent(lang);
                    return (
                        <FormGenerator
                            innerRef={node => this.refNode = node}
                            title={language.reservation.createReservation}
                            controls={this.controls(language, entity)}
                            buttons={this.buttons(language, entity)}
                            openButton={buttonRender(language.reservation.createReservation, buttonColor)}
                            onSubmit={this.handleSubmit.bind(this, language)}
                            onChange={this.handleChange}
                            validate={this.validateForm}
                        />
                    );
                }}
            </SystemContext.Consumer>
        );
    }
}
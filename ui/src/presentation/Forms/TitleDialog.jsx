import React from "react";
import PropTypes from "prop-types";

import { SystemContext } from "../../variables/SystemContext";
import FormGenerator from "../SmartComponents/FormGenerator/Dialog";
import Button from "../SmartComponents/Button";
import UrlStore from "../../stores/UrlStore";
import TitleStore from "../../stores/TitleStore";
import AbstractDialogForm from "./AbstractDialogForm";
import GenreStore from "../../stores/GenreStore";
import AuthorStore from "../../stores/AuthorStore";

import ISBN from "isbn-validate";
import DateUtil from "../../utils/Date";
import Configuration from "../../variables/Configuration";

function validateForm(id, value, values) {
  switch (id) {
    case "isbn":
      return ISBN.Validate("ISBN " + value.replace(/-/g, ""));
    case "ean":
      return value >= 0;
    case "medium":
      return TitleStore.validateMedium(value);
    default:
      return true;
  }
}

export default class Form extends AbstractDialogForm {
  static propTypes = {
    onSubmit: PropTypes.func,
    buttonValue: PropTypes.string,
    buttonColor: PropTypes.string,
    buttonRender: PropTypes.func,
    entity: PropTypes.object,
    backLink: PropTypes.string
  };
  static defaultProps = {
    entity: undefined,
    buttonValue: undefined,
    buttonColor: "transparent",
    buttonRender: (value, color) => props => (
      <Button {...props} color={color}>
        {value}
      </Button>
    ),
    backLink: UrlStore.getPath("Admin", "Dashboard")
  };

  constructor(props) {
    super(props);
    this.state.authorList = AuthorStore.getAll();
    this.state.genreList = GenreStore.getAll();
    this.state.isbnError = false;
  }


  componentDidMount() {
    super.componentDidMount();
    GenreStore.addChangeListener(this.updateGenres);
    AuthorStore.addChangeListener(this.updateAuthors);
    this.updateAuthors();
    this.updateGenres();
  }
  componentWillUnmount() {
    super.componentWillUnmount();
    GenreStore.removeChangeListener(this.updateGenres);
    AuthorStore.removeChangeListener(this.updateAuthors);
  }

  updateGenres = () => {
    this.setState({ genreList: GenreStore.getAll() });
  };
  updateAuthors = () => {
    this.setState({ authorList: AuthorStore.getAll() });
  };

  onSuccess = (response, init) => {
    this.refInnerForm.handleClose();
    Configuration.redirect(UrlStore.generateUrl("Admin", "TitleDetail", {id: response.id}));
    let language = Configuration.langMapper.getContent(Configuration.lang);
    Configuration.addMessage("primary", init? language.message["title_create"]: language.message["title_edit"]);
  };

  handleSubmit(data) {
    if (this.state.entity !== undefined) {
      TitleStore.update(
        this.state.entity.id,
        data,
        this.onSuccess,
        this.onError
      );
    } else {
      TitleStore.add(data, response => this.onSuccess(response, true), this.onError);
    }
  }

  controls = (translate, entity) => {
    let result = [
      {
        id: "titleName",
        label: translate.title.titleName,
        required: true
      },
      {
        id: "isbn",
        label: translate.title.isbn,
        error: this.state.isbnError,
        required: true
      },
      {
        id: "ean",
        type: "number",
        label: translate.title.ean,
        required: true
      },
      {
        id: "publishedAt",
        type: "date",
        label: translate.title.publishedAt,
        required: true
      },
      {
        id: "publisher",
        label: translate.title.publisher,
        required: true
      },
      {
        id: "description",
        multiline: true,
        label: translate.title.description,
        required: true
      },
      {
        id: "medium",
        type: "select",
        label: translate.title.medium,
        required: true,
        options: TitleStore.getMediumOptions()
      },
      {
        id: "authors",
        type: "select-chip",
        options: this.state.authorList.map(author => ({
          id: author.id,
          name: [author.lastName, author.firstName, author.middleName].join(" ")
        })),
        value: [],
        label: translate.title.authors,
        required: true
      },
      {
        id: "genres",
        type: "select-chip",
        label: translate.title.genres,
        options: this.state.genreList.map(genre => ({
          id: genre.id,
          name: genre.genreName
        })),
        value: [],
        required: true
      }
    ];
    if (entity !== undefined) {
      result[0].value = entity.titleName;
      result[1].value = entity.isbn;
      result[2].value = entity.ean;
      if (entity.publishedAt) {
        result[3].value = DateUtil.transformDate(entity.publishedAt);
      }
      result[4].value = entity.publisher;
      result[5].value = entity.description;
      result[6].value = entity.medium;
      result[7].value = entity.authors;
      result[8].value = entity.genres;
    }
    return result;
  };
  render() {
    const { entity, buttonValue, buttonColor, buttonRender } = this.props;

    if (!this.checkPermission()) return null;

    let buttons = language => ({
      submit: buttonValue === undefined ? language.button.create : buttonValue
    });

    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              innerRef={node => (this.refInnerForm = node)}
              title={language.title.title}
              controls={this.controls(language, entity)}
              buttons={buttons(language)}
              openButton={buttonRender(
                buttonValue === undefined ? language.user.create : buttonValue,
                buttonColor
              )}
              onSubmit={this.handleSubmit.bind(this)}
              validate={validateForm}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { Grid, TextField, withStyles } from "@material-ui/core";
import Select from "./FormControls/Select";
import Select2 from "./FormControls/Select2";
import Switch from "./FormControls/Switch";
import AbstractComponent from "./AbstractComponent";
import styles from "../../assets/jss/material-dashboard-react/components/formStyle";

class Filter extends AbstractComponent {
  static defaultProps = {
    filters: [],
    color: "primary"
  };
  static propTypes = {
    classes: PropTypes.object.isRequired,
    filters: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func,
    color: PropTypes.oneOf(["primary"])
  };

  constructor(props) {
    super(props);
    this.state.filter = {};
    let _this = this;
    this.onChangeCallback = props.onChange;
    props.filters.filter(filter => filter.options !== undefined).map(filter => {
      _this.state.filter[filter.id] = filter.value;
      return null;
    });
  }

  handleChange(id, value) {
    let newState = Object.assign(this.state.filter, { [id]: value });
    this.setState({ filter: newState });
    if (this.onChangeCallback !== undefined) {
      this.onChangeCallback(newState);
    }
  }

  generateSelect(classes, filter) {
    return (
      <Select
        className={classes.formControl}
        id={filter.id}
        value={this.state[filter.id]}
        label={filter.name}
        options={filter.options.options}
        onChange={this.handleChange.bind(this, filter.id)}
        disabled={filter.options.disabled === true}
        emptyValue={filter.emptyValue || true}
      />
    );
  }

  generateSelect2(classes, filter) {
    return (
      <Select2
        classes={classes.formControl}
        id={filter.id}
        value={this.state[filter.id]}
        placeholder={filter.name}
        options={filter.options.options}
        onChange={this.handleChange.bind(this, filter.id)}
        disabled={filter.options.disabled === true}
      />
    );
  }

  generateBoolSwitch(classes, filter) {
    return (
      <Switch
        classes={classes}
        id={filter.id}
        name={filter.name}
        value={this.state[filter.id]}
        label={filter.name}
        options={filter.options.options}
        onChange={this.handleChange.bind(this, filter.id)}
      />
    );
  }

  generateSearchBox(classes, filter) {
    return (
      <TextField
        className={classes[this.getClassName("root")]}
        id={filter.id}
        name={filter.name}
        value={this.state[filter.id]}
        label={filter.name}
        onChange={event => {
          this.handleChange(filter.id, event.target.value);
        }}
        InputProps={{
          classes: {
            root: classes[this.getClassName("input")]
          }
        }}
        InputLabelProps={{
          FormLabelClasses: {
            root: classes[this.getClassName("label")],
            focused: classes[this.getClassName("labelFocused")]
          }
        }}
      />
    );
  }

  generateFilter(classes, filter) {
    if (!("options" in filter) || !filter.options.filter) {
      return null;
    }

    switch (filter.options.filter) {
      case "select":
      case "select2":
        return this.generateSelect(classes, filter);
      //return this.generateSelect2(classes, filter);
      case "search":
        return this.generateSearchBox(classes, filter);
      case "bool-switch":
        return this.generateBoolSwitch(classes, filter);
      default:
        throw new TypeError(
          "Bad type of filter (" + filter.options.filter + ")"
        );
    }
  }

  static applyFilter(data, filtersValues) {
    return data.filter(row => {
      for (let filterId in filtersValues) {
        let filter = filtersValues[filterId];
        if (
          filter !== undefined &&
          ((typeof filter !== "function" && row[filterId].id !== filter) ||
            (typeof filter === "function" && !filter(row)))
        )
          return false;
      }
      return true;
    });
  }

  getClassName(name) {
    return (
      this.props.color + name.charAt(0).toLocaleUpperCase() + name.slice(1)
    );
  }

  render() {
    const { classes, filters } = this.props;

    return (
      <Grid container className={classes.root}>
        {filters.map(filter => {
          return (
            <Grid
              item
              className={classes.gridItem}
              key={filter.id}
              style={{ width: `${95 / filters.length}%`, marginRight: "5px" }}
            >
              {this.generateFilter(classes, filter)}
            </Grid>
          );
        })}
      </Grid>
    );
  }
}

export default withStyles(styles)(Filter);

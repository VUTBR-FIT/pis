import React from "react";
import AbstractComponent from "../AbstractComponent";
import PropTypes from "prop-types";
import { FormControl, MenuItem, TextField } from "@material-ui/core";

class CustomSelect extends AbstractComponent {
  static defaultProps = {
    value: "",
    options: [],
    required: false,
    disabled: false,
    emptyValue: true,
    alwaysEmpty: false
  };
  static propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    onChange: PropTypes.func,
    emptyValue: PropTypes.bool,
    options: PropTypes.arrayOf(PropTypes.object),
    alwaysEmpty: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state.id = props.id;
    this.state.value = props.value || "";
    this.state.emptyValue = props.emptyValue;
    this.onCallbackCange = props.onChange;
  }

  handleOnChange = event => {
    let key = event.target.value;
    this.setState({ value: key });
    if (this.onCallbackCange !== undefined) {
      this.onCallbackCange(key, this.state.id);
    }
  };

  generateOptions = options => {
    let result = [];
    if (this.state.emptyValue)
      result.push(<MenuItem key={-1} value={-1}>-</MenuItem>);
    options.sort((a, b) => a.name.localeCompare(b.name)).map(option =>
      result.push(<MenuItem key={option.id} value={option.id}>{option.name}</MenuItem>
      )
    );
    return result;
  };

  render() {
    const {
      label,
      defaultValue,
      required,
      options,
      disabled,
      emptyValue,
      alwaysEmpty,
      ...rest
    } = this.props;

    return (
      <FormControl style={{ width: "100%" }}>
        <TextField
          {...rest}
          select
          fullWidth
          id={this.state.id}
          label={label}
          required={required}
          value={this.state.value || ""}
          defaultValue={defaultValue}
          disabled={disabled === true}
          onChange={this.handleOnChange}
          InputLabelProps={{
            shrink: alwaysEmpty ? false : this.state.value !== ""
          }}
        >
          {this.generateOptions(options)}
        </TextField>
      </FormControl>
    );
  }
}

export default CustomSelect;

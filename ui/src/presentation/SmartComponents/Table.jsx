import React from "react";
import AbstractComponent from "./AbstractComponent";

import PropTypes from "prop-types";
import {
  Table,
  TableBody,
  TableCell, TableFooter,
  TableHead,
  TableRow,
  withStyles
} from "@material-ui/core";
import styles from "../../assets/jss/material-dashboard-react/components/tableStyle";

class CustomTable extends AbstractComponent {
  static defaultProps = {
    onSelect: undefined,
    color: "primary"
  };
  static propTypes = {
    header: PropTypes.arrayOf(PropTypes.array).isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    keyRow: PropTypes.func.isRequired,
    selectedRow: PropTypes.string,
    onSelect: PropTypes.func,
    color: PropTypes.oneOf("primary")
  };

  constructor(props) {
    super(props);
    this.state.selectedRow = props.selectedRow;
  }

  generateHeaderCell = (data, classes) => {
    return (
      <TableCell
        key={data.id}
        className={classes.tableHeadCell}
        align={"right"}
      >
        {data.name}
      </TableCell>
    );
  };
  generateFooter = (classes, header, data, keyRow) => {
    if (data.length === 0) return null;
    let key = undefined;
    return (
      <TableFooter>
        {data.map(row => {
          key = keyRow(row);
          const isSelected = key === this.state.selectedRow;
          return (
            <TableRow
              key={key}
              hover
              selected={isSelected}
              aria-checked={isSelected}
              onClick={this.handleSelectRow.bind(this, key)}
            >
              {header.map(col => {
                return (
                  <TableCell key={col.id} className={classes.tableCell}>
                    {col.access(row)}
                  </TableCell>
                );
              })}
            </TableRow>
          );
        })}
      </TableFooter>
    );
  };

  handleSelectRow = id => {
    this.setState({ selectedRow: id });
    if (this.onSelectCallback !== undefined) this.onSelectCallback(id);
  };

  render() {
    const { classes, data, header, color, keyRow } = this.props;
    let key = undefined;
    let headerClass = color + "TableHeader";
    return (
      <div className={classes.root}>
        <Table className={classes.table + " " + classes.tableResponsive}>
          <TableHead className={classes[headerClass]}>
            <TableRow>
              {header.map(col => {
                return this.generateHeaderCell(col, classes);
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => {
              key = keyRow(row);
              const isSelected = key === this.state.selectedRow;
              return (
                <TableRow
                  key={key}
                  hover
                  selected={isSelected}
                  aria-checked={isSelected}
                  onClick={this.handleSelectRow.bind(this, key)}
                >
                  {header.map(col => {
                    return (
                      <TableCell
                        key={col.id}
                        className={classes.tableCell}
                        align={"right"}
                      >
                        {col.access(row)}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
          {this.generateFooter(classes, header, data, keyRow)}
        </Table>
      </div>
    );
  }
}

export default withStyles(styles)(CustomTable);

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import AbstractGenerator from "./AbstractGenerator";
import { SystemContext } from "../../../variables/SystemContext";

const styles = theme => ({
  formControl: {
    width: "100%"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  }
});

class Default extends AbstractGenerator {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    controls: PropTypes.arrayOf(PropTypes.object).isRequired,
    buttons: PropTypes.object,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    validate: PropTypes.func
  };
  static defaultProps = {
    controls: [],
    buttons: {},
    validate: () => true
  };

  render() {
    const { classes, controls, buttons } = this.props;
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          let actionButtons = null;
          if (Object.keys(buttons).length)
            actionButtons = (
              <div>{this.generateButtons(language, buttons)}</div>
            );

          return (
            <form className={classes.form} onSubmit={this.handleSave}>
              {controls.map(control => this.generateControl(control, classes))}
              {actionButtons}
            </form>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(styles)(Default);

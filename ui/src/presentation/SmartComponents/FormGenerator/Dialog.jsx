import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import AbstractGenerator from "./AbstractGenerator";
import Dialog from "../Dialog";
import { SystemContext } from "../../../variables/SystemContext";

const styles = theme => ({
  closeIcon: {
    float: "right"
  },
  formControl: {
    width: "100%"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  }
});

class FormDialog extends AbstractGenerator {
  static propTypes = {
    controls: PropTypes.arrayOf(PropTypes.object).isRequired,
    classes: PropTypes.object.isRequired,
    openButton: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    buttons: PropTypes.object,
    validate: PropTypes.func
  };
  static defaultProps = {
    controls: [],
    buttons: {},
    validate: () => true
  };

  constructor(props) {
    super(props);
    this.refForm = React.createRef();
  }

  handleClose = () => {
    this.refForm.handleClose();
  };

  render() {
    const { classes, title, controls, buttons, openButton } = this.props;
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <Dialog
              innerRef={node => (this.refForm = node)}
              title={title}
              fullScreen={false}
              button={openButton}
              actions={this.generateButtons(language, buttons)}
              onClose={this.handleClear}
            >

              <form onSubmit={this.handleSave}>
                {controls.map(control =>
                  this.generateControl(control, classes)
                )}
              </form>
            </Dialog>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(styles)(FormDialog);

import React from "react";
import PropTypes from "prop-types";
import { FormControl, InputAdornment, TextField } from "@material-ui/core";

import SearchIcon from "@material-ui/icons/Search";

import AbstractComponent from "../AbstractComponent";
import Button from "../Button";
import Select from "../FormControls/Select";
import SelectChip from "../FormControls/SelectChip";
import DateUtil from "../../../utils/Date";

class Default extends AbstractComponent {
  static propTypes = {
    controls: PropTypes.arrayOf(PropTypes.object).isRequired,
    buttons: PropTypes.object,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    validate: PropTypes.func
  };
  static defaultProps = {
    controls: [],
    buttons: {},
    validate: () => true
  };
  state = {
    redrawId: 0,
    validationForce: false,
    error: {},
    displayError: false
  };

  constructor(props) {
    super(props);
    let _this = this;
    this.refSearch = {};
    props.controls.map(control => {
      if (control.type === "string-search")
        this.refSearch[control.id] = React.createRef();

      _this.setControl(control.id, control.value, true);
      return null;
    });
    this.validationForm(true);
    this.onChangeCallback = props.onChange;
    this.onSubmitCallback = props.onSubmit;
  }

  componentWillReceiveProps(nextProps, nextContext) {
    let controls = this.getControl();
    nextProps.controls.map(control => {
      if (!(control.id in controls) || (controls[control.id] === undefined && control.value !== undefined))
        this.setControl(control.id, control.value);
      return null;
    });
    this.setState({ controls });
  }

  handleSave = event => {
    event.preventDefault();
    if (!this.validationForm(undefined, this.getControl())) {
      this.setState({ validationForce: true });
    } else if (this.onSubmitCallback !== undefined) {
      this.onSubmitCallback(this.getControl());
    }
  };
  handleUpdate = (id, value) => {
    if (id in this.refSearch)
      this.refSearch[id].handleClose();
    let newValues = this.setControl(id, value);
    if (this.onChangeCallback !== undefined)
      this.onChangeCallback(newValues);
  };
  handleUpdateEvent = (id, event) => {
    this.handleUpdate(id, event.target.value);
  };

  handleClear = () => {
    for (let index in this.props.controls) {
      let control = this.props.controls[index];
      this.setControl(control.id, control.value || control.defaultValue);
      this.setState({ displayError: false, validationForce: false, error: {} });
    }
  };

  getSourceControl(id) {
    let temp = this.props.controls.filter(item => item.id === id);
    if (temp.length) return temp[0];
    return {};
  }

  getControl(id) {
    let control = {};
    for (let key in this.state) {
      if (key.startsWith("control-")) {
        control[key.substr(8)] = this.state[key];
      }
    }

    if (id !== undefined) return control[id];
    return control;
  }

  setControl(id, value, init) {
    let key = "control-" + id;
    let result = this.getControl();
    result[id] = value;
    if (init === undefined)
      this.setState({ redrawId: this.state.redrawId + 1, [key]: value });
    else {
      this.state[key] = value;
    }
    if (this.state.validationForce) this.validationForm(undefined, result);
    return result;
  }

  validationForm(init, controls) {
    let results = {};
    for (let id in controls) {
      let value = controls[id];
      let control = this.getSourceControl(id);
      if (init) {
        results[id] = false;
      } else if (control.required) {
        if (control.type === "select-chip")
          results[id] = !Array.isArray(value) || value.length === 0 || !this.props.validate(id, value, this.getControl());
        else
          results[id] = value === undefined || value.length === 0 || !this.props.validate(id, value, this.getControl());
      } else if (control.required !== true && value !== undefined)
        results[id] = !this.props.validate(id, value, this.getControl());
      else if (control.required !== true && value === undefined)
        results[id] = false;
      else results[id] = true;
    }
    if (init) this.state.error = results;
    else this.setState({ error: results });
    return Object.values(results).filter(item => item === true).length === 0;
  }

  generateFormControl(control, classes, children) {
    return (
      <FormControl
        disabled={control.disable || false}
        hidden={control.hidden === true}
        className={classes.formControl}
        fullWidth
        key={control.id}
      >
        {children}
      </FormControl>
    );
  }

  generateControl(control, classes) {
    let value = this.getControl(control.id);
    let props = {
      key: control.id,
      id: control.id,
      disabled: control.disable || false,
      onChange: this.handleUpdate.bind(this, control.id),
      label: control.label,
      required: control.required === true,
      value: value !== undefined ? value.toString() : "",
      error: control.error || this.state.error[control.id]
    };
    if (control.readOnly === true) props.InputProps = { readOnly: true };
    if (control.type === "select2") {
    } else if (control.type === "select-chip") {
      return this.generateFormControl(
        control,
        classes,
        <SelectChip
          {...props}
          chipValues={!value ? undefined : value.map(item => item.id)}
          options={control.options}
          value={undefined}
        />
      );
    } else if (control.type === "select") {
      return <Select {...props} options={control.options} emptyValue={control.emptyValue || false} />;
    } else if (control.type === "string-search") {
      return this.generateFormControl(
        control,
        classes,
        control.searchDialog({
          innerRef: node => this.refSearch[control.id] = node,
          button: rest => (
            <TextField
              {...props}
              value={Number.isInteger(control.value) ? "" : control.value}
              type="string"
              InputProps={{
                endAdornment: (
                  <InputAdornment position={"end"}>
                    <SearchIcon />
                  </InputAdornment>
                ),
                readOnly: control.readOnly === true
              }}
              {...rest}
            />
          )
        }, props.onChange)
      );
    } else if (control.type === "date" || control.type === "time") {
      return this.generateFormControl(
        control,
        classes,
        <TextField
          {...props}
          type={control.type}
          InputLabelProps={{
            shrink: true
          }}
          autoComplete={control.autoComplete || control.id}
          onChange={event => this.handleUpdate(control.id, event.target.value)}
        />
      );
    } else if (
      control.type === "datetime-local" ||
      control.type === "datetime"
    ) {
      return this.generateFormControl(
        control,
        classes,
        <TextField
          {...props}
          type="datetime-local"
          InputLabelProps={{
            shrink: true
          }}
          autoComplete={control.autoComplete || control.id}
          defaultValue={value || DateUtil.getCurrentDate("/")}
          onChange={event => this.handleUpdate(control.id, event.target.value)}
        />
      );
    } else if (control.type === "custom") {
      return control.render;
    } else {
      return this.generateFormControl(
        control,
        classes,
        <TextField
          {...props}
          type={control.type || "string"}
          onChange={event => this.handleUpdate(control.id, event.target.value)}
          autoComplete={control.autoComplete || control.id}
          multiline={control.multiline || false}
          placeholder={control.placeholder}
          InputProps={{
            startAdornment:
              control.startAdornment !== undefined ? (
                <InputAdornment position="start">
                  {control.startAdornment}
                </InputAdornment>
              ) : (
                undefined
              ),
            endAdornment:
              control.endAdornment !== undefined ? (
                <InputAdornment position="end">
                  {control.endAdornment}
                </InputAdornment>
              ) : (
                undefined
              ),
            readOnly: control.readOnly === true
          }}
        />
      );
    }
  }

  generateButtons(language, buttons) {
    /* TODO function button(source) {
                  let temp = {
                    color: "primary",
                    value: undefined
                  };
                  if (typeof source === "string") {
                    temp.value = source;
                  } else {
                    temp.color = temp.color || source.value;
                    temp.value = source.value;
                  }
                }*/
    let result = [];
    if (buttons.submit !== undefined)
      result.push(
        <Button
          color="primary"
          fullWidth
          variant="contained"
          key="submit"
          type="submit"
          onClick={this.handleSave}
        >
          {buttons.submit}
        </Button>
      );
    if (buttons.others)
      buttons.others.map(button => {
        result.push(
          <Button key={button.key}>{language.button[button.key]}</Button>
        );
        return null;
      });
    return result;
  }
}

export default Default;

import React from "react";
import image from "../../assets/img/sidebar-2.jpg";

export default class AbstractLayout extends React.PureComponent {
  constructor(props) {
    if (new.target === AbstractLayout) {
      throw new TypeError("This is abstract class.");
    }

    super(props);
    this.state = {};
    this.state.image = image;
    this.state.color = "blue";
    this.state.lang = "en";
    this.state.hasImage = true;
    this.state.fixedClasses = "dropdown show";
    this.state.mobileOpen = false;
    this.refMainPanel = React.createRef();
  }

  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };
  resizeFunction = () => {
    if (window.innerWidth >= 960) {
      this.setState({ mobileOpen: false });
    }
  };

  componentDidMount() {
    window.addEventListener("resize", this.resizeFunction);
  }
  componentDidUpdate(e) {
    if (e.history.location.pathname !== e.location.pathname) {
      if (this.refMainPanel.current !== null)
        this.refMainPanel.current.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({ mobileOpen: false });
      }
    }
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.resizeFunction);
  }
}

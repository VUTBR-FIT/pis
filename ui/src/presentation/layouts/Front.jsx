/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import { Redirect, Route, Switch } from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";
import Header from "../components/Header";
import Footer from "../components/Footer/Footer";
import Sidebar from "../components/Sidebar";

import routes from "../../routes/front.js";

import dashboardStyle from "../../assets/jss/material-dashboard-react/layouts/dashboardStyle";

import logo from "../../assets/img/reactlogo.png";
import AbstractLayout from "./AbstractLayout";
import { SystemContext } from "../../variables/SystemContext";
import config from "../../variables/Configuration";

const switchRoutes = (
  <Switch>
    {routes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key}/>;
      return <Route exact={prop.exact||false} path={prop.path} component={prop.component} key={key}/>;
    })}
  </Switch>
);

class App extends AbstractLayout {
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.root}>
        <SystemContext.Provider value={config}>
          <Sidebar
            routes={routes}
            logoText={"SIGMA GROUP"}
            logo={logo}
            image={image}
            handleDrawerToggle={this.handleDrawerToggle}
            open={this.state.mobileOpen}
            color="blue"
            {...rest}
          />
          <div className={classes.mainPanel} ref={this.refMainPanel}>
            <Header
              routes={routes}
              handleDrawerToggle={this.handleDrawerToggle}
              {...rest}
            />
            <main className={classes.content}>
              {switchRoutes}
            </main>
            {this.getRoute() ? <Footer/> : null}
          </div>
        </SystemContext.Provider>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(App);

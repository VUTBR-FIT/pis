import React from "react";
import PropTypes from "prop-types";
import { Redirect, Route, Switch } from "react-router-dom";

import { withStyles } from "@material-ui/core";
import styles from "../../assets/jss/material-dashboard-react/layouts/dashboardStyle";

import routes from "../../routes/admin";

import AbstractLayout from "./AbstractLayout";
import Header from "../components/Header";
import Footer from "../components/Footer/Footer";
import Sidebar from "../components/Sidebar";

import { SystemContext } from "../../variables/SystemContext";
import Configuration from "../../variables/Configuration";

import UserStore from "../../stores/UserStore";
import UrlStore from "../../stores/UrlStore";

import image from "../../assets/img/sidebar-1.jpg";
import logo from "../../assets/img/book-16-512.png";

const switchRoutes = (
  <Switch>
    {routes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />;
      return (
        <Route
          exact={prop.exact || false}
          path={prop.path}
          component={prop.component}
          key={key}
        />
      );
    })}
    <Redirect to="/Dashboard" />
  </Switch>
);

class Admin extends AbstractLayout {
  updateUser = () => {
    if (!UserStore.isSigned()) {
      if (this.props.location)
        Configuration.redirect(
          UrlStore.getPath("Sign", "SignIn"),
          this.props.location.pathname
        );
      else Configuration.redirect(UrlStore.getPath("Sign", "SignIn"));
    }
  };

  componentDidMount() {
    super.componentDidMount();
    UserStore.addChangeListener(this.updateUser);
  }
  componentWillUnmount() {
    super.componentWillUnmount();
    UserStore.removeChangeListener(this.updateUser);
  }

  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.root}>
        <SystemContext.Provider value={Configuration}>
          <Sidebar
            routes={routes}
            logoText={"Library"}
            logo={logo}
            image={image}
            handleDrawerToggle={this.handleDrawerToggle}
            open={this.state.mobileOpen}
            color={this.state.color}
            layoutName={"Admin"}
            {...rest}
          />
          <div className={classes.mainPanel} ref={this.refMainPanel}>
            <Header
              admin
              routes={routes}
              handleDrawerToggle={this.handleDrawerToggle}
              {...rest}
            />
            <main className={classes.content}>{switchRoutes}</main>
          </div>
          <Footer />
        </SystemContext.Provider>
      </div>
    );
  }
}

Admin.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Admin);

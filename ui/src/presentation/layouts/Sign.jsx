import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router-dom";

import { withStyles } from "@material-ui/core";
import styles from "../../assets/jss/material-dashboard-react/layouts/dashboardStyle";

import routes from "../../routes/sign";

import AbstractLayout from "./AbstractLayout";

import { SystemContext } from "../../variables/SystemContext";
import Configuration from "../../variables/Configuration";
import UserStore from "../../stores/UserStore";
import UrlStore from "../../stores/UrlStore";

const switchRoutes = (
  <Switch>
    {routes.map((prop, key) => {
      return <Route path={prop.path} component={prop.component} key={key} />;
    })}
  </Switch>
);

class Sign extends AbstractLayout {
  updateUser = () => {
    if (UserStore.isSigned()) {
      if (this.props.location)
        Configuration.redirect(
          UrlStore.getPath("Admin", "Dashboard"),
          this.props.location.pathname
        );
      else Configuration.redirect(UrlStore.getPath("Admin", "Dashboard"));
    }
  };

  componentWillUnmount() {
    super.componentWillUnmount();
    UserStore.removeChangeListener(this.updateUser);
  }
  componentDidMount() {
    super.componentDidMount();
    UserStore.addChangeListener(this.updateUser);
  }

  render() {
    const { classes } = this.props;

    return (
      <SystemContext.Provider value={Configuration}>
        <div className={classes.content}>
          <div className={classes.container} ref="mainPanel">
            {switchRoutes}
          </div>
        </div>
      </SystemContext.Provider>
    );
  }
}

Sign.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Sign);

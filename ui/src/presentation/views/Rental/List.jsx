import React from "react";

import RentalList from "../../components/RentalList";
import AbstractView from "../AbstractView";

export default class List extends AbstractView {
  render() {
    return (
      <>
        {super.render()}
        <RentalList />
      </>
    );
  }
}

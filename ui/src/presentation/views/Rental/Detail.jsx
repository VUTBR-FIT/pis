import AbstractView from "../AbstractView";
import PropTypes from "prop-types";
import Store from "../../../stores/RentalStore";
import UserStore from "../../../stores/UserStore";
import {DialogContent, DialogTitle, Table, TableBody, TableCell, TableRow, withStyles} from "@material-ui/core";
import style from "../../../assets/jss/material-dashboard-react/components/tableStyle";
import React from "react";
import {SystemContext} from "../../../variables/SystemContext";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import Configurator from "../../../variables/Configuration";
import UrlStore from "../../../stores/UrlStore";
import ReservationStore from "../../../stores/ReservationStore"
import CardFooter from "../../components/Card/CardFooter";
import Button from "../../SmartComponents/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import Configuration from "../../../variables/Configuration";

class Detail extends AbstractView {
    static propTypes = {
        classes: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        let id = props.match.params.id;
        this.state.id = parseInt(id);
        this.state.entity = Store.getById(this.state.id);
        this.state.user = UserStore.getUser();
        this.state.dialogOpen = false;
        this.state.dialogType = "";
    }

    componentDidMount() {
        super.componentDidMount();
        Store.addChangeListener(this.updateEntity);
        UserStore.addChangeListener(this.updateUser);
        this.updateEntity();
        this.updateUser();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        Store.removeChangeListener(this.updateEntity);
        UserStore.removeChangeListener(this.updateUser);
    }

    updateEntity = () => {
        this.setState({
            entity: Store.getById(this.state.id)
        });
    };

    updateUser = () => {
        this.setState({user: UserStore.getUser()})
    };

    mark = state => () => {
        if (this.state.entity === undefined)
            return;
        let _this = this;
        Store.update(
            this.state.id,
            {status: Store.getStatus(state)},
            () => {
                _this.dialogToggle(state);
                Configuration.addMessage("primary", "Rental marked as " + state + ".");
            },
            response => {
                for (let type in response.errors)
                    Configuration.addMessage("danger", response.errors[type]);
            }
        );
    };

    reserve = () => {
        if (this.state.entity === undefined
            || this.state.entity.user === undefined
            || this.state.entity.copy === undefined)
            return;
        let _this = this;
        ReservationStore.add(
            this.state.entity.copy.titleId,
            this.state.entity.user.id,
            response => {
                _this.dialogToggle("reserve");
                let language = Configuration.langMapper.getContent(Configuration.lang);
                Configuration.addMessage("primary", language.message["reservation_create"]);
                Configuration.redirect(UrlStore.generateUrl("Admin", "ReservationDetail", {id: response.id}))
            },
            response => {
                let language = Configuration.langMapper.getContent(
                    Configuration.lang
                );
                for (let type in response.errors) {
                    if (language.errorMessage[type] !== undefined)
                        Configuration.addMessage("danger", language.errorMessage[type]);
                    else Configuration.addMessage("danger", response.errors[type]);
                }
            }
        );
    };

    generateButtons = language => {
        let entity = this.state.entity;
        if (this.state.user !== undefined && this.state.user.role !== "external") {
            if ((entity.status === "active" || entity.status === "expired")) {
                if (this.state.user.role !== "external")
                    return (
                        <CardFooter>
                            <Button
                                onClick={this.dialogToggle.bind(this, "returned")}
                                color={"primary"}>
                                {language.rental.return}
                            </Button>
                            <Button
                                onClick={this.dialogToggle.bind(this, "lost")}
                                color={"danger"}>
                                {language.rental.lose}
                            </Button>
                        </CardFooter>
                    );
            } else if (entity.status === "returned")
                return (
                    <CardFooter>
                        <Button
                            color={"primary"}
                            onClick={this.dialogToggle.bind(this, "reserve")}
                        >
                            {language.rental.reserve}
                        </Button>
                    </CardFooter>
                );
        }
        return(<CardFooter />);
    };

    dialogToggle = type => {
        this.setState({
            dialogType: type,
            dialogOpen: !this.state.dialogOpen
        })
    };

    render() {
        const { classes } = this.props;
        return (
            <>
              {super.render()}
            <SystemContext.Consumer>
                {({ lang, langMapper }) => {
                    let language = langMapper.getContent(lang);
                    let entity = this.state.entity || {};
                    let user = entity.user === undefined ? undefined : entity.user.id;
                    let title = entity.copy === undefined ? undefined : entity.copy.titleId;
                    return (
                        <div>
                            <Card>
                                <CardBody>
                                    <Table className={classes.table}>
                                        <TableBody>
                                            <TableRow hover onClick={() => Configurator.redirect(
                                                UrlStore.generateUrl("Admin", "UserDetail", { id: user }),
                                                UrlStore.getPath("Admin", "RentalDetail")
                                            )}>
                                                <TableCell>{language.rental.name}</TableCell>
                                                <TableCell>{entity.user === undefined ? "" : entity.user.name}</TableCell>
                                            </TableRow>
                                            <TableRow hover onClick={() => Configuration.redirect(
                                                UrlStore.generateUrl("Admin", "TitleDetail", {id: title}),
                                                UrlStore.getPath("Admin", "TitleList")
                                            )}>
                                                <TableCell>{language.rental.titleName}</TableCell>
                                                <TableCell>{entity.copy === undefined ? "" : entity.copy.title}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell>{language.rental.dateRental}</TableCell>
                                                <TableCell>{entity.dateRental}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell>{language.rental.dateExpiry}</TableCell>
                                                <TableCell>{entity.dateExpiry}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell>{language.rental.status}</TableCell>
                                                <TableCell style={{color: entity.status === "active" ? "green" : (entity.status === "expired" ? "red" : "SlateGray")}}>{entity.status === undefined ? "" : entity.status.toString().charAt(0).toUpperCase() + entity.status.toString().slice(1)}</TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </CardBody>
                                {this.generateButtons(language)}
                            </Card>
                            <Dialog
                                open={this.state.dialogOpen && this.state.dialogType === "returned"}
                                onClose={this.dialogToggle.bind(this, "")}
                            >
                                <DialogTitle id="alert-dialog-title">Mark as Returned?</DialogTitle>
                                <DialogContent id="alert-dialog-description">Are you sure you want to mark this rental as returned? {(this.state.entity !== undefined && this.state.entity.status === "expired") ? "The user will not be able to make further rentals and reservations until all expired rentals are returned." : ""} </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.dialogToggle.bind(this, "")} color="primary" autoFocus>
                                        Back
                                    </Button>
                                    <Button color="primary" onClick={this.mark("returned")}>
                                        {language.rental.return}
                                    </Button>
                                </DialogActions>
                            </Dialog>
                            <Dialog
                                open={this.state.dialogOpen && this.state.dialogType === "lost"}
                                onClose={this.dialogToggle.bind(this, "")}
                            >
                                <DialogTitle id="lost-dialog-title">Mark as Lost?</DialogTitle>
                                <DialogContent id="lost-dialog-description">Are you sure you want to mark this rental as lost? This process cannot be reverted.</DialogContent>
                                <DialogActions>
                                    <Button onClick={this.dialogToggle.bind(this, "")} color="primary" autoFocus>
                                        Back
                                    </Button>
                                    <Button onClick={this.mark("lost")} color="danger">
                                        {language.rental.lose}
                                    </Button>
                                </DialogActions>
                            </Dialog>
                            <Dialog
                                open={this.state.dialogOpen && this.state.dialogType === "reserve"}
                            >
                                <DialogTitle id="reserve-dialog-title">{language.rental.reserve}?</DialogTitle>
                                <DialogContent id="reserve-dialog-description">Create a reservation for the user {this.state.entity !== undefined  && this.state.entity.user !== undefined ? this.state.entity.user.name : ""} on the title {this.state.entity !== undefined && this.state.entity.copy !== undefined ? this.state.entity.copy.title : ""}?</DialogContent>
                                <DialogActions>
                                    <Button onClick={this.dialogToggle.bind(this, "")} color="primary" autoFocus>
                                        Back
                                    </Button>
                                    <Button onClick={this.reserve} color="primary">
                                        {language.rental.reserve}
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </div>
                    );
                }}
            </SystemContext.Consumer>
          </>
        )
    }
}

export default withStyles(style)(Detail);
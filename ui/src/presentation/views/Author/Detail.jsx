import React from "react";
import PropTypes from "prop-types";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  withStyles
} from "@material-ui/core";

import AbstractView from "../AbstractView";
import Store from "../../../stores/AuthorStore";
import { SystemContext } from "../../../variables/SystemContext";
import style from "../../../assets/jss/material-dashboard-react/components/tableStyle";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import CardFooter from "../../components/Card/CardFooter";
import AuthorDialog from "../../Forms/AuthorDialog";
import Configuration from "../../../variables/Configuration";
import UrlStore from "../../../stores/UrlStore";
import CardHeader from "../../components/Card/CardHeader";
import Datatable from "../../SmartComponents/Datatable";
import Configurator from "../../../variables/Configuration";
import TitleStore from "../../../stores/TitleStore";
import DeleteDialog from "../../components/DeleteDialog";

class Detail extends AbstractView {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    let id = props.match.params.id;

    this.state.id = parseInt(id);
    this.state.entity = Store.getById(this.state.id);
    this.state.isDialogOpen = false;
    this.state.titles = [];
    this.updateTitles(true);
  }

  componentDidMount() {
    super.componentDidMount();
    Store.addChangeListener(this.updateEntity);
    Store.addChangeListener(this.updateTitles);
    TitleStore.addChangeListener(this.updateTitles);
    this.updateEntity();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    Store.removeChangeListener(this.updateEntity);
    Store.addChangeListener(this.updateTitles);
    TitleStore.removeChangeListener(this.updateTitles);
  }

  updateEntity = () => {
    this.setState({
      entity: Store.getById(this.state.id)
    });
  };

  handleDelete = () => {
    Store.delete(this.state.id, () => {
        Configuration.redirect(
            UrlStore.getPath("Admin", "AuthorList"),
            UrlStore.getPath("Admin", "AuthorDetail")
        );
        let language = Configuration.getLanguage();
        Configuration.addMessage("primary", language.message["author_delete"]);
    }
    );
  };

  updateTitles = init => {
      if (this.state.entity === undefined)
          return;
      let items = this.state.entity.titles || [];
      items = items.map(item => {
          let temp = TitleStore.getById(item.id);
          if (temp.id !== undefined)
              return temp;
          return item;
      });
      if (init) this.state.titles = items;
      else this.setState({ titles: items });
  };

  render() {
    const { classes } = this.props;
    return (
        <SystemContext.Consumer>
          {({ lang, langMapper }) => {
            let language = langMapper.getContent(lang);
            let entity = this.state.entity || {};
            return (
              <>
              {super.render()}
              <Card>
                <CardBody>
                  <Table className={classes.table}>
                    <TableBody>
                      <TableRow>
                        <TableCell>{language.author.firstName}</TableCell>
                        <TableCell>{entity.firstName}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.author.middleName}</TableCell>
                        <TableCell>{entity.middleName}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.author.lastName}</TableCell>
                        <TableCell>{entity.lastName}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.author.dateBirth}</TableCell>
                        <TableCell>{entity.dateBirth}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell style={{ whiteSpace: "nowrap" }}>
                          {language.author.dateDeath}
                        </TableCell>
                        <TableCell>{entity.dateDeath}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.author.description}</TableCell>
                        <TableCell>{entity.description}</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </CardBody>
                <CardFooter>
                  <AuthorDialog
                    buttonColor={"primary"}
                    entity={this.state.entity}
                  />
                  <DeleteDialog
                    title={"Delete Author?"}
                    buttonValue={language.button.delete}
                    deleteValue={"Delete Author"}
                    content={
                      language.author.delete + " " +
                      this.state.entity.firstName + " " +
                      (this.state.entity.middleName === undefined ? "" : this.state.entity.middleName + " ") +
                      this.state.entity.lastName +"?"
                    }
                    onDelete={this.handleDelete}
                  />
                </CardFooter>
              </Card>
              <Card>
                <CardHeader color={"primary"}>{language.author.relatedTitles}</CardHeader>
                <CardBody>
                  <Datatable
                    data={this.state.titles}
                    keyRow={row => row.id}
                    header={[
                      {
                        id: "titleId",
                        name: language.title.titleName,
                        access: row => row.titleName
                      },
                        {
                            id: "genre",
                            name: language.title.genres,
                            access: row => row.genres === undefined ? "" : row.genres.map(item => item.name).join("; ")
                        },
                      {
                        id: "isbn",
                        name: language.title.isbn,
                        access: row => row.isbn
                      },
                      {
                        id: "ean",
                        name: language.title.ean,
                        access: row => row.ean
                      }
                    ]}
                    optLanguage={language.dataTable}
                    optSortable={true}
                    // optSortBy={"titleName"}
                    optSortDir={"asc"}
                    onSelectRow={id =>
                        Configurator.redirect(
                            UrlStore.generateUrl("Admin", "TitleDetail", { id }),
                            UrlStore.getPath("Admin", "TitleList")
                        )
                    }
                  />
                </CardBody>
              </Card>
              </>
            );
          }}
        </SystemContext.Consumer>
    );
  }
}

export default withStyles(style)(Detail);

import React from "react";

import { withStyles } from "@material-ui/core";
import Plus from "@material-ui/icons/Add";

import AbstractView from "../AbstractView";
import AuthorStore from "../../../stores/AuthorStore";
import style from "../../../assets/jss/material-dashboard-react/views/dashboardStyle";
import UrlStore from "../../../stores/UrlStore";
import { SystemContext } from "../../../variables/SystemContext";
import Filter from "../../SmartComponents/Filter";
import CardHeader from "../../components/Card/CardHeader";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import Configurator from "../../../variables/Configuration";
import Datatable from "../../SmartComponents/Datatable";
import CardFooter from "../../components/Card/CardFooter";
import DialogForm from "../../Forms/AuthorDialog";
import Button from "../../SmartComponents/Button";
import DateUtil from "../../../utils/Date";

class List extends AbstractView {
  constructor(props) {
    super(props);
    this.state.source = AuthorStore.getAll();
    this.state.data = [];
    this.state.value = {
      fulltext: () => true
    };
  }

  componentDidMount() {
    super.componentDidMount();
    AuthorStore.addChangeListener(this.updateData);
    this.updateData();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    AuthorStore.removeChangeListener(this.updateData);
  }

  updateData = () => {
    this.setState({ source: AuthorStore.getAll() });
  };

  handleFilter = data => {
    let newState = Object.assign({}, this.state);
    Object.assign(newState.value, data);
    if (data.fulltext && data.fulltext.length !== 0) {
      let filters = this.header({ author: {} });
      newState.value.fulltext = row => {
        for (let key in filters) {
          let value = filters[key].access(row);

          if (value !== null && value !== undefined) {
            if (
              (filters[key].type === undefined ||
                filters[key].type === "string") &&
              value.toLowerCase().indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
            if (
              filters[key].type === "number" &&
              value
                .toString()
                .toLowerCase()
                .indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
          }
        }
        return false;
      };
    } else newState.value.fulltext = () => true;
    this.setState(newState);
  };

  header = (language, forFilter) => {
    let results = [
      {
        id: "firstName",
        name: language.author.firstName,
        access: row => row.firstName
      },
      {
        id: "middleName",
        name: language.author.middleName,
        access: row => row.middleName
      },
      {
        id: "lastName",
        name: language.author.lastName,
        access: row => row.lastName
      },
      {
        id: "dateBirth",
        type: "date",
        name: language.author.dateBirth,
        access: row => row.dateBirth,
        accessDate: row => DateUtil.transformDate(row.dateBirth)
      },
      {
        id: "dateDeath",
        type: "date",
        name: language.author.dateDeath,
        access: row => row.dateDeath,
        accessDate: row => DateUtil.transformDate(row.dateDeath)
      }
    ];
    if (forFilter)
      results.push({
        id: "fulltext",
        name: language.dataTable.toolbar.search,
        access: () => {},
        options: {
          filter: "search"
        }
      });
    while (results.length > 4 && forFilter) results.shift();
    return results;
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.tableContainer}>
        {super.render()}
        <SystemContext.Consumer>
          {({ lang, langMapper }) => {
            let language = langMapper.getContent(lang);
            return (
              <Card>
                <CardHeader color="primary">
                  <Filter
                    filters={this.header(language, true)}
                    onChange={this.handleFilter}
                  />
                </CardHeader>
                <CardBody>
                  <Datatable
                    data={Filter.applyFilter(
                      this.state.source,
                      this.state.value
                    )}
                    keyRow={row => row.id}
                    header={this.header(language)}
                    optLanguage={language.dataTable}
                    optSortable={true}
                    optSortBy={"lastName"}
                    optSortDir={"asc"}
                    onSelectRow={id =>
                      Configurator.redirect(
                        UrlStore.generateUrl("Admin", "AuthorDetail", { id }),
                        UrlStore.getPath("Admin", "AuthorList")
                      )
                    }
                  />
                </CardBody>
                <CardFooter>
                  <DialogForm
                    buttonRender={() => props => (
                      <Button {...props} color={"primary"}>
                        <Plus /> {language.author.create}
                      </Button>
                    )}
                  />
                </CardFooter>
              </Card>
            );
          }}
        </SystemContext.Consumer>
      </div>
    );
  }
}
//
// List.propTypes = {
//     classes: PropTypes.object.isRequired
// };

export default withStyles(style)(List);

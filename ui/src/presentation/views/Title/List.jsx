import React from "react";

import Plus from "@material-ui/icons/Add";
import Button from "../../SmartComponents/Button";
import Datatable from "../../SmartComponents/Datatable";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import Filter from "../../SmartComponents/Filter";
import CardBody from "../../components/Card/CardBody";
import { SystemContext } from "../../../variables/SystemContext";
import TitleStore from "../../../stores/TitleStore";
import UrlStore from "../../../stores/UrlStore";
import CardFooter from "../../components/Card/CardFooter";
import DialogForm from "../../Forms/TitleDialog";
import Configurator from "../../../variables/Configuration";
import AbstractView from "../AbstractView";
import UserStore from "../../../stores/UserStore";
import DateUtil from "../../../utils/Date";

export default class List extends AbstractView {
  constructor(props) {
    super(props);
    this.state.source = TitleStore.getAll();
    this.state.data = [];
    this.state.value = {
      fulltext: () => true
    };
    this.state.user = {};
  }
  componentDidMount() {
    super.componentDidMount();
    TitleStore.addChangeListener(this.updateData);
    UserStore.addChangeListener(this.updateUser);
    this.updateData();
    this.updateUser();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    TitleStore.removeChangeListener(this.updateData);
    UserStore.removeChangeListener(this.updateUser);
  }

  updateData = () => {
    this.setState({ source: TitleStore.getAll() });
  };
  updateUser = () => {
    this.setState({ user: UserStore.getUser() });
  };

  handleFilter = data => {
    let newState = Object.assign({}, this.state);
    Object.assign(newState.value, data);
    if (data.fulltext && data.fulltext !== "") {
      let filters = this.header({ title: {} });
      newState.value.fulltext = row => {
        for (let key in filters) {
          let value = filters[key].access(row);

          if (value !== null && value !== undefined) {
            if (
              (filters[key].type === undefined ||
                filters[key].type === "string") &&
              value.toLowerCase().indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
            if (
              filters[key].type === "number" &&
              value
                .toString()
                .toLowerCase()
                .indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
          }
        }
        return false;
      };
    } else newState.value.fulltext = () => true;
    this.setState(newState);
  };

  header = (language, forFilter) => {
    let data = [
      {
        id: "authorName",
        name: language.title.authors,
        access: row => row.authors.map( item => item.name ).join('; ')
      },
      {
        id: "titleName",
        name: language.title.titleName,
        access: row => row.titleName
      },
      {
        id: "publisher",
        name: language.title.publisher,
        access: row => row.publisher
      },
      {
        id: "publishedAt",
        type: "date",
        name: language.title.publishedAt,
        access: row => row.publishedAt,
        accessDate: row => DateUtil.transformDate(row.publishedAt)
      },
      {
        id: "medium",
        name: language.title.medium,
        access: row => row.medium
      }
    ];
    if (this.state.user.role === "admin" || this.state.user.role === "employee")
      data.push({
        id: "copies",
        type: "number",
        name: language.title.copyCount,
        access: row => row.copiesCount
      });
    if (forFilter)
      data.push({
        id: "fulltext",
        name: language.dataTable.toolbar.search,
        access: () => {},
        options: {
          filter: "search"
        }
      });
    while (data.length > 4 && forFilter) data.shift();
    return data;
  };

  render() {
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <>
              {super.render()}
              <Card>
                <CardHeader color="primary">
                  <Filter
                    filters={this.header(language, true)}
                    onChange={this.handleFilter}
                  />
                </CardHeader>
                <CardBody>
                  <Datatable
                    data={Filter.applyFilter(this.state.source, this.state.value)}
                    keyRow={row => row.id}
                    header={this.header(language)}
                    optLanguage={language.dataTable}
                    optSortable={true}
                    optSortBy={"titleName"}
                    optSortDir={"asc"}
                    onSelectRow={id =>
                      Configurator.redirect(
                        UrlStore.generateUrl("Admin", "TitleDetail", { id }),
                        UrlStore.getPath("Admin", "TitleList")
                      )
                    }
                  />
                </CardBody>
                <CardFooter>
                  <DialogForm
                    buttonRender={() => props => (
                      <Button {...props} color={"primary"}>
                        <Plus /> {language.title.create}
                      </Button>
                    )}
                  />
                </CardFooter>
              </Card>
            </>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

import React from "react";
import PropTypes from "prop-types";

import {Table, TableBody, TableCell, TableRow, withStyles} from "@material-ui/core";

import Plus from "@material-ui/icons/Add";
import style from "../../../assets/jss/material-dashboard-react/components/tableStyle";
import {SystemContext} from "../../../variables/SystemContext";
import AbstractView from "../AbstractView";
import Store from "../../../stores/TitleStore";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import CardFooter from "../../components/Card/CardFooter";
import Button from "../../SmartComponents/Button";
import UrlStore from "../../../stores/UrlStore";
import Datatable from "../../SmartComponents/Datatable";
import CardHeader from "../../components/Card/CardHeader";
import ReservationStore from "../../../stores/ReservationStore";
import UserStore from "../../../stores/UserStore";
import Configuration from "../../../variables/Configuration";
import Configurator from "../../../variables/Configuration";
import UpdateDialog from "../../Forms/TitleDialog";
import CopyDialog from "../../Forms/CopyDialog";
import RentalDialog from "../../Forms/RentalDialog";
import DeleteDialog from "../../components/DeleteDialog";
import ReservationDialog from "../../Forms/ReservationDialog";
import DateUtil from "../../../utils/Date";

class Detail extends AbstractView {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    let id = props.match.params.id;

    this.state.id = parseInt(id);
    this.state.entity = {};
    this.state.user = {};
    this.state.reservations = [];
    this.updateReservations(true);
    this.state.isDialogOpen = false;
    this.state.isCopyDialogOpen = false;
    this.state.copies = [];
  }

  componentDidMount() {
    super.componentDidMount();
    Store.addChangeListener(this.updateEntity);
    UserStore.addChangeListener(this.updateUser);
    ReservationStore.addChangeListener(this.updateReservations);
    this.updateEntity();
    this.updateUser();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    Store.removeChangeListener(this.updateEntity);
    UserStore.removeChangeListener(this.updateUser);
    ReservationStore.removeChangeListener(this.updateReservations);
  }

  updateEntity = () => {
    this.setState({
      entity: Store.getById(this.state.id)
    });
  };

  updateUser = () => {
    this.setState({
      user: UserStore.getUser()
    });
  };

  updateReservations = init => {
    if (this.state.id === undefined)
      return;
    let reserve = ReservationStore.getAll();
    reserve = reserve.filter(item => item.title.id === this.state.id);
    if (init) this.state.reservations = reserve;
    else this.setState({
      reservations: reserve
    });
  };

  onError = response => {
    if (response.message !== undefined)
      Configuration.addMessage("danger", response.message);
    else {
      let language = Configuration.langMapper.getContent(Configuration.lang);
      if (language.errorMessage[response.statusCode] !== undefined)
        Configuration.addMessage(
            "danger",
            language.errorMessage[response.statusCode]
        );
    }
  };

  handleDelete = () => {
    Store.delete(this.state.id, () => {
      Configuration.redirect(
        UrlStore.getPath("Admin", "TitleList"),
        UrlStore.getPath("Admin", "TitleDetail")
      );
    });
  };

  handleDeleteCopy = id => {
    Store.deleteCopy(id,
        () => {
            let language = Configuration.getLanguage();
            Configuration.addMessage("primary", language.message["title_delete"]);
        });
  };

  reservationHeader = language => {
    return [
      {
        id: "name",
        name: language.reservation.user,
        access: row => row.user.name
      },
      {
        id: "dateReservation",
        name: language.reservation.dateReservation,
        access: row => row.dateReservation,
        accessDate: row => DateUtil.transformDate(row.dateReservation)
      },
      {
        id: "dateExpiry",
        name: language.reservation.dateExpiry,
        access: row => row.dateExpiry,
        accessDate: row => DateUtil.transformDate(row.dateExpiry)
      },
      {
        id: "status",
        name: language.reservation.status,
        access: row => row.status === "cancelled" ? "Closed" : row.status.charAt(0).toUpperCase() + row.status.slice(1)
      }
    ];
  };

  rentalHeader = language => {
    let chrono_sort = (lhs, rhs) => {
      if (lhs.id < rhs.id)
        return 1;
      if (lhs.id > rhs.id)
        return -1;
      return 0;
    };

    let results = [
      {
        id: "copyId",
        name: language.copy.id,
        access: row => row.id
      },
      {
        id: "availability",
        name: language.copy.availability,
        access: row => row.status.replace(/\b\w/g, l => l.toUpperCase())
      },
      {
        id: "healthPercentage",
        name: language.copy.state,
        access: row => row.healthPercentage
      }
    ];
    if (this.state.user !== undefined && this.state.user.role !== "external") {
      results.push({
        id: "user",
        name: language.copy.rentedTo,
        access: row => {
          let rentals = row.rentals ? row.rentals.sort(chrono_sort) : [];
          if (rentals.length)
            return rentals[0].status === "returned"? "" : rentals[0].user.name;
          return "";
        }
      });

      results.push({
        id: "actions",
        name: language.copy.operations,
        access: row => {
            return (
            <div style={{display: "flex"}}>
              <CopyDialog
                  buttonRender={() => props => <Button {...props} color={"primary"} style={{flex: "1"}} disabled={row.status !== "available"}>
                    {language.copy.update}
                  </Button>}
                  entity={row}
                  titleId={this.state.entity.id}
                  shortIcon
              />
              {row.status === "rented" || row.status === "rental expired" ?
                  <Button color={"primary"}
                    style={{flex: "1"}}
                    onClick={() =>
                      Configurator.redirect(UrlStore.generateUrl("Admin", "RentalDetail", {id: row.rentals ? row.rentals.sort(chrono_sort)[0].id : 0}))}>
                    Go to Rental</Button>
                  : <RentalDialog
                      copy={row}
                      reservations={this.state.reservations}
                      entity={this.state.entity}
                      buttonRender={() => props =>
                          <Button color={"primary"} {...props}
                                  disabled={row.rentals.filter(item => item.status !== "returned").length > 0}
                                  style={{flex: "1"}}>
                            {language.rental.rentOut}
                          </Button>}
                  />
              }
              <DeleteDialog
                  buttonRender={() => props => <Button {...props} color={"danger"} disabled={row.status !== "available"}>
                    {language.button.delete}
                  </Button>}
                  key={row.id}
                  content={language.copy.deleteContent}
                  onDelete={this.handleDeleteCopy.bind(
                      this,
                      row.id
                  )}
                  title="Delete Copy?"
                  buttonValue={language.button.delete}
                  deleteValue="Delete Copy"
                  // buttonRender={() => props => <Button {...props} color={"danger"} style={{flex: "1"}} disabled={row.status !== "available"}>
                  //   {language.button.delete}
                  // </Button>}
              />
            </div>
        )
      },
        options: {
          sortable: false
        }
      });
    }
    return results;
  };

  render() {
    const { classes } = this.props;
    let subTable = (data, detail, list) => {
      if (data !== undefined && data.length > 0) {
        if (data.length > 1) {
          return (
            <Table>
              <TableBody>
                {data.map(item => (
                  <TableRow hover key={item.id} onClick={() => {
                    let _id = item.id;
                    Configurator.redirect(
                        UrlStore.generateUrl("Admin", detail, {item: _id}),
                        UrlStore.getPath("Admin", list)
                    )
                  }}>
                    <TableCell>{item.name}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          );
        } else return <Table>
            <TableBody>
              <TableRow hover onClick={() => {
                let _id = data[0].id;
                Configurator.redirect(
                    UrlStore.generateUrl("Admin", detail, {item: _id}),
                    UrlStore.getPath("Admin", list)
                )
              }}><TableCell>{data[0].name}</TableCell></TableRow></TableBody></Table>;
      } else return null;
    };
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          let entity = this.state.entity || {};
          let authors = subTable(entity.authors, "AuthorDetail", "AuthorList");
          let genres = subTable(entity.genres, "GenreDetail", "GenreList");
          return (
            <>
              {super.render()}
              <Card>
                <CardBody>
                  <Table className={classes.table}>
                    <TableBody>
                      <TableRow>
                        <TableCell>{language.title.titleName}</TableCell>
                        <TableCell>{entity.titleName}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.title.authors}</TableCell>
                        {authors}
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.title.isbn}</TableCell>
                        <TableCell>{entity.isbn}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.title.ean}</TableCell>
                        <TableCell>{entity.ean}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.title.medium}</TableCell>
                        <TableCell>{entity.medium}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.title.genres}</TableCell>
                        {genres}
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.title.publisher}</TableCell>
                        <TableCell>{entity.publisher}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.title.publishedAt}</TableCell>
                        <TableCell>{entity.publishedAt}</TableCell>
                      </TableRow>
                      {/*<TableRow>*/}
                      {/*  <TableCell>{language.title.copyCount}</TableCell>*/}
                      {/*  <TableCell>{entity.copiesCount}</TableCell>*/}
                      {/*</TableRow>*/}
                    </TableBody>
                  </Table>
                </CardBody>
                <CardFooter>
                  <div>
                    <UpdateDialog
                      buttonColor={"primary"}
                      style={{ marginRight: "7px" }}
                      entity={this.state.entity}
                      buttonValue={language.button.update}
                    />
                    <ReservationDialog
                        buttonColor={"primary"}
                        entity={this.state.entity}
                        reservations={this.state.reservations}
                        buttonValue={language.reservation.createReservation}
                        />
                  </div>
                  <DeleteDialog
                    content={
                      language.title.delete_1 + " " + this.state.entity.titleName + " " + language.title.delete_2
                    }
                    onDelete={this.handleDelete}
                    title="Delete Title?"
                    buttonValue={language.button.delete}
                    deleteValue="Delete Title"
                  />
                </CardFooter>
              </Card>
              <Card>
                <CardHeader color={"primary"}>{language.title.reservations}</CardHeader>
                <CardBody>
                <Datatable
                  header={this.reservationHeader(language)}
                  data={this.state.reservations}
                  keyRow={row => row.id}
                  optLanguage={language.dataTable}
                  optSortable={true}
                  optSortBy={"name"}
                  optSortDir={"asc"}
                  onSelectRow={id => Configuration.redirect(
                      UrlStore.generateUrl("Admin", "ReservationDetail", {id: id})
                  )}
                  />
                </CardBody>
              </Card>
              {this.state.user && this.state.user.role !== "external" ? <Card>
                <CardHeader color={"primary"}>
                  {language.title.copyCount}
                </CardHeader>
                <CardBody>
                  <Datatable
                    header={this.rentalHeader(language)}
                    data={entity.copies || []}
                    keyRow={row => row.id}
                    optLanguage={language.dataTable}
                    optSortBy={"healthPercentage"}
                    optSortDir={"asc"}
                  />
                </CardBody>
                {entity.id !== undefined ? (
                  <CardFooter>
                    <CopyDialog
                      buttonRender={() => props => (
                        <Button {...props} color={"primary"}>
                          <Plus /> {language.copy.create}
                        </Button>
                      )}
                      titleId={entity.id}
                    />
                  </CardFooter>
                ) : null}
              </Card> : null}
            </>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(style)(Detail);

import React from "react";
import PropTypes from "prop-types";
import { CssBaseline, withStyles, Grid } from "@material-ui/core";

import RentalList from "../components/RentalList";
import ReservationList from "../components/ReservationList";
import AbstractView from "./AbstractView";

const styles = theme => ({
  root: {
    display: "flex"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3
  }
});

class Dashboard extends AbstractView {
  render() {
    const { classes } = this.props;

    return (
      <>
        {super.render()}
        <div className={classes.root}>
          <CssBaseline />
          <main className={classes.content}>
            <Grid container>
              <Grid item style={{ flex: 1, marginRight: "5px" }}>
                <RentalList
                  filter={{ defaultStatus: row => row.status === "active" }}
                  displayStatus={false}
                  title
                />
              </Grid>
              <Grid item style={{ flex: 1 }}>
                <ReservationList
                  filter={{ defaultStatus: row => row.status === "active" }}
                  displayStatus={false}
                  displayUser={false}
                  title
                />
              </Grid>
            </Grid>
          </main>
        </div>
      </>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Dashboard);

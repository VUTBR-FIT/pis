import React from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core";

import style from "../../../assets/jss/material-dashboard-react/views/dashboardStyle";
import AbstractView from "../AbstractView";
import UrlStore from "../../../stores/UrlStore";
import Configurator from "../../../variables/Configuration";
import UserList from "../../components/UserList";

class List extends AbstractView {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  render() {
    const { classes } = this.props;
    return (
      <>
        {super.render()}
        <div className={classes.tableContainer}>
          <UserList
            onSelect={id =>
              Configurator.redirect(
                UrlStore.generateUrl("Admin", "UserDetail", { id }),
                UrlStore.getPath("Admin", "UserList")
              )
            }
            allowCreate={true}
          />
        </div>
      </>
    );
  }
}

export default withStyles(style)(List);

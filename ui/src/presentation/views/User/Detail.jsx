import React from "react";
import PropTypes from "prop-types";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  withStyles
} from "@material-ui/core";

import style from "../../../assets/jss/material-dashboard-react/components/tableStyle";
import { SystemContext } from "../../../variables/SystemContext";
import AbstractView from "../AbstractView";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import CardFooter from "../../components/Card/CardFooter";
import Store from "../../../stores/UserStore";
import UrlStore from "../../../stores/UrlStore";
import UpdateDialog from "../../Forms/User/Default";
import Configuration from "../../../variables/Configuration";
import Datatable from "../../SmartComponents/Datatable";
import CardHeader from "../../components/Card/CardHeader";
import RentalStore from "../../../stores/RentalStore";
import ReservationStore from "../../../stores/ReservationStore";
import DateUtil from "../../../utils/Date";
import Configurator from "../../../variables/Configuration";
import DeleteDialog from "../../components/DeleteDialog";

class Detail extends AbstractView {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    let id = props.match.params.id;

    this.state.id = parseInt(id);
    this.state.currentUser = {};
    this.state.entity = Store.getById(this.state.id);
    this.state.rentals = [];
    this.state.reservations = [];
    this.updateRentals(true);
    this.updateReservations(true);
  }

  componentDidMount() {
    super.componentDidMount();
    Store.addChangeListener(this.updateEntity);
    RentalStore.addChangeListener(this.updateRentals);
    ReservationStore.addChangeListener(this.updateReservations);
    this.updateEntity();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    Store.removeChangeListener(this.updateEntity);
    RentalStore.removeChangeListener(this.updateRentals);
    ReservationStore.removeChangeListener(this.updateReservations);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    let id = this.props.match.params.id;
    if (Number.isInteger(parseInt(id)) && parseInt(id) !== this.state.id) {
      this.setState({
        id: parseInt(id),
        entity: Store.getById(parseInt(id))
      });
    }
  }

  updateEntity = () => {
    let entity = Store.getById(this.state.id);
    this.setState({
      entity: entity,
      currentUser: Store.getUser()
    });
  };
  updateRentals = init => {
    let items = this.state.entity.rentals || [];
    let setState = true;
    let result = [];
    items.map(item => {
      let temp = RentalStore.getById(item.id);
      if (temp.id === undefined) setState = false;
      result.push(temp);
      return null;
    });
    if (setState)
      if (init) this.state.rentals = result;
      else this.setState({ rentals: result });
  };
  updateReservations = init => {
    let items = this.state.entity.reservations || [];
    let setState = true;
    let result = [];
    items.map(item => {
      let temp = ReservationStore.getById(item.id);
      if (temp.id === undefined) setState = false;
      result.push(temp);
      return null;
    });
    if (setState)
      if (init) this.state.reservations = result;
      else this.setState({ reservations: result });
  };

  handleDelete = () => {
    Store.delete(this.state.id, () => {
      Configuration.redirect(
        UrlStore.getPath("Admin", "UserList"),
        UrlStore.getPath("Admin", "UserDetail")
      );
      let language = Configuration.getLanguage();
      Configuration.addMessage("primary", language.message["user_delete"]);
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          let entity = this.state.entity || {};
          return (
            <>
              {super.render()}
              <Card>
                <CardBody>
                  <Table className={classes.table}>
                    <TableBody>
                      <TableRow>
                        <TableCell>{language.user.name}</TableCell>
                        <TableCell>{entity.firstName}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.user.middleName}</TableCell>
                        <TableCell>{entity.middleName}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.user.surname}</TableCell>
                        <TableCell>{entity.lastName}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.user.dateOfBirth}</TableCell>
                        <TableCell>{entity.dateBirth}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.user.email}</TableCell>
                        <TableCell>{entity.email}</TableCell>
                      </TableRow>
                      {entity.role === "external" ? (
                        <>
                          <TableRow>
                        <TableCell>{language.user.reservationCapacity}</TableCell>
                        <TableCell>{entity.reservationCapacity}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.user.rentalCapacity}</TableCell>
                        <TableCell>{entity.rentalCapacity}</TableCell>
                      </TableRow></>) : null}
                    </TableBody>
                  </Table>
                </CardBody>
                <CardFooter>
                  <UpdateDialog
                    buttonColor={"primary"}
                    entity={this.state.entity}
                    role={entity.role}
                    userRole={entity.role || ""}
                  />
                  <DeleteDialog
                    title={"Delete User?"}
                    buttonValue={language.button.delete}
                    deleteValue={"Delete User"}
                    content={
                      language.user.delete + " " +
                      entity.firstName + " " +
                      (entity.middleName === undefined ? "" : entity.middleName + " ") +
                      entity.lastName + "?"
                    }
                    userRole={entity.role || ""}
                    onDelete={this.handleDelete}
                  />
                </CardFooter>
              </Card>
              {entity.role === "external" ? (
                <Card>
                  <CardHeader color={"primary"}>
                    {language.user.rentals}
                  </CardHeader>
                  <CardBody>
                    <Datatable
                      data={this.state.rentals}
                      keyRow={row => row.id}
                      header={[
                        {
                          id: "titleName",
                          name: language.title.titleName,
                          access: row => row.copy.title
                        },
                        {
                          id: "dateRental",
                          name: language.rental.dateRental,
                          access: row => row.dateRental
                        },
                        {
                          id: "dateExpiry",
                          name: language.rental.dateExpiry,
                          access: row => row.dateExpiry
                        },
                        {
                          id: "status",
                          name: language.rental.status,
                          access: row => row.status.charAt(0).toUpperCase() + row.status.slice(1)
                        }
                      ]}
                      optLanguage={language.dataTable}
                      optSortable={true}
                      optSortBy={"dateExpiry"}
                      optSortDir={"desc"}
                      onSelectRow={id =>
                          Configurator.redirect(
                              UrlStore.generateUrl("Admin", "RentalDetail", { id }),
                              UrlStore.getPath("Admin", "RentalList")
                          )
                      }
                    />
                  </CardBody>
                  <CardFooter />
                </Card>
              ) : null}
              {entity.role === "external" ? (
                <Card>
                  <CardHeader color={"primary"}>
                    {language.user.reservations}
                  </CardHeader>
                  <CardBody>
                    <Datatable
                      data={this.state.reservations}
                      keyRow={row => row.id}
                      header={[
                        {
                          id: "titleName",
                          name: language.title.titleName,
                          access: row => row.title.name
                        },
                        {
                          id: "dateReservation",
                          type: "date",
                          name: language.reservation.dateReservation,
                          access: row => row.dateReservation,
                          accessDate: row => DateUtil.transformDate(row.dateReservation)
                        },
                        {
                          id: "dateExpiry",
                          type: "date",
                          name: language.reservation.dateExpiry,
                          access: row => row.dateExpiry,
                          accessDate: row => DateUtil.transformDate(row.dateExpiry)
                        },
                        {
                          id: "status",
                          name: language.reservation.status,
                          access: row => row.status === "cancelled"? "Closed" : row.status.charAt(0).toUpperCase() + row.status.slice(1)
                          }
                      ]}
                      optLanguage={language.dataTable}
                      optSortable={true}
                      optSortBy={"dateReservation"}
                      optSortDir={"desc"}
                      onSelectRow={id => Configuration.redirect(
                          UrlStore.generateUrl("Admin", "ReservationDetail", {id: id})
                      )}
                    />
                  </CardBody>
                  <CardFooter />
                </Card>
              ) : null}
            </>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(style)(Detail);

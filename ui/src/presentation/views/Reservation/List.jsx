import React from "react";

import AbstractView from "../AbstractView";
import ReservationList from "../../components/ReservationList";

export default class List extends AbstractView {
  render() {
    return (
      <>
        {super.render()}
        <ReservationList />
      </>
    );
  }
}

import AbstractView from "../AbstractView";
import PropTypes from "prop-types";
import Store from "../../../stores/ReservationStore";
import UserStore from "../../../stores/UserStore";
import {
    DialogActions,
    DialogContent,
    DialogTitle,
    Table,
    TableBody,
    TableCell,
    TableRow,
    withStyles
} from "@material-ui/core";
import style from "../../../assets/jss/material-dashboard-react/components/tableStyle";
import React from "react";
import {SystemContext} from "../../../variables/SystemContext";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import Configurator from "../../../variables/Configuration";
import UrlStore from "../../../stores/UrlStore";
import CardFooter from "../../components/Card/CardFooter";
import Button from "../../SmartComponents/Button";
import Dialog from "@material-ui/core/Dialog";

class Detail extends AbstractView {
    static propTypes = {
        classes: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        let id = props.match.params.id;
        this.state.id = parseInt(id);
        this.state.entity = Store.getById(this.state.id);
        this.state.user = UserStore.getUser();
        this.state.dialogOpen = false;
        this.state.dialogType = "";
    }

    componentDidMount() {
        super.componentDidMount();
        Store.addChangeListener(this.updateEntity);
        UserStore.addChangeListener(this.updateUser);
        this.updateUser();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        Store.removeChangeListener(this.updateEntity);
        UserStore.removeChangeListener(this.updateUser);
    }

    updateEntity = () => {
        this.setState({
            entity: Store.getById(this.state.id)
        });
    };

    updateUser = () => {
        this.setState({user: UserStore.getUser()})
    };

    toggleDialog = type => {
        this.setState({
            dialogType: type,
            dialogOpen: !this.state.dialogOpen
        })
    };

    onError = response => {
        if (response.message !== undefined)
            Configurator.addMessage("danger", response.message);
        for (let type in response.errors)
            Configurator.addMessage("danger", response.errors[type]);
    };

    cancel = language => {
        if (this.state.entity === undefined)
            return;
        let _this = this;
        Store.update(
            this.state.id,
            {status: Store.getStatus("cancelled")},
            () => {
                _this.toggleDialog("cancel");
                Configurator.addMessage("primary", language.message["reservation_cancel_success"]);
            },
            this.onError
        );
    };

    extend = language => {
        let expiry = new Date();
        expiry.setDate(expiry.getDate() + 30);
        let expiry_value = String(expiry.getFullYear()) + "-" +
            String(expiry.getMonth() + 1).padStart(2, "0") + "-" +
            String(expiry.getDate()).padStart(2, "0");
        let _this = this;
        Store.update(
            this.state.id,
            { dateExpiry: expiry_value },
            () => {
                _this.toggleDialog("extend");
                Configurator.addMessage("primary", language.message["reservation_extend"]);
            },
            this.onError
        );
    };

    reserve = language => {
        if (!this.state.entity.title || !this.state.entity.user) return;
        let _this = this;
        Store.add(
            this.state.entity.title.id,
            this.state.entity.user.id,
            response => {
                _this.toggleDialog("reserve");
                Configurator.redirect(UrlStore.generateUrl("Admin", "ReservationDetail", {id: response.id}));
                _this.setState({
                    entity: response,
                    id: response.id
                });
                Configurator.addMessage("primary", language.message["reservation_create"]);
            },
            this.onError
        )
    };

    generateButtons = language => {
        if (!this.state.user || this.state.user.role === "external")
            return;
        if (this.state.entity.status === "active")
            return (<CardFooter>
                <Button color={"primary"} onClick={this.toggleDialog.bind(this, "extend")}>
                    {language.reservation.extend}
                </Button>
                <Button color={"danger"} onClick={this.toggleDialog.bind(this, "cancel")}>
                    {language.reservation.cancel}
                </Button>
            </CardFooter>);
        else return (
            <CardFooter>
                <Button color={"primary"} onClick={this.toggleDialog.bind(this, "reserve")}>
                    {language.reservation.reserveAgain}
                </Button>
            </CardFooter>
        )
    };

    render() {
        const {classes} = this.props;
        return(
        <>
            {super.render()}
            <SystemContext.Consumer>
                {({lang, langMapper}) => {
                    let language = langMapper.getContent(lang);
                    let entity = this.state.entity || {};
                    let user = entity.user === undefined ? {} : entity.user.id;
                    let title = entity.title === undefined ? {} : entity.title.id;
                    return (
                        <Card>
                            <CardBody>
                                <Table className={classes.table}>
                                    <TableBody>
                                        <TableRow hover onClick={() => Configurator.redirect(
                                            UrlStore.generateUrl("Admin", "UserDetail", { id: user }),
                                            UrlStore.getPath("Admin", "ReservationDetail")
                                        )}>
                                            <TableCell>{language.reservation.user}</TableCell>
                                            <TableCell>{entity.user === undefined ? "" : entity.user.name}</TableCell>
                                        </TableRow>
                                        <TableRow hover onClick={() => Configurator.redirect(
                                            UrlStore.generateUrl("Admin", "TitleDetail", {id: title}),
                                            UrlStore.getPath("Admin", "TitleList")
                                        )}>
                                            <TableCell>{language.reservation.titleId}</TableCell>
                                            <TableCell>{entity.title === undefined ? "" : entity.title.name}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>{language.reservation.dateReservation}</TableCell>
                                            <TableCell>{entity.dateReservation}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>{language.reservation.dateExpiry}</TableCell>
                                            <TableCell>{entity.dateExpiry}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>{language.reservation.status}</TableCell>
                                            <TableCell style={{color: entity.status === "active" ? "green" : (entity.status === "expired" ? "red" : "SlateGray")}}>{entity.status ? entity.status.charAt(0).toUpperCase() + entity.status.slice(1) : ""}</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </CardBody>
                            {this.state.user && this.state.user.role !== "external"? this.generateButtons(language): null}
                            <Dialog
                                open={this.state.dialogOpen && this.state.dialogType === "extend"}
                                onClose={this.toggleDialog.bind(this, "")}>
                                <DialogTitle>Extend reservation expiry date?</DialogTitle>
                                <DialogContent>Are you sure you want to extend the expiry date on this reservation?</DialogContent>
                                <DialogActions>
                                    <Button onClick={this.toggleDialog.bind(this, "")} color="primary" autoFocus>
                                        Back
                                    </Button>
                                    <Button color="primary" onClick={this.extend.bind(this, language)}>
                                        {language.reservation.extend}
                                    </Button>
                                </DialogActions>
                            </Dialog>
                            <Dialog
                                open={this.state.dialogOpen && this.state.dialogType === "cancel"}
                                onClose={this.toggleDialog.bind(this, "")}>
                                <DialogTitle>Cancel reservation?</DialogTitle>
                                <DialogContent>Are you sure you want to cancel this reservation? Repeated creation of a reservation will move the user to the end of the waiting list.</DialogContent>
                                <DialogActions>
                                    <Button onClick={this.toggleDialog.bind(this, "")} color="primary" autoFocus>
                                        Back
                                    </Button>
                                    <Button color="primary" onClick={this.cancel.bind(this, language)}>
                                        {language.reservation.cancel}
                                    </Button>
                                </DialogActions>
                            </Dialog>
                            <Dialog
                                open={this.state.dialogOpen && this.state.dialogType === "reserve"}
                                onClose={this.toggleDialog.bind(this, "")}>
                                <DialogTitle>{language.rental.reserve}?</DialogTitle>
                                <DialogContent>Create a reservation a new reservation on this title?</DialogContent>
                                <DialogActions>
                                    <Button onClick={this.toggleDialog.bind(this, "")} color="primary" autoFocus>
                                        Back
                                    </Button>
                                    <Button color="primary" onClick={this.reserve.bind(this, language)}>
                                        {language.rental.reserve}
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </Card>

                    );
                }}
            </SystemContext.Consumer>
        </>
        );
    }
}

export default withStyles(style)(Detail);
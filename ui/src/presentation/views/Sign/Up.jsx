import React from "react";
import PropTypes from "prop-types";

import SignUpComponent from "../../Forms/User/SignUp";
import AbstractView from "../AbstractView";
import UserStore from "../../../stores/UserStore";
import UrlStore from "../../../stores/UrlStore";
import Configuration from "../../../variables/Configuration";
import { SystemContext } from "../../../variables/SystemContext";
import Button from "../../SmartComponents/Button";

class Up extends AbstractView {
  static propTypes = {
    backLink: PropTypes.string
  };
  static defaultProps = {
    backLink: UrlStore.getPath("Admin", "Dashboard")
  };
  constructor(props) {
    super(props);
    this.state.backLink = props.backLink;
    this.updateSigned();
  }

  componentDidMount() {
    super.componentDidMount();
    UserStore.addChangeListener(this.updateSigned);
  }
  componentWillUnmount() {
    super.componentWillUnmount();
    UserStore.removeChangeListener(this.updateSigned);
  }

  updateSigned = () => {
    if (UserStore.isSigned())
      Configuration.redirect(
        this.state.backLink,
        UrlStore.getPath("Sign", "SignUp")
      );
  };

  handleSignIn = () => {
    Configuration.redirect(
      UrlStore.getPath("Sign", "SignIn"),
      UrlStore.getPath("Sign", "SignUp")
    );
  };
  render() {
    return (
      <>
        {super.render()}
        <SystemContext.Consumer>
          {({ lang, langMapper }) => {
            let language = langMapper.getContent(lang);
            return (
              <div style={{ textAlign: "center" }}>
                <SignUpComponent />
                <Button
                  color={"transparent"}
                  round={true}
                  size={"lg"}
                  onClick={this.handleSignIn}
                >
                  {language.button.signIn}
                </Button>
              </div>
            );
          }}
        </SystemContext.Consumer>
      </>
    );
  }
}

export default Up;

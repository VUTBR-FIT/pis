import React from "react";
import PropTypes from "prop-types";

import SignInComponent from "../../Forms/User/SignIn";
import AbstractView from "../AbstractView";
import UserStore from "../../../stores/UserStore";
import UrlStore from "../../../stores/UrlStore";
import Button from "../../SmartComponents/Button";
import { SystemContext } from "../../../variables/SystemContext";
import Configuration from "../../../variables/Configuration";

class In extends AbstractView {
  static propTypes = {
    backLink: PropTypes.string
  };
  static defaultProps = {
    backLink: UrlStore.getPath("Admin", "Dashboard")
  };
  constructor(props) {
    super(props);
    this.state.backLink = props.backLink;
    this.updateSigned();
  }

  componentDidMount() {
    super.componentDidMount();
    UserStore.addChangeListener(this.updateSigned);
  }
  componentWillUnmount() {
    super.componentWillUnmount();
    UserStore.removeChangeListener(this.updateSigned);
  }

  updateSigned = () => {
    if (UserStore.isSigned())
      Configuration.redirect(
        this.state.backLink,
        UrlStore.getPath("Sign", "SignIn")
      );
  };

  handleSignUp = () => {
    Configuration.redirect(
      UrlStore.getPath("Sign", "SignUp"),
      UrlStore.getPath("Sign", "SignIn")
    );
  };

  render() {
    return (
      <>
        {super.render()}
        <SystemContext.Consumer>
          {({ lang, langMapper }) => {
            let language = langMapper.getContent(lang);
            return (
              <div style={{ textAlign: "center" }}>
                <SignInComponent />
                <Button
                  color={"transparent"}
                  round={true}
                  size={"lg"}
                  onClick={this.handleSignUp}
                >
                  {language.button.signUp}
                </Button>
              </div>
            );
          }}
        </SystemContext.Consumer>
      </>
    );
  }
}

export default In;

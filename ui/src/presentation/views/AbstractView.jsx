import React from "react";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import Configuration from "../../variables/Configuration";
import { Redirect } from "react-router-dom";
import Snackbar from "../SmartComponents/Snackbar";
import UrlStore from "../../stores/UrlStore";

export default class AbstractView extends AbstractComponent {
    constructor(props) {
        super(props);
        this.state = {};
        if (new.target === AbstractView) {
            throw new TypeError(
                "Cannot construct " +
                AbstractView.name +
                " class instances directly. Class is abstract class."
            );
        }

        this.state.messages = [];
        this.state.redirect = false;
        this.state.redirectRoute = undefined;
        //TODO if (!this.checkPermissions())
    }

    componentDidMount() {
        Configuration.addListener(Configuration.EVENT_MESSAGE, this.updateMessages);
        this.updateMessages();
        Configuration.addListener("EVENT_REDIRECT", this.updateRedirect);
    }
    componentWillUnmount() {
        Configuration.removeListener(
            Configuration.EVENT_MESSAGE,
            this.updateMessages
        );
        Configuration.removeListener("EVENT_REDIRECT", this.updateRedirect);
    }

    updateRedirect = () => {
        let routeData = Configuration.getRedirect();
        if (routeData !== undefined)
            this.setState({redirectRoute: routeData, redirect: true});
    };
    updateMessages = () => {
        this.setState({ messages: Configuration.getMessages()});
    };
    needRedirect() {
        return this.state.redirect === true && this.state.redirectRoute.to !== this.props.location.pathname;
    }

    /**
     * True <= User is allowed display this view
     * False <= otherwise
     */
    checkPermissions() {

    }

    generateMessages() {
        if (this.state.messages.length > 0) {
            let item = this.state.messages.shift();
            return (
                <Snackbar
                    key={item.id}
                    place={item.place || "tl"}
                    color={item.color}
                    message={item.message}
                    close={true}
                    open={true}
                    onClose={this.handleCloseMessage.bind(this, item.id)}
                    duration={item.duration}
                />
            );
        }
        return null;
    }
    handleCloseMessage = id => {
      this.setState({messages: this.state.messages.filter(message => message.id !== id)})
    };

    render() {
        if (this.needRedirect()) {
            let url = "";
            if (typeof this.state.redirectRoute.to === "string") url = this.state.redirectRoute.to;
            else url = UrlStore.getPath(this.state.redirectRoute.to.layout, this.state.redirectRoute.to.view);
            return <Redirect push={true} from={this.state.redirectRoute.from} to={url}/>;
        } else Configuration.getRedirect();

        return this.generateMessages();
    }
}


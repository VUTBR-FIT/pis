import React from "react";
import PropTypes from "prop-types";

import Plus from "@material-ui/icons/Add";

import { withStyles } from "@material-ui/core";

import style from "../../../assets/jss/material-dashboard-react/views/dashboardStyle";
import Datatable from "../../SmartComponents/Datatable";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import Filter from "../../SmartComponents/Filter";
import CardBody from "../../components/Card/CardBody";
import { SystemContext } from "../../../variables/SystemContext";
import CardFooter from "../../components/Card/CardFooter";
import GenreStore from "../../../stores/GenreStore";
import DialogForm from "../../Forms/GenreDialog";
import UrlStore from "../../../stores/UrlStore";
import AbstractView from "../AbstractView";
import Configurator from "../../../variables/Configuration";
import Button from "../../SmartComponents/Button";

class List extends AbstractView {
  constructor(props) {
    super(props);
    this.state.source = GenreStore.getAll();
    this.state.data = [];
    this.state.value = {
      fulltext: () => true
    };
  }

  componentDidMount() {
    super.componentDidMount();
    GenreStore.addChangeListener(this.updateData);
    this.updateData();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    GenreStore.removeChangeListener(this.updateData);
  }

  updateData = () => {
    this.setState({ source: GenreStore.getAll() });
  };
  handleFilter = data => {
    let newState = Object.assign({}, this.state);
    Object.assign(newState.value, data);
    if (data.fulltext && data.fulltext !== "") {
      let filters = this.header({ genre: {} });
      newState.value.fulltext = row => {
        for (let key in filters) {
          let value = filters[key].access(row);

          if (value !== null && value !== undefined) {
            if (
              (filters[key].type === undefined ||
                filters[key].type === "string") &&
              value.toLowerCase().indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
            if (
              filters[key].type === "number" &&
              value
                .toString()
                .toLowerCase()
                .indexOf(data.fulltext.toLowerCase()) > -1
            )
              return true;
          }
        }
        return false;
      };
    } else newState.value.fulltext = () => true;
    this.setState(newState);
  };

  header = (language, forFilter) => {
    let results = [
      {
        id: "genreName",
        name: language.genre.name,
        access: row => row.genreName
      },
      {
        id: "ageRestriction",
        type: "number",
        name: language.genre.ageRestriction,
        access: row => row.ageRestriction
      },
      {
        id: "description",
        name: language.genre.description,
        access: row => row.description
      }
    ];
    if (forFilter)
      results.push({
        id: "fulltext",
        name: language.dataTable.toolbar.search,
        access: () => {},
        options: {
          filter: "search"
        }
      });
    return results;
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.tableContainer}>
        {super.render()}
        <SystemContext.Consumer>
          {({ lang, langMapper }) => {
            let language = langMapper.getContent(lang);
            return (
              <Card>
                <CardHeader color="primary">
                  <Filter
                    filters={this.header(language, true)}
                    onChange={this.handleFilter}
                  />
                </CardHeader>
                <CardBody>
                  <Datatable
                    data={Filter.applyFilter(
                      this.state.source,
                      this.state.value
                    )}
                    keyRow={row => row.id}
                    header={this.header(language)}
                    optLanguage={language.dataTable}
                    optSortable={true}
                    optSortBy={"genreName"}
                    optSortDir={"asc"}
                    onSelectRow={id =>
                      Configurator.redirect(
                        UrlStore.generateUrl("Admin", "GenreDetail", { id }),
                        UrlStore.getPath("Admin", "GenreList")
                      )
                    }
                  />
                </CardBody>
                <CardFooter>
                  <DialogForm
                    buttonRender={() => props => (
                      <Button {...props} color={"primary"}>
                        <Plus /> {language.genre.create}
                      </Button>
                    )}
                  />
                </CardFooter>
              </Card>
            );
          }}
        </SystemContext.Consumer>
      </div>
    );
  }
}

List.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(style)(List);

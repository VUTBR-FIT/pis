import React from "react";
import PropTypes from "prop-types";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  withStyles
} from "@material-ui/core";

import style from "../../../assets/jss/material-dashboard-react/components/tableStyle";
import AbstractView from "../AbstractView";
import { SystemContext } from "../../../variables/SystemContext";
import Store from "../../../stores/GenreStore";
import UserStore from "../../../stores/UserStore";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import CardFooter from "../../components/Card/CardFooter";
import GenreDialog from "../../Forms/GenreDialog";
import UrlStore from "../../../stores/UrlStore";
import TitleStore from "../../../stores/TitleStore";
import Configuration from "../../../variables/Configuration";
import Datatable from "../../SmartComponents/Datatable";
import Configurator from "../../../variables/Configuration";
import DeleteDialog from "../../components/DeleteDialog";
import CardHeader from "../../components/Card/CardHeader";

class Detail extends AbstractView {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    let id = props.match.params.id;

    this.state.id = parseInt(id);
    this.state.entity = Store.getById(this.state.id);
    this.state.relatedTitles = [];
    this.state.user = UserStore.getUser();
    this.updateTitles(true);
    this.state.isDialogOpen = false;
  }

  componentDidMount() {
    super.componentDidMount();
    Store.addChangeListener(this.updateEntity);
    TitleStore.addChangeListener(this.updateTitles);
    UserStore.addChangeListener(this.updateUser);
    this.updateEntity();
  }
  componentWillUnmount() {
    super.componentWillUnmount();
    Store.removeChangeListener(this.updateEntity);
    TitleStore.removeChangeListener(this.updateTitles);
    UserStore.removeChangeListener(this.updateUser);
  }

  updateEntity = () => {
    this.setState({
      entity: Store.getById(this.state.id)
    });
  };

  updateUser = () => {
    this.setState({
      user: UserStore.getUser()
    })
  };

  updateTitles = init => {
    if (this.state.id === undefined)
      return;
    let _id = this.state.id;
    let titleList = TitleStore.getAll();
    titleList = titleList.filter( item => {
        for (let iter = 0; iter < item.genres.length; iter++) {
          if (item.genres[iter].id === _id)
            return true;
        }
        return false;
      }
    );
    if (!titleList.length)
      return;
    if (init) this.state.relatedTitles = titleList;
    else this.setState({ relatedTitles: titleList });
  };

  handleDelete = () => {
    Store.delete(this.state.id, () => {
        Configuration.redirect(
            UrlStore.getPath("Admin", "GenreList"),
            UrlStore.getPath("Admin", "GenreDetail")
        );
        let language = Configuration.getLanguage();
        Configuration.addMessage("primary", language.message["genre_delete"]);
      }
    );
  };

  titleHeader = language => {
    let results = [
      {
        id: "authors",
        name: language.title.authors,
        access: row => row.authors.map(item => item.name).join("; ")
      },
      {
        id: "title",
        name: language.title.titleName,
        access: row => row.titleName
      },
      {
        id: "isbn",
        name: language.title.isbn,
        access: row => row.isbn
      },
      {
        id: "ean",
        name: language.title.ean,
        access: row => row.ean
      }
    ];
    if (this.state.user && this.state.user.role !== "external")
    results.push({
        id: "copies",
        name: language.title.copyCount,
        access: row => row.copiesCount
      });
    return results;
  };

  render() {
    const { classes } = this.props;
    return (

        <SystemContext.Consumer>
          {({ lang, langMapper }) => {
            let language = langMapper.getContent(lang);
            let entity = this.state.entity || {};
            return (
              <>
              {super.render()}
              <Card>
                <CardBody>
                  <Table className={classes.table}>
                    <TableBody>
                      <TableRow>
                        <TableCell>{language.genre.name}</TableCell>
                        <TableCell>{entity.genreName}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.genre.ageRestriction}</TableCell>
                        <TableCell>{entity.ageRestriction}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.genre.description}</TableCell>
                        <TableCell>{entity.description}</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </CardBody>
                <CardFooter>
                  <GenreDialog
                    buttonColor={"primary"}
                    entity={this.state.entity}
                  />
                  <DeleteDialog
                    title={"Delete Genre?"}
                    content={language.genre.delete+ " " + this.state.entity.genreName + "?"}
                    onDelete={this.handleDelete}
                    deleteValue={"Delete Genre"}
                    buttonValue={language.button.delete}
                  />
                </CardFooter>
              </Card>
              <Card>
                <CardHeader color={"primary"}>{language.genre.relatedTitles}</CardHeader>
                <CardBody>
                  <Datatable
                    data={this.state.relatedTitles || []}
                    header={this.titleHeader(language)}
                    keyRow={row => row.id}
                    optLanguage={language.dataTable}
                    optSortBy={"title"}
                    optSortDir={"asc"}
                    onSelectRow={id =>
                        Configurator.redirect(
                            UrlStore.generateUrl("Admin", "TitleDetail", { id }),
                            UrlStore.getPath("Admin", "TitleList")
                        )
                    }
                    />
                </CardBody>
              </Card>
              </>
            );
          }}
        </SystemContext.Consumer>
    );
  }
}

export default withStyles(style)(Detail);

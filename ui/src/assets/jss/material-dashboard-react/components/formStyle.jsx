import { whiteColor } from "../../material-dashboard-react";

const styles = theme => ({
  closeIcon: {
    float: "right"
  },
  formControl: {
    width: "100%"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  primaryRoot: {
    color: whiteColor
  },
  primaryInput: {},
  primaryLabel: {
    color: whiteColor,
    "&$primaryLabelFocused": {
      color: whiteColor
    }
  },
  primaryLabelFocused: {
    // color: whiteColor
  }
});

export default styles;

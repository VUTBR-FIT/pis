import {
  primaryColor,
  whiteColor,
  primaryBoxShadow
} from "../../material-dashboard-react.jsx";

export default {
  appBar: {
    position: "relative",
    backgroundColor: primaryColor[2],
    color: whiteColor,
    ...primaryBoxShadow
  },
  title: {
    color: whiteColor
  },
  titleIcon: {
    color: whiteColor
  },
  simpleTitle: {
    color: primaryColor
  },
  simpleTitleIcon: {
    color: primaryColor
  }
};

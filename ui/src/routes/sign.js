import In from "../presentation/views/Sign/In";
import Up from "../presentation/views/Sign/Up";

import UrlStore from "../stores/UrlStore";

const routes = [
  {
    path: UrlStore.getPath("Sign", "SignIn"),
    component: In
  },
  {
    path: UrlStore.getPath("Sign", "SignUp"),
    component: Up
  }
];

export default routes;

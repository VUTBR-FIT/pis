import Dashboard from "../presentation/views/Dashboard";
import GenreList from "../presentation/views/Genre/List";
import GenreDetail from "../presentation/views/Genre/Detail";
import UserList from "../presentation/views/User/List";
import UserDetail from "../presentation/views/User/Detail";
import TitleList from "../presentation/views/Title/List";
import TitleDetail from "../presentation/views/Title/Detail";
import ReservationList from "../presentation/views/Reservation/List"
import ReservationDetail from "../presentation/views/Reservation/Detail"
import AuthorList from "../presentation/views/Author/List";
import AuthorDetail from "../presentation/views/Author/Detail";
import RentalList from "../presentation/views/Rental/List";
import RentalDetail from "../presentation/views/Rental/Detail";

import UrlStore from "../stores/UrlStore";
import config from "../variables/Configuration";

let language = config.langMapper.getContent(config.lang).menu;

const routes = [
  {
    path: UrlStore.getPath("Admin", "Dashboard"),
    title: language.dashboard.title,
    sidebarName: language.dashboard.sidebar,
    icon: "dashboard",
    component: Dashboard
  },
  {
    path: UrlStore.getPath("Admin", "TitleList"),
    title: language.title.title,
    sidebarName: language.title.sidebar,
    icon: "library_books",
    component: TitleList,
    exact: true
  },
  {
    hidden: true,
    path: UrlStore.getPath("Admin", "TitleDetail"),
    title: language.title.titleDetail,
    component: TitleDetail
  },
  {
    path: UrlStore.getPath("Admin", "GenreList"),
    title: language.genre.title,
    sidebarName: language.genre.sidebar,
    icon: "loyalty",
    component: GenreList,
    exact: true
  },
  {
    hidden: true,
    path: UrlStore.getPath("Admin", "GenreDetail"),
    title: language.genre.titleDetail,
    component: GenreDetail
  },
  {
    path: UrlStore.getPath("Admin", "AuthorList"),
    title: language.author.title,
    sidebarName: language.author.sidebar,
    icon: "person_outline",
    component: AuthorList,
    exact: true
  },
  {
    hidden: true,
    path: UrlStore.getPath("Admin", "AuthorDetail"),
    title: language.author.titleDetail,
    component: AuthorDetail
  },
  {
    path: UrlStore.getPath("Admin", "RentalList"),
    title: language.rental.title,
    sidebarName: language.rental.sidebar,
    // icon: "folder_shared",
    icon: "bookmark",
    component: RentalList,
    exact: true
  },
  {
    hidden: true,
    path: UrlStore.getPath("Admin", "RentalDetail"),
    title: language.rental.titleDetail,
    component: RentalDetail
  },
  {
    path: UrlStore.getPath("Admin", "ReservationList"),
    title: language.reservation.title,
    sidebarName: language.reservation.sidebar,
    // icon: "folder_special",
    icon: "bookmark_border",
    component: ReservationList,
    exact: true
  },
  {
    hidden: true,
    path: UrlStore.getPath("Admin", "ReservationDetail"),
    title: language.reservation.titleDetail,
    component: ReservationDetail
  },
  {
    path: UrlStore.getPath("Admin", "UserList"),
    title: language.user.title,
    sidebarName: language.user.sidebar,
    icon: "people",
    component: UserList,
    exact: true,
    permission: role => role === "admin" || role === "employee"
  },
  {
    hidden: true,
    path: UrlStore.getPath("Admin", "UserDetail"),
    title: language.user.titleDetail,
    component: UserDetail
  }
];

const data = routes.map(route => {
  if (route.permission === undefined)
    route.permission = role => role !== undefined;
  return route;
});

export default data;

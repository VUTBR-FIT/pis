import { EventEmitter } from "events";
import UrlStore from "../stores/UrlStore";
import Cookie from "../utils/Cookie";

class Configuration extends EventEmitter {
  EVENT_CHANGE = "CONFIG_CHANGE";
  EVENT_MESSAGE = "CONFIG_MESSAGE";
  EVENT_REDIRECT = "CONFIG_REDIRECT";

  tokenCheckSeconds = 5 * 60;
  tokenCheckSecondsPercent = 0.1; // 10%
  tokenExpiryChecks = 10;
  userInactiveMinutes = 10;
  defaultLang = "en";
  lang = "en";
  apiUrl = "http://localhost:8000/api";
  token = undefined;
  tokenCheckCounter = 0;
  langMapper = require("../lang/mapper");
  redirectRoute = undefined;
  lastRedirect = new Date();
  timer = null;
  messages = [];
  messageId = 0;

  constructor() {
    super();
    if (this.timer != null)
      this.clearTimer();
    let token = Cookie.get("token");
    this.tokenExpiryTime = Cookie.get("tokenExpiry");
    if (token !== undefined && token.length > 0) {
      this.token = token;
      this.startTokenCounter();
    }
  }

  getToken() {
    return this.token;
  }
  getUserId() {
    return Cookie.get("userId");
  }

  initUserData(userId, token, expiredTime) {
    this.token = token;
    this.tokenExpiryTime = expiredTime;
    Cookie.set("userId", userId, expiredTime);
    this.setToken(token, expiredTime);
    this.startTokenCounter();
  }
  clearUserData() {
    this.token = undefined;
    Cookie.set("token", "", 0);
    Cookie.set("userId", "", 0);
    Cookie.set("tokenExpiry", "", 0);
  }
  setToken(token, expiredTime) {
    Cookie.set("token", token, expiredTime);
    Cookie.set("tokenExpiry", expiredTime, expiredTime);
  }

  startTokenCounter() {
    clearInterval(this.timer);
    this.timer = setInterval(this.tokenTick.bind(this), this.checkTokenExpiry() * 1000);
  }

  tokenTick() {
    if (this.token === undefined || !this.token.length)
      return;
    this.tokenCheckCounter++;
    if (this.tokenCheckCounter >= this.tokenExpiryChecks) {
      this.clearTimer();
      if (!this.isUserActive()) {
        UrlStore.logout();
      } else {
        // console.log(new Date() - this.lastRedirect, "since last activity");
        let _this = this;
        UrlStore.refreshToken(response => {
          _this.setToken(response.token, response.tokenCreated);
          _this.tokenCheckCounter = 0;
          _this.startTokenCounter();
        });
      }
    }
  }

  clearTimer() {
    clearInterval(this.timer);
    this.timer = null;
  }

  getLanguage() {
    return this.langMapper.getContent(this.lang);
  }

  logOperation() {
    this.lastRedirect = new Date();
  }

  redirect(destination, source) {
    this.redirectRoute = {
      from: source,
      to: destination
    };
    this.emit("EVENT_REDIRECT");
  }

  isUserActive = () => {
      let now = new Date() - this.lastRedirect;
      now /= (1000 * 60);
      return now <= this.userInactiveMinutes;
  };

  getRedirect() {
    let data = this.redirectRoute;
    if (this.redirectRoute !== undefined)
      this.logOperation();
    this.redirectRoute = undefined;
    return data;
  }

  addMessage(type, message, position = "bl") {
    let id = this.messageId++;
    this.messages.push({
      id: id,
      place: position || "bl",
      message: Array.isArray(message) ? message.shift() : message,
      color: type
    });
    this.emit(this.EVENT_MESSAGE);
  }
  
  getMessages() {
    let data = this.messages;
    this.messages = [];
    return data;
  }

  badPermissions() {
    this.redirect({ layout: "Admin", view: "Dashboard" });
    this.addMessage("danger", this.getLanguage().errorMessage[401]);
  }

  emitClear() {
    this.emit("EVENT_CLEAR");
  }

  checkTokenExpiry() {
    return Math.min(this.tokenExpiryTime * this.tokenCheckSecondsPercent, this.tokenCheckSeconds);
  }
}

export default new Configuration();

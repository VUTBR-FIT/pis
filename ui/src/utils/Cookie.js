export default class Cookie {
    static set(cName, cValue, exSeconds) {
        let d = new Date();
        d.setTime(d.getTime() + exSeconds * 1000);
        let expires = "expires=" + d.toUTCString();
        document.cookie = cName + "=" + cValue + ";" + expires + ";path=/";
    }

    static get(cname) {
        let name = cname + "=";
        let data = document.cookie.split(";");
        for (let id in data) {
            let cookie = data[id];
            while (cookie.charAt(0) === " ") {
                cookie = cookie.substring(1);
            }

            if (cookie.indexOf(name) === 0) {
                let parts = cookie.substring(name.length, cookie.length).split(" ");
                if (parts.length !== 0) return parts[0];
            }
        }
        return "";
    }
}
export default class DateUtil {
  /**
   *
   * @source https://www.w3resource.com/javascript-exercises/javascript-date-exercise-2.php
   * @param sp
   * @param day
   * @param month
   * @param year
   * @returns {*}
   */
  static getCurrentDate(sp, day, month, year) {
    let today = new Date();
    if (day !== undefined) today.setDate(today.getDate() + day);
    if (month !== undefined) today.setMonth(today.getMonth() + month);
    if (year !== undefined) today.setFullYear(today.getFullYear() + year);

    let dd = today.getDate();
    let mm = today.getMonth() + 1; //As January is 0.
    let yyyy = today.getFullYear();

    if (dd < 10) dd = "0" + dd;
    if (mm < 10) mm = "0" + mm;
    return yyyy + sp + mm + sp + dd;
  }

  static transformDate(value, reverse) {
    if (value === undefined)
      return undefined;
    if (reverse) {
      let temp = value.split(/\.[\s]+/);
      return temp[2] + ". " + temp[1] + ". " + temp[0];
    } else {
      let temp = value.split(/\.[\s]+/);
      return temp[2] + "-" + temp[1] + "-" + temp[0];
    }
  }
}

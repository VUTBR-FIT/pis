import Configuration from "../variables/Configuration";

import { EventEmitter } from "events";
import StringUtil from "../utils/String";
import CookieUtil from "../utils/Cookie";
import Dispatcher from "../Dispatcher";

export default class AbstractStore extends EventEmitter {
  constructor() {
    super();
    this.setCookie = CookieUtil.set;
    this.getCookie = CookieUtil.get;
    this._config = Configuration;
    this.actionChange = "CHANGE";
    this.detailList = [];
    this.finishedRequest = [];
    this.list = [];
    this.initialized = false;

    if (new.target === AbstractStore) {
      throw new TypeError(
        "Cannot construct " +
          AbstractStore.name +
          " class instances directly. Empty is abstract class."
      );
    }
    this.setMaxListeners(20);
    Dispatcher.register(this.clearData.bind(this));
  }
  _itemUpdate(id, item) {
    this.list = this.list.filter(item => parseInt(item.id) !== parseInt(id));
    this.list.push(item);
  }
  _byId(id, path, onSuccess, onError) {
    if (this.detailList.indexOf(parseInt(id)) !== -1)
      return this.list.filter(item => parseInt(item.id) === parseInt(id))[0];

    this._initialization();
    let _this = this;
    this.initRequest(
      path,
      "GET",
      true,
      response => {
        _this.list = _this.list.filter(item => item.id !== id);
        _this.list.push(response);
        _this.detailList.push(parseInt(id));
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      onError,
      false
    );

    return {};
  }
  _getAll(path, onSuccess, onError) {
    let _this = this;
    this._initialization();
    this.initRequest(
      path,
      "GET",
      true,
      response => {
        response.map(item => {
          if (this.detailList.indexOf(item.id) === -1) _this.list.push(item);
          return null;
        });
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      onError,
      false
    );
    return Object.values(this.list);
  }
  _add(path, data, onSuccess, onError) {
    let _this = this;
    this._initialization();
    this.initRequest(
      path,
      "POST",
      true,
      response => {
        _this.list.push(response);
        _this.detailList.push(response.id);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      onError,
      false,
      data
    );
  }
  _update(path, data, onSuccess, onError) {
    let _this = this;
    this._initialization();
    this.initRequest(
      path,
      "PUT",
      true,
      response => {
        _this._itemUpdate(response.id, response);
        _this.detailList.push(response.id);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      onError,
      false,
      data
    );
  }
  _delete(id, path, onSuccess, onError) {
    let _this = this;
    this._initialization();
    this.initRequest(
      path,
      "DELETE",
      true,
      response => {
        _this.list = _this.list.filter(item => item.id !== id);
        _this.detailList = _this.detailList.filter(item => item !== id);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      onError,
      false
    );
  }

  logout = () => {
    Configuration.clearUserData();
    let language = Configuration.getLanguage();
    this.initRequest(
      this.getApiPath("/logout"),
      "POST",
      true,
      () => {
        Configuration.addMessage("danger", language.message.signout);
        Configuration.redirect("/");
      },
      () => {
        Configuration.addMessage("danger", language.errorMessage.oops);
        Configuration.redirect("/");
      }
    )
  };

  refreshToken(onSuccess) {
    this.initRequest(
      this.getApiPath("/refresh"),
      "POST",
      true,
      onSuccess,
      () => {
        Configuration.clearUserData();
        let language = Configuration.getLanguage();
        Configuration.addMessage("danger", language.errorMessage.refreshFail);
        this.logout();
      }
    );
  }

  _getListeners() {}
  _initialization() {
    if (this.initialized) return;
    this._getListeners();
  }
  /**
   * Validation of input data before send to server (remove undefined values)
   * @param data
   * @returns {*}
   */
  validateData(data) {
    for (let key in data) {
      if (data[key] === undefined) delete data[key];
    }
    return data;
  }

  getDifference(id, result) {
    let entity = this.getById(id);
    for (let key in result) {
      if (key in entity && entity[key] === result[key]) delete result[key];
    }
    return result;
  }

  /**
   * Load init data
   */
  init() {}

  /**
   * Get all items (by current language)
   * @return Array
   */
  getAll() {
    throw new TypeError("Not implemented");
  }

  /**
   * Get item by ID
   * @param id
   * @return Object
   */
  getById(id) {
    throw new TypeError("Not implemented");
  }

  getApiPath = (path = "") => {
    return Configuration.apiUrl + path;
  };
  /**
   * Register callback to CHANGE event
   * @param callback Callback function
   */
  addChangeListener = callback => {
    this.addListener(this.actionChange, callback);
  };
  /**
   * Register callback to specific event
   * @param type Type of callback
   * @param callback Callback function
   */
  addTypeListener = (type, callback) => {
    this.addListener(type, callback);
  };
  /**
   * Remove callback form CHANGE event
   * @param callback Callback function
   */
  removeChangeListener = callback => {
    this.removeListener(this.actionChange, callback);
  };
  /**
   * Remove callback form specific event
   * @param type Type of callback
   * @param callback Callback function
   */
  removeTypeListener = (type, callback) => {
    this.removeListener(type, callback);
  };

  /**
   *
   * @param url
   * @param method
   * @param asynch
   * @param onSuccess
   * @param onError
   * @param ajax
   * @param data
   * @param isJson
   * @param addToken
   * @param createForm
   */
  initRequest(
    url,
    method,
    asynch,
    onSuccess,
    onError,
    ajax,
    data,
    isJson = true,
    addToken = true,
    createForm = false
  ) {
    if (this.finishedRequest.indexOf(method + " " + url) !== -1) return;
    if (method !== "POST" && method !== "PUT")
      this.finishedRequest.push(method + " " + url);
    let xhr;
    if (window.XMLHttpRequest) {
      xhr = new XMLHttpRequest();
      xhr.withCredentials = true;
    } else {
      xhr = new window.ActiveXObject("Microsoft.XMLHTTP");
    }
    if (Configuration.token === undefined)
      Configuration.token = CookieUtil.get("token");

    xhr.open(method, url, asynch);
    if (addToken) {
      xhr.setRequestHeader("Authorization", "Bearer " + Configuration.token);
    }

    // Ajax request
    if (ajax === true)
      xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    else
      xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");

    let _this = this;
    xhr.onload = () => {
      let response = {};
      try {
        response = JSON.parse(xhr.responseText);
      } catch (syntaxError) {
        response = {};
      }
      if (xhr.status >= 200 && xhr.status < 300) {
        if (onSuccess !== undefined) {
          onSuccess(this._transformNaming(response, false), xhr);
        }
      } else {
        if (method !== "POST" && method !== "PUT") {
          let path = method + " " + url;
          _this.finishedRequest = _this.finishedRequest.filter(
            _path => _path.localeCompare(path) !== 0
          );
        }
        response = this._transformNaming(response, false);
        if ("statusCode" in response) {
          switch (response.statusCode) {
            case 500:
              if (response.message === "Token has expired") {
                Configuration.addMessage("danger", response.message);
                Configuration.redirect("/");
                // this.refreshToken();
              }
                break;
            case 404:
              Configuration.redirect("/error/404");
              break;
            case 401:
              Configuration.badPermissions();
            default:
              if (onError !== undefined) onError(response, xhr);
          }
        }
      }
    };
    if (method === "POST" || method === "PUT") {
      if (!isJson) {
        if (createForm) {
          data = this.validateData(data);
          if (method === "PUT") {
            let id = parseInt(url.match(/([0-9]+)\/*$/)[0]);
            data = this.getDifference(id, data);
          }
          data = this._transformNaming(data, true);
          if (Object.keys(data).length === 0) {
            if (onSuccess !== undefined)
              Configuration.addMessage(
                "info",
                Configuration.getLanguage().infoMessage.noChange
              );
            return;
          }
          let form = new FormData();
          for (let key in data) {
            if (Array.isArray(data[key])) {
              data[key].map((item, id) => {
                form.append(key + "[" + id + "]", item);
                return null;
              });
            } else form.append(key, data[key]);
          }
          xhr.send(form);
        } else xhr.send(data);
      } else {
        data = this.validateData(data);
        if (method === "PUT") {
          let id = parseInt(url.match(/([0-9]+)\/*$/)[0]);
          data = this.getDifference(id, data);
        }
        data = this._transformNaming(data || {}, true);
        if (Object.keys(data).length === 0) {
          Configuration.addMessage(
            "info",
            Configuration.getLanguage().infoMessage.noChange
          );
          return;
        }
        xhr.send(JSON.stringify(data));
      }
    } else {
      xhr.send();
    }
  }

  emitChange() {
    this.emit(this.actionChange);
  }

  _transformNaming = (data, reverse) => {
    let result = {};
    if (
      typeof data === "object" &&
      Object.keys(data).length === 1 &&
      data.data !== undefined
    ) {
      data = data.data;
    }
    if (Array.isArray(data)) {
      return this._transformNamingArray(data, reverse);
    } else if (typeof data === "object" && data !== null) {
      for (let key in data) {
        let _key = "";
        if (reverse) _key = StringUtil.toSnakeCase(key);
        else _key = StringUtil.toCamelCase(key);
        if (Array.isArray(data[key])) {
          result[_key] = this._transformNamingArray(data[key], reverse);
        } else if (typeof data[key] === "object" && data[key] !== null) {
          result[_key] = this._transformNaming(data[key], reverse);
        } else {
          if (reverse) result[_key] = data[key] === undefined ? null : data[key];
          else result[_key] = data[key] === null ? undefined : data[key];
        }
      }
    } else return data;
    return result;
  };
  _transformNamingArray = (data, reverse) => {
    return data.map(item => {
      if (Array.isArray(item)) return this._transformNamingArray(item, reverse);
      else if (typeof item === "object")
        return this._transformNaming(item, reverse);
      else {
        if (reverse) return item === undefined ? null : item;
        else return item === null ? undefined : item;
      }
    });
  };
  clearData(data) {
    if (data.type === "CLEAR") {
      setTimeout(() => {
      this.list = [];
      this.detailList = [];
      this.finishedRequest = [];
      this.initialized = false;
      }, 500);
    }
  }
}

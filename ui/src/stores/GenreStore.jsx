import AbstractStore from "./AbstractStore";

class GenreStore extends AbstractStore {
  getById(id) {
    return this._byId(id, this.getApiPath("/genre/" + id));
  }

  getAll() {
    return this._getAll(this.getApiPath("/genre"));
  }

  add(data, onSuccess, onError) {
    this._add(this.getApiPath("/genre"), data, onSuccess, onError);
  }

  update(id, data, onSuccess, onError) {
    this._update(this.getApiPath("/genre/" + id), data, onSuccess, onError);
  }

  delete(id, onSuccess, onError) {
    this._delete(id, this.getApiPath("/genre/" + id), onSuccess, onError);
  }
}

export default new GenreStore();

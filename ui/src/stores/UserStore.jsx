import AbstractStore from "./AbstractStore";
import Actions from "../actions";
import Configuration from "../variables/Configuration";
import DateUtil from "../utils/Date";
import RentalStore from "./RentalStore";
import ReservationStore from "./ReservationStore";

class UserStore extends AbstractStore {
  constructor() {
    super();
    this.actionUser = "ACTION_USER";
    this.signed = false;
    this.user = {};
    this.userId = undefined;
    this.init();
  }

  init() {
    let userId = Configuration.getUserId();
    let token = Configuration.getToken();
    if (token !== undefined && token.length > 0) {
      Configuration.startTokenCounter();
      this.userId = userId;
      this.signed = true;
    }
    this.emit(this.actionChange);
  }

  getById(id) {
    let item = this._byId(id, this.getApiPath("/user/" + id));
    if (item.id) {
      (item.rentals || []).map(item => {
        RentalStore.getById(item.id);
        return null;
      });
      (item.reservations || []).map(item => {
        ReservationStore.getById(item.id);
        return null;
      });
    }
    return item;
  }

  getAll() {
    return this._getAll(this.getApiPath("/user"));
  }

  add(data, onSuccess, onError) {
    this._add(this.getApiPath("/user"), data, onSuccess, onError);
  }

  update(id, data, onSuccess, onError) {
    this._update(this.getApiPath("/user/" + id), data, onSuccess, onError);
  }

  delete(id, onSuccess, onError) {
    this._delete(id, this.getApiPath("/user/" + id), onSuccess, onError);
  }

  validateData(data) {
    for (let key in data)
      if (data[key] === undefined || data[key] === null) delete data[key];
    return AbstractStore.prototype.validateData.call(this, data);
  }

  getDifference(id, result) {
    let entity = this.getById(id);
    let temp = Object.assign({}, result);
    if (
      entity.role !== undefined &&
      this.getRoleName(entity.role).localeCompare(
        this.getRoleName(temp.role)
      ) === 0
    )
      delete temp.role;
    if (
      entity.dateBirth !== undefined &&
      DateUtil.transformDate(entity.dateBirth).localeCompare(temp.dateBirth) ===
        0
    )
      delete temp.dateBirth;

    return AbstractStore.prototype.getDifference.call(this, id, temp);
  }

  signIn(email, password, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/login"),
      "POST",
      true,
      response => {
        _this.user = response.user;
        Configuration.initUserData(_this.user.id, response.token, response.tokenCreated);
        _this.userId = _this.user.id;
        _this.signed = true;
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      onError,
      false,
      { email, password },
      true,
      false
    );
  }

  signUp(data, onSuccess, onError) {
    let _this = this;
    data.reservationCapacity = 5;
    data.rentalCapacity = 3;
    this.initRequest(
      this.getApiPath("/register"),
      "POST",
      true,
      response => {
        _this.user = response.user;
        Configuration.initUserData(_this.user.id, response.token, response.tokenCreated);
        _this.userId = _this.user.id;
        _this.signed = true;
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      onError,
      false,
      data,
      true,
      false
    );
  }

  isSigned() {
    return this.signed === true;
  }

  getUser() {
    if (this.user.id === undefined && this.userId !== undefined) {
      if (this.inProcessUser !== true) {
        this.inProcessUser = true;
        let _this = this;
        this.initRequest(
          this.getApiPath("/user/" + this.userId),
          "GET",
          true,
          response => {
            _this.user = response;
            _this._itemUpdate(this.user.id, response);
            _this.detailList.push(this.user.id);
            _this.inProcessUser = false;
            _this.emit(_this.actionUser);
            _this.emit(_this.actionChange);
          },
          () => {
            _this.inProcessUser = false;
            _this.signOut();
          }
        );
      }
    }
    return this.user;
  }

  signOut() {
    Configuration.clearUserData();
    this.signed = false;
    this.userId = undefined;
    this.user = {};
    this.emit(this.actionChange);
    Actions.clearStore();
  }

  getRoleMapping() {
    let language = Configuration.langMapper.getContent(Configuration.lang);
    return [
      { id: "admin", numId: 1, name: language.user.roleAdmin },
      { id: "employee", numId: 2, name: language.user.roleEmployee },
      { id: "external", numId: 3, name: language.user.roleExternal }
    ];
  }

  getRole(id) {
    let result = this.getRoleMapping().filter(item => {
      if (typeof id === "string") return item.id === id;
      else return item.numId === parseInt(id);
    });
    if (result.length) return result[0];
    return {};
  }

  getRoleName(id) {
    return this.getRole(id).name;
  }
}

export default new UserStore();

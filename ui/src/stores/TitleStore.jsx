import AbstractStore from "./AbstractStore";

class TitleStore extends AbstractStore {
  getById(id) {
    return this._byId(id, this.getApiPath("/title/" + id));
  }

  getAll() {
    return this._getAll(this.getApiPath("/title"));
  }

  delete(id, onSuccess, onError) {
    return this._delete(id, this.getApiPath("/title/" + id), onSuccess, onError);
  }

  add(data, onSuccess, onError) {
    this._add(this.getApiPath("/title"), data, onSuccess, onError);
  }

  update(id, data, onSuccess, onError) {
    this._update(this.getApiPath("/title/" + id), data, onSuccess, onError);
  }

  addCopy(titleId, healthPercentage, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/copy"),
      "POST",
      true,
      response => {
        _this.list = _this.list.map(item => {
          if (item.id === titleId) {
            item.copies.push(response);
            item.copiesCount += 1;
          }
          return item;
        });
        _this.list.push(response);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      onError,
      false,
      {
        titleId: titleId,
        healthPercentage:
          healthPercentage > 1.0 ? healthPercentage / 100 : healthPercentage
      }
    );
  }

  updateCopy(id, data, onSuccess, onError) {
    let _this = this;
    this.initRequest(
        this.getApiPath("/copy/" + id),
        "PUT",
        true,
        response => {
          _this.list = _this.list.map(item => {
            if (_this.detailList.indexOf(item.id) !== -1) {
              item.copies = item.copies.map(copyItem => {
                if (copyItem.id === id) return response;
                return copyItem;
              });
            }
            return item;
          });
          _this.emit(_this.actionChange);
          if (onSuccess !== undefined) onSuccess(response);
        },
        onError,
        false,
        {
          health_percentage: data.healthPercentage > 1.0 ? data.healthPercentage / 100 : data.healthPercentage
        }
    );
  }

  deleteCopy(id, onSuccess, onError) {
    let _this = this;
    this.initRequest(
        this.getApiPath("/copy/" + id),
        "DELETE",
        true,
        response => {
          _this.list = _this.list.map(item => {
            if (_this.detailList.indexOf(item.id) !== -1) {
              item.copies = item.copies.filter(copyItem => copyItem.id !== id);
            }
            return item;
          });
          _this.emit(_this.actionChange);
          if (onSuccess !== undefined) onSuccess(response);
        },
        onError,
        false
    );
  }

  getMediumOptions() {
    return [
      { id: "Book", name: "Book" },
      { id: "Magazine", name: "Magazine" },
      { id: "Audiobook", name: "Audiobook" },
      { id: "CD", name: "CD" },
      { id: "Other", name: "Other" }
    ];
  }
  validateMedium(value) {
    return (
      this.getMediumOptions()
        .map(item => item.id)
        .indexOf(value) !== -1
    );
  }
}

export default new TitleStore();

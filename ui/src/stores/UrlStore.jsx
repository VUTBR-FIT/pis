import AbstractStore from "./AbstractStore";
import Configuration from "../variables/Configuration";

class UrlStore extends AbstractStore {
  constructor() {
    super();

    this.init();
  }

  init() {
    this.layoutList = [
      {
        name: "Sign",
        path: "/sign/"
      },
      {
        name: "Admin",
        path: "/admin/"
      }
    ];
    this.urlList = [
      {
        path: "in",
        view: "SignIn",
        lang: "en"
      },
      {
        path: "up",
        view: "SignUp",
        lang: "en"
      },
      {
        path: "dashboard",
        view: "Dashboard",
        lang: "en"
      },
      {
        path: "rental",
        view: "RentalList",
        lang: "en"
      },
      {
        path: "rental/:id",
        view: "RentalDetail",
        lang: "en"
      },
      {
        path: "genre",
        view: "GenreList",
        lang: "en"
      },
      {
        path: "genre/:id",
        view: "GenreDetail",
        lang: "en"
      },
      {
        path: "user",
        view: "UserList",
        lang: "en"
      },
      {
        path: "user/:id",
        view: "UserDetail",
        lang: "en"
      },
      {
        path: "title",
        view: "TitleList",
        lang: "en"
      },
      {
        path: "title/:id",
        view: "TitleDetail",
        lang: "en"
      },
      {
        path: "author",
        view: "AuthorList",
        lang: "en"
      },
      {
        path: "author/:id",
        view: "AuthorDetail",
        lang: "en"
      },
      {
        path: "reservation",
        view: "ReservationList",
        lang: "en"
      },
      {
        path: "reservation/:id",
        view: "ReservationDetail",
        lang: "en"
      }
    ];

    this.emit(this.actionChange);
  }

  getPath(layout, view) {
    return this.getLayoutPath(layout) + this.getViewPath(view);
  }

  getLayoutPath(layout) {
    let urls = this.layoutList.filter(item => item.name === layout);

    if (urls.length !== 1) {
      urls = urls.filter(url => url.lang === this._config.lang);
    }
    return urls[0].path;
  }

  getViewPath(view) {
    let urls = this.urlList.filter(url => {
      return (
        url.view === view &&
        (url.lang === Configuration.lang ||
          url.lang === Configuration.defaultLang)
      );
    });

    if (urls.length !== 1) {
      urls = urls.filter(url => url.lang === Configuration.lang);
    }
    return urls[0].path;
  }

  generateUrl(layout, view, params) {
    let path = this.getPath(layout, view);
    params.id = Object.values(params).join("_");
    for (let id in params) {
      let reg = ":" + id;
      path = path.replace(reg, params[id]);
    }
    return path;
  }
}

export default new UrlStore();

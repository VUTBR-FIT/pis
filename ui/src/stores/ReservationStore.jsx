import AbstractStore from "./AbstractStore";
import DateUtil from "../utils/Date";

class ReservationStore extends AbstractStore {
  getById(id) {
    return this._byId(id, this.getApiPath("/reservation/" + id));
  }

  getAll() {
    return this._getAll(this.getApiPath("/reservation"));
  }

  add(titleId, userId, onSuccess, onError) {
    let date = new Date();
    let dateReservation = String(date.getFullYear()) + "-" +
        String(date.getMonth() + 1).padStart(2, "0") + "-" +
      String(date.getDate()).padStart(2, "0");
    date.setDate(date.getDate() + 30);
    let dateExpiry =
      String(date.getFullYear()) + "-" +
      String(date.getMonth() + 1).padStart(2, "0") + "-" +
      String(date.getDate()).padStart(2, "0");
    this._add(
      this.getApiPath("/reservation"),
      {
        dateReservation,
        dateExpiry,
        userId,
        titleId
      },
      onSuccess,
      onError
    );
  }

  update(id, data, onSuccess, onError) {
      let _this = this;
      let startDate = undefined;
      if (data.dateReservation !== undefined)
          startDate = DateUtil.transformDate(data.dateReservation);

      let endDate = undefined;
      if (data.dateExpiry !== undefined)
          endDate = data.dateExpiry;

      this._update(
          this.getApiPath("/reservation/" + id),
          {
              dateReservation: startDate,
              dateExpiry: endDate,
              userId: data.user !== undefined ? data.user.id : undefined,
              titleId: data.title !== undefined ? data.title.id : undefined,
              status: data.status !== undefined ? _this.getStatus(data.status) : undefined
          },
          onSuccess,
          onError
      );
  }

  delete(id, onSuccess, onError) {
    this._delete(id, this.getApiPath("/reservation/" + id), onSuccess, onError);
  }

    getStatusMapping = () => {
        return [
            { id: "active", numId: 1},
            { id: "cancelled", numId: 3},
            { id: "expired", numId:  2}
        ];
    };

    getStatus = id => {
        let result = this.getStatusMapping().filter(item => {
            if (typeof id === "string") return item.id === id;
            else return item.numId === parseInt(id);
        });
        return result[0].numId;
    }
}

export default new ReservationStore();

import AbstractStore from "./AbstractStore";
import DateUtil from "../utils/Date";
import TitleStore from "./TitleStore";

class Store extends AbstractStore {
  getById(id) {
    return this._byId(id, this.getApiPath("/rental/" + id));
  }

  getAll() {
    return this._getAll(this.getApiPath("/rental"));
  }

  add(data, onSuccess, onError) {
    if (data.dateRental === undefined)
      data.dateRental = DateUtil.getCurrentDate("-");
    if (data.dateExpiry === undefined)
      data.dateExpiry = DateUtil.getCurrentDate("-", undefined, 1);
    data.status = this.getStatus("active");

    this._add(this.getApiPath("/rental"), data, response => {
        let item = TitleStore.list.filter(title => {
            let copies = title.copies.filter(copy => copy.id === data.copyId);
            return copies.length !== 0;
        });
        if (item.length) {
            TitleStore.detailList = TitleStore.detailList.filter(title => title !== item[0].id);
            TitleStore.finishedRequest = TitleStore.finishedRequest.filter(link => !link.includes(item[0].id.toString()));
        }
        if (onSuccess !== undefined)
            onSuccess(response);
    }, onError);
  }

  update(id, data, onSuccess, onError) {
    this._update(this.getApiPath("/rental/" + id), data, response => {
      let item = TitleStore.list.filter(title => {
        let copies = title.copies.filter(copy => copy.id === response.copy.id);
        return copies.length !== 0;
      });
      if (item.length) {
          TitleStore.detailList = TitleStore.detailList.filter(title => title !== item[0].id);
          TitleStore.finishedRequest = TitleStore.finishedRequest.filter(link => !link.includes(item[0].id.toString()));
      }
      if (onSuccess !== undefined)
        onSuccess(response);
    }, onError);
  }

  delete(id, onSuccess, onError) {
    this._delete(id, this.getApiPath("/rental" + id), onSuccess, onError);
  }

  getStatusMapping = () => [
    { id: "active", numId: 1, name: "Active"},
    { id: "expired", numId:  2, name: "Expired"},
    { id: "returned", numId: 3, name: "Returned"},
    { id: "lost", numId: 4, name: "Lost"}
  ];

  getStatus = id => {
    let result = this.getStatusMapping().filter(item => {
      if (typeof id === "string") return item.id === id;
      else return item.numId === parseInt(id);
    });
    return result[0].numId;
  }
}

export default new Store();

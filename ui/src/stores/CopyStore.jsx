import AbstractStore from "./AbstractStore";

class CopyStore extends AbstractStore {
  getById(id) {
    return this._byId(id, this.getApiPath("/copy/" + id));
  }

  getAll() {
    return this._getAll(this.getApiPath("/copy"));
  }

  add(data, onSuccess, onError) {
    this._add(this.getApiPath("/copy"), data, onSuccess, onError);
  }

  update(id, data, onSuccess, onError) {
    this._update(this.getApiPath("/copy/" + id), data, onSuccess, onError);
  }

  delete(id, onSuccess, onError) {
    this._delete(id, this.getApiPath("/copy/" + id), onSuccess, onError);
  }
}

export default new CopyStore();

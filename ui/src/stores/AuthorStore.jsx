import AbstractStore from "./AbstractStore";

class AuthorStore extends AbstractStore {

  validateData(data) {
    if (data.dateDeath !== undefined && data.dateDeath.length === 0)
      data.dateDeath = undefined;
    return AbstractStore.prototype.validateData.call(this, data);
  }

  getById(id) {
    return this._byId(id, this.getApiPath("/author/" + id));
  }

  getAll() {
    return this._getAll(this.getApiPath("/author"));
  }

  add(data, onSuccess, onError) {
    this._add(this.getApiPath("/author"), data, onSuccess, onError);
  }

  update(id, data, onSuccess, onError) {
    this._update(this.getApiPath("/author/" + id), data, onSuccess, onError);
  }

  delete(id, onSuccess, onError) {
    return this._delete(
      id,
      this.getApiPath("/author/" + id),
      onSuccess,
      onError
    );
  }
}

export default new AuthorStore();

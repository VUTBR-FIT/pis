import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Redirect, Switch, Route } from "react-router-dom";

import "assets/css/material-dashboard-react.css?v=1.6.0";

import Sign from "./presentation/layouts/Sign";
import Admin from "./presentation/layouts/Admin";
import UrlStore from "./stores/UrlStore";
import Empty from "./presentation/layouts/Empty"
import NotFound from "./presentation/views/404_NotFound";

const hist = createBrowserHistory();

//UrlStore.addTypeListener(ActionType.URL_INIT, () => {
ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/" exact render={() => <Redirect to={UrlStore.getPath("Sign", "SignIn")}/>} />
      <Route strict={true} path={UrlStore.getLayoutPath("Sign")} component={Sign} />
      <Route strict={true} path={UrlStore.getLayoutPath("Admin")} component={Admin} />
        <Empty ><NotFound/></Empty>
    </Switch>
  </Router>,
  document.getElementById("root")
);
//});

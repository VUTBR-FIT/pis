let Api = {
  getContent(lang = "en") {
    switch (lang) {
      case "cz":
      case "cs":
      default:
        return require("./en.js");
    }
  }
};

module.exports = Api;

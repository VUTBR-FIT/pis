module.exports = {
  signIn: {
    labelEmail: "Email",
    labelPassword: "Password",
    submit: "Sign In"
  },
  dataTable: {
    body: {
      noMatch: "Sorry, no matching records found",
      toolTip: "Sort"
    },
    pagination: {
      next: "Next Page",
      previous: "Previous Page",
      rowsPerPage: "Rows per page:",
      displayRows: "of"
    },
    toolbar: {
      search: "Search",
      downloadCsv: "Download CSV",
      print: "Print",
      viewColumns: "View Columns",
      filterTable: "Filter Table"
    },
    filter: {
      all: "All",
      title: "FILTERS",
      reset: "RESET"
    },
    viewColumns: {
      title: "Show Columns",
      titleAria: "Show/Hide Table Columns"
    },
    selectedRows: {
      text: "rows(s) selected",
      delete: "Delete",
      deleteAria: "Delete Selected Rows"
    }
  },
  pageTitle: {//Title of page or subTitle of parts of page
    signIn: "Sign In",
    signUp: "Sign Up",
    copies: "Copies",
  },
  title: {
    authors: "Authors",
    copyCount: "Existing Copies",
    description: "Description",
    genres: "Genres",
    isbn: "ISBN",
    ean: "EAN",
    medium: "Medium",
    publishedAt: "Publication Date",
    publisher: "Publisher",
    titleName: "Title",
    create: "New Title",
    title: "Title",
    reserve: "Reserve",
    delete_1: "Are you sure you want to delete",
    delete_2: "and all of its copies?",
    reservations: "Reservation History"
  },
  author: {
    firstName: "First Name",
    middleName: "Middle Name",
    lastName: "Surname",
    dateBirth: "Date of Birth",
    dateDeath: "Date of Death",
    description: "Description",
    create: "New Author",
    formTitle: "Author",
    relatedTitles: "Released Titles",
    delete: "Are you sure you want to delete "
  },
  copy: {
    title: "Title",
    state: "Health state",
    create: "New Copy",
    update: "Edit State",
    operations: "Manage Copy",
    rentedTo: "Renting User",
    formTitle: "Copy",
    deleteContent: "Are you sure you want to delete this copy?",
    id: "Copy ID",
    availability: "Availability",
    stateRented: "rented",
    stateFree: "available"
  },
  reservation: {
    titleId: "Title",
    dateReservation: "Reservation Date",
    dateExpiry: "Expiry Date",
    operations: "Manage Reservation",
    isbn: "ISBN",
    status: "Status",
    user: "User",
    cancel: "Cancel",
    reserveAgain: "Reserve Again",
    createReservation: "Create a Reservation",
    dashboard: "Current Reservations",
    extend: "Extend Duration"
  },
  genre: {
    formTitle: "Genre",
    name: "Name",
    ageRestriction: "Age Restriction",
    description: "Description",
    create: "New Genre",
    delete: "Are you sure you want to delete",
    relatedTitles: "Related Titles"
  },
  rental: {
    dateRental: "Rental Date",
    dateExpiry: "Expiry Date",
    rentOut: "Rent to User",
    penalties: "Penalties",
    isLate: "Late Status",
    formTitle: "Rental",
    name: "User",
    titleName: "Title",
    status: "Status",
    return: "Mark as Returned",
    lose: "Mark as Lost",
    find: "Mark as Found",
    operations: "Manage Rental",
    reserve: "Reserve Again",
    dashboard: "Current Rentals"
  },
  user: {
    profileButton: "Display Profile",
    formTitle: "User",
    name: "Name",
    firstName: "First name",
    middleName: "Middle name",
    surname: "Surname",
    email: "Email",
    password: "Password",
    passwordAgain: "Password again",
    dateOfBirth: "Date of Birth",
    create: "New User",
    role: "Role",
    roleAdmin: "Admin",
    roleEmployee: "Employee",
    roleExternal: "External",
    reservations: "Reservations",
    rentals: "Rentals",
    rentalCapacity: "Capacity of Rentals",
    reservationCapacity: "Capacity of Reservations",
    delete: "Are you sure you want to delete "
  },
  menu: {
    dashboard: {
      sidebar: "Dashboard",
      title: "Dashboard"
    },
    rental: {
      sidebar: "Rentals",
      title: "Rental History",
      titleDetail: "Rental Detail"
    },
    reservation: {
      sidebar: "Reservations",
      title: "Reservation History",
      titleDetail: "Reservation Detail"
    },
    genre: {
      sidebar: "Genres",
      title: "Genre List",
      titleDetail: "Genre Detail",
      relatedTitles: "Related Titles"
    },
    title: {
      sidebar: "Titles",
      title: "Title List",
      titleDetail: "Title Detail"
    },
    user: {
      sidebar: "Users",
      title: "User List ",
      titleDetail: "User Detail"
    },
    author: {
      sidebar: "Authors",
      title: "Author List",
      titleDetail: "Author Detail"
    }
  },
  action: {
    signIn: "In",
    signOut: "Sign Out",
    close: "Close"
  },
  button: {
    signUp: "Sign Up",
    signIn: "Sign In",
    create: "Create",
    save: "Save",
    update: "Edit",
    delete: "Remove",
    add: "Add",
    close: "Close",
    back: "Back"
  },
  errorMessage: {
    signIn: "Bad password or user not found.",
    signUp: " sign up",
    refreshFail: "Failed to refresh login token.",
    signout_expiry: "You have been logged out due to inactivity.",
    oops: "Something went wrong.",
    notValidInputData: "Invalid Input Data",
    401: "You do not have the requested permissions."
    // 405: "Method not allowed."
  },
  message: {
    rental_create: "Rental has been created successfully.",
    rental_fail_reservation: "User needs to reserve this title in order to rent a copy.",
    rental_fail_waitlist: "User can only rent this copy after users with earlier reservations claim theirs.",
    reservation_create: "Reservation has been created successfully.",
    reservation_extend: "Reservation has been extended successfully.",
    reservation_cancel: "Are you sure you want to cancel the reservation of the following title?",
    reservation_info: "This title was reserved by: ",
    reservation_capacity: "Reservation capacity exceeded.",
    reservation_overdue_rental: "User has expired rentals that need to be closed.",
    reservation_cancel_success: "Reservation  has been cancelled.",
    reservation_extend_expired: "Reservation has expired and cannot be extended.",
    reservation_cancel_expired: "Reservation has expired and cannot be cancelled.",
    reservation_cancel_cancelled: "Reservation has been cancelled already.",
    genre_create: "Genre created successfully.",
    genre_edit: "Genre edited successfully.",
    copy_create: "Copy created successfully.",
    copy_edit: "Copy edited successfully.",
    title_create: "Title created successfully.",
    title_edit: "Title edited successfully.",
    author_create: "Author created successfully.",
    author_edit: "Author edited successfully.",
    author_delete: "Author removed successfully.",
    genre_delete: "Genre removed successfully.",
    user_delete: "User removed successfully.",
    title_delete: "Title removed successfully.",
    signout: "You have been logged out."
  },
    infoMessage: {
        noChange: "Input data not changed"
    }
};
# PIS Projekt 2019

Nejlepší projekt

## Instalace Laravel aplikace

Pro zavedení Laravel aplikace je třeba nainstalovat [Composer](https://getcomposer.org/download/) a mít spuštěný MySql server s vytvořenou databází pro projekt

Poté inicializovat závislosti pomocí

```bash
cd laravel
composer install
```

## Spuštění Laravel aplikace

- Vyplnit DB údaje do `.env`, pokud není vytvořen, stačí vytvořit podle předlohy `.env.example`
- Spustit server
```
php artisan serve
```
- Migrace DB (v novém okně terminálu)
```
php artisan migrate
```

- Pokial chceme naplnit DB mockupovymi datami
```
php artisan db:seed
```

- Aplikace bude běžet na `localhost:8000`
- Pokud nastane error `No application encryption key has been specified.`, je potřeba generovat klíč příkazem

- Pre vygenerovanie dokumentacie k API:
```
php artisan key:generate
```